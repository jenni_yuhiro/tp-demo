<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();


#Route::get('/', 'HomeController@index')->name('home')->middleware('auth');
 
 

 
Route::get('locale/{locale}', function () {
  	$locale = \Request::segment(2); 
	\Session::put('locale', $locale);
	return redirect('/2');
});


 



//Route::get('/', 'FrontendController@index');
//Route::group(array('prefix' => '{locale}'), function () {
Route::group(['middleware' => 'auth'], function () {
	Route::get('table-list', function () {
		return view('pages.table_list');
	})->name('table');

	Route::get('typography', function () {
		return view('pages.typography');
	})->name('typography');



	Route::get('map', function () {
		return view('pages.map');
	})->name('map');

	Route::get('notifications', function () {
		return view('pages.notifications');
	})->name('notifications');

	Route::get('rtl-support', function () {
		return view('pages.language');
	})->name('language');

	Route::get('icons', function () {
		return view('pages.icons');
	})->name('icons');

	Route::get('upgrade', function () {
		return view('pages.upgrade');
	})->name('upgrade');
});
 


//Route::group(['prefix' => 'de', 'middleware' => 'adminLanguage'], function () {

	Route::group(['prefix' => 'admin', 'middleware' => ['auth' ]], function () {
	
		

	 
		Route::resource('user', 'UserController', ['except' => ['show']]);
		Route::get('profile', ['as' => 'profile.edit', 'uses' => 'ProfileController@edit']);
		Route::put('profile', ['as' => 'profile.update', 'uses' => 'ProfileController@update']);
		Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'ProfileController@password']);

		Route::resource('questions', 'QuestionsController');
		Route::post('question-sortable', 'QuestionsController@updateOrder');
		Route::resource('categories', 'CategoriesController');
		Route::post('categories-sortable', 'CategoriesController@updateOrder');

		Route::resource('quiz', 'QuizController');
		Route::post('quiz-sortable', 'QuizController@updateOrder');

		Route::resource('information', 'InformationController');
		Route::post('info-sortable', 'InformationController@updateOrder');
		Route::post('information/delete-image', 'InformationController@deleteImage');
		Route::post('info-details', 'InformationController@infoDetailsById');
		Route::post('addQuiz', 'InformationController@addQuiz');
		Route::post('get-info-desc-html', 'InformationController@getDescHtml');
	 
		Route::post('upload_image','InformationController@uploadImage')->name('upload');
		

		Route::match(['post', 'get'], 'explanatory-video', 'ExplanatoryVideoController@explanatoryVideo')->name('explanatoryVideo');
		Route::post('explanatory/upload-video', 'ExplanatoryVideoController@uploadVideo')->name('uploadVideo');

		Route::resource('video', 'ExplanatoryVideoController');
		/*Route::get('questions', function () {
		return view('questions.index');
	})->name('questions'); */
	});
//});
Route::post('user/store', 'FrontendController@userStore')->name('userStore');
Route::post('change-language', 'FrontendController@changeLanguage')->name('changeLanguage');
Route::post('question/user-type-details', 'FrontendController@userTypeDetails')->name('userTypeDetails');
Route::post('question/info-details', 'FrontendController@infoDetails')->name('infoDetails');

Route::post('information/get-quiz-details', 'FrontendController@getQuizDetails')->name('getQuizDetails');
Route::post('information/verify-quiz', 'FrontendController@verifyQuiz')->name('verifyQuiz');
Route::post('information/review', 'FrontendController@review')->name('review');
 

Route::post('information/get-more-information', 'FrontendController@getMoreInformation');

/*************route to terminal starts here ************* */
 Route::get('terminal', 'VisitorController@index')->name('terminal');


/*************route to terminal ends here ************* */
// "/" routes will be default language routes, and "/$prefix" routes will be routes for all other languages

 
	Route::get('/home', 'HomeController@index')->name('home');
	 Route::get('/', 'FrontendController@index');
	 Route::match(['post', 'get'], '/{lang}/{user_group}',  'FrontendController@indexCategories');
	 Route::match(['post', 'get'], '/{lang}',  'FrontendController@indexUserGroup');
	 Route::match(['post', 'get'], '/',  'FrontendController@indexLanguage');
	 