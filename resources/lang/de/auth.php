<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Kein Benutzerkonto mit diesen Angaben gefunden.',
    'throttle' => 'Zu viele Login-Fehlversuche. Bitte versuchen Sie es in :seconds Sekunden erneut.',

];
