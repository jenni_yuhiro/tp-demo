<?php

return [

  /*
    |--------------------------------------------------------------------------
    | videos Language Lines
    |--------------------------------------------------------------------------
    |
  
    |
    */
  'videos'           => 'Videos',
  'video'            => 'Video',
  'management'          => 'Video Verwaltung ',
  'subHead'             => 'Hier können Sie Videos zu Ihren Einträgen verwalten',
  'addVideo'         => 'Neu',
  'categories' => 'Kategorien',
  'video_de' => 'Video DE',
  'video_en' => 'Video EN',
  'updatevideo'         => 'Update',

  'video_information'    => 'Video Information',
  'title_de' => 'Titel  (DE)',
  'title' =>  'Titel',
  'actions'             => 'Aktionen',
  'category'            => 'Kategorie',
  'created_success'     => 'Video erfolgreich hinzugefügt.',
  'updated_success'     => 'Video erfolgreich geändert.',
  'delete_title'        => 'Löschen bestätigen',
  'delete_confirmation' => 'Möchten Sie das Video wirklich löschen?',
  'delete_sucess'       => 'Video erfolgreich gelöscht.',



  /*  <!------------------------> */


  'video_management' => 'Video Verwaltung',
  'video'            => 'Video',

  'validation' => 
  [ 'category_required' => 'Kategorie wird benötigt',
    'videoName_required'  =>'Video wird benötigt',
  ],
];
