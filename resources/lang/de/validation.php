<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted' => 'Sie müssen das :attribute annehmen.',
    'active_url' => ':attribute ist keine gültige URL.',
    'after' => ':attribute muss ein Datum nach :date sein.',
    'after_or_equal' => ':attribute muss ein Datum nach oder gleich zu :date sein.',
    'alpha' => ':attribute darf nur Buchstaben enthalten.',
    'alpha_dash' => ':attribute darf nur Buchstaben, Zahlen, Gedanken- und Unterstriche enthalten.',
    'alpha_num' => ':attribute darf nur Buchstaben und Zahlen enthalten.',
    'array' => 'The :attribute must be an array.',
    'before' => ':attribute muss ein Datum vor :date sein.',
    'before_or_equal' => ':attribute muss ein Datum vor oder gleich zu :date sein.',
    'between' => [
        'numeric' => ':attribute muss zwischen :min und :max sein.',
        'file' => ':attribute muss zwischen :min und :max kilobytes haben.',
        'string' => ':attribute muss zwischen :min und :max Zeichen enthalten.',
        'array' => ':attribute muss zwischen :min und :max Gegenstände sein.',
    ],
    'boolean' => 'Das :attribute Feld muss wahr oder falsch sein.',
    'confirmed' => 'Die :attribute Bestätigung stimmt nicht überein.',
    'date' => ':attribute ist kein gültiges Datum.',
    'date_equals' => ':attribute Datum muss mit :date identisch sein.',
    'date_format' => ':attribute stimmt nicht mit diesem Format :format überein.',
    'different' => ':attribute und :other dürfen nicht gleich sein.',
    'digits' => ':attribute muss aus :digits Ziffern bestehen.',
    'digits_between' => ':attribute muss zwischen :min und :max Ziffern bestehen.',
    'dimensions' => ':attribute hat ungültige Bildabmessungen.',
    'distinct' => 'Das :attribute Feld hat einen doppelten Wert.',
    'email' => ':attribute muss eine gültige E-Mail-Adresse sein.',
    'ends_with' => ':attribute muss mit: :values enden.',
    'exists' => 'Das gewählte :attribute ist ungültig.',
    'file' => ':attribute muss eine Datei sein.',
    'filled' => 'Das :attribute Feld muss einen Wert haben.',
    'gt' => [
        'numeric' => ':attribute muss größer sein als :value.',
        'file' => ':attribute muss größer sein als :value kilobytes.',
        'string' => ':attribute muss mehr als :value Zeichen haben.',
        'array' => ':attribute muss mehr als :value Gegenstände haben.',
    ],
    'gte' => [
        'numeric' => ':attribute muss größer oder gleich sein als :value.',
        'file' => ':attribute muss größer oder gleich sein als :value kilobytes.',
        'string' => ':attribute muss größer oder gleich sein als :value Zeichen.',
        'array' => ':attribute muss :value Gegenstände oder mehr haben.',
    ],
    'image' => ':attribute muss ein Bild sein.',
    'in' => 'Das gewählte :attribute ist ungültig.',
    'in_array' => 'Das :attribute Feld existiert nicht in :other.',
    'integer' => ':attribute muss eine ganze Zahl sein.',
    'ip' => ':attribute muss eine gültige IP Adresse sein.',
    'ipv4' => ':attribute muss eine gültige IPv4 Adresse sein.',
    'ipv6' => ':attribute muss eine gültige IPv6 Adresse sein.',
    'json' => ':attribute muss ein gültiger JSON String sein.',
    'lt' => [
        'numeric' => ':attribute muss weniger sein als :value.',
        'file' => ':attribute muss kleiner als :value kilobytes sein.',
        'string' => ':attribute muss weniger als :value Zeichen haben.',
        'array' => ':attribute muss weniger als :value Gegenstände haben.',
    ],
    'lte' => [
        'numeric' => ':attribute muss gleich oder weniger als :value sein.',
        'file' => ':attribute muss gleich oder weniger als :value kilobytes sein.',
        'string' => ':attribute muss gleich oder weniger als :value Zeichen haben.',
        'array' => ':attribute darf nicht mehr als :value Gegenstände haben.',
    ],
    'max' => [
        'numeric' => ':attribute darf nicht größer als :max sein.',
        'file' => ':attribute darf nicht mehr als :max kilobytes haben.',
        'string' => ':attribute darf nicht mehr als :max Zeichen haben.',
        'array' => ':attribute darf nicht mehr als :max Gegenstände haben.',
    ],
    'mimes' => ':attribute muss diser Dateityp sein: :values.',
    'mimetypes' => ':attribute muss diser Dateityp sein: :values.',
    'min' => [
        'numeric' => ':attribute muss mindestens :min sein.',
        'file' => ':attribute muss mindestens :min kilobytes sein.',
        'string' => ':attribute muss mindestens :min Zeichen haben.',
        'array' => ':attribute muss mindestens :min Gegenstände haben.',
    ],
    'not_in' => 'Das gewählte :attribute ist ungültig.',
    'not_regex' => 'Das :attribute Format ist ungültig.',
    'numeric' => ':attribute muss eine Zahl sein.',
    'password' => 'Das Passwort ist inkorrekt.',
    'present' => 'Das :attribute Feld muss vorhanden sein.',
    'regex' => 'Das :attribute Format ist ungültig.',
    'required' => 'Das :attribute Feld wird benötigt.',
    'required_if' => 'Das :attribute Feld wird benötigt, wenn :other :value ist.',
    'required_unless' => 'Das :attribute Feld wird benötigt, außer :other ist in :values.',
    'required_with' => 'Das :attribute Feld wird benötigt, wenn :values vorhanden ist.',
    'required_with_all' => 'Das :attribute Feld wird benötigt, wenn :values vorhanden sind.',
    'required_without' => 'Das :attribute Feld wird benötigt, wenn :values nicht vorhanden sind.',
    'required_without_all' => 'Das :attribute Feld wird benötigt, wenn keine der :values vorhanden sind.',
    'same' => ':attribute und :other müssen übereinstimmen.',
    'size' => [
        'numeric' => ':attribute muss :size sein.',
        'file' => ':attribute muss :size kilobytes sein.',
        'string' => ':attribute muss :size Zeichen haben.',
        'array' => ':attribute muss :size Gegenstände beinhalten.',
    ],
    'starts_with' => ':attribute muss beginnen mit: :values.',
    'string' => ':attribute muss ein String sein.',
    'timezone' => ':attribute muss innerhalb einer gültigen Zone liegen.',
    'unique' => ':attribute wird bereits verwendet.',
    'uploaded' => ':attribute wurde nicht hochgeladen.',
    'url' => 'Das :attribute Format ist ungültig.',
    'uuid' => ':attribute muss eine gültige UUID sein.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap our attribute placeholder
    | with something more reader friendly such as "E-Mail Address" instead
    | of "email". This simply helps us make our message more expressive.
    |
    */

    'attributes' => [],

];
