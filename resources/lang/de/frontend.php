<?php

return [

  /*
    |--------------------------------------------------------------------------
    | Users Language Lines
    |--------------------------------------------------------------------------
    |
  
    |
    */
  'exvideo'             => 'Erklärendes Video',
  'language'            => 'Sprache',
  'select_language'      => 'Sprache wählen',
  'personal_information'           => 'Persönliche Angaben',
  'personal_details'     => 'Persönliche Daten',
  'review'              => 'Vorschau ',
  'others'            => 'Andere',

  'step' => 'Schritt',
  'to_complete' => 'abschließen',
  'complete' => 'abgeschlossen',
  'of' => 'von',
  'next' => 'Weiter',
  'back' => 'zurück',
  'neu_starten'  => 'Neu Starten',

  'actions'          => 'Actionen',
  'role'            => 'Rolle',
  'user_type' => 'In welcher Funktion kommen Sie zu uns?',
  //----------------------------------
  'contact_details' => 'Bitte geben Sie lhre persönlichen Daten ein.',
  'user_type'        => 'In welcher Funktion kommen Sie zu uns?',
  'short_intro'      => 'Vielen Dank für Ihre Anmeldung!',

  'short_instructions' => 'Bitte sehen Sie sich die folgende Unterweisung aufmerksam an. Im Anschluss werden wir Ihnen noch einige Verständnisfragen stellen.',
  'understand_btn'    =>'Verstanden',

  'user_type_lbl' => 'Nutzergruppe',
  'surname' => 'Nachname',
  'firstname' => 'Vorname',
  'company' => 'Firma',
  'surname_error' => 'Bitte geben Sie Ihren Nachnamen an.',
  'firstname_error' => 'Bitte geben Sie Ihren Vornamen an.',
  'user_type_error'   => 'Bitte geben Sie Nutzergruppen an.',

  'quiz_failed_confirmation' => 'Achtung!',
  'quiz_failed_confirmation_txt' => "Sie haben eine falsche Antwort gegeben! <br> Möchten Sie die Einweisung erneut anzeigen?",
  'yes' => 'Ja',
  'close' => 'Schließen',
  'print' => 'Drucken',
  'video_confirmation' => 'Aufmerksam',
  'video_confirmation_txt' => 'Bitte schauen Sie sich das vollständige Video an.',
  'video_restarten' => 'Video Neu Starten',
  /*  <!------------------------> */


  'review_details' =>
  [

    'user_type' => 'Funktion: :name',
    'company' => 'Firma: :name',
    'firstname' => 'Vorname: :name',
    'surname' => 'Name: :name',

    'cs_valid_date' => 'Dieses Sicherheitszertifikat ist gültig bis: :name',
    

    'review_content' => 'Sie haben erfolgreich die Einweisung auf unserem Online-CHECK-IN-Portal durchgeführt.<br>
                        Am Besuchstag melden Sie sich bitte mit diesem Online-Zertifikat an der Anmeldung.<br>
                        Danach erhalten Sie Ihren Besucherausweis.',

    'greet' => 'Sehr geehrte Frau/Herr :name',
    'date' => 'Datum :name',

    'security_certificate' => 'Sicherheitszertifikat',


  ],

  'languageSelect' => 'Bitte wählen Sie Ihre Sprache',
  
'language_head' =>'HERZLICH <br> WILLKOMMEN',
'language_footer'=>'Bitte wählen Sie Ihre Sprache',
  'languageSelect' => 'Please select your Language',
];
