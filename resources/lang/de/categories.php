<?php

return [

  /*
    |--------------------------------------------------------------------------
    | Users Language Lines
    |--------------------------------------------------------------------------
    |
  
    |
    */
   
 
     'categories' =>   'Kategorien',
  
     'management'          => 'Kategorien verwalten',
     'subHead'             => 'Hier können Sie Ihre Kategorien verwalten',
     'addCategory'         => 'Neu',
     'updateCategory'      => 'Aktualisieren',
   
     'actions'             => 'Aktionen',
     'category'            => 'Kategorie',
     'created_success'     => 'Kategorie erfolgreich hinzugefügt.',
     'updated_success'     => 'Kategorie erfolgreich aktualisiert.',
     'delete_title'        => 'Löschen bestätigen',
     'delete_confirmation' => 'Sind Sie sicher, dass Sie diese Kategorie löschen möchten?',
     'delete_sucess'       => 'Kategorie erfolgreich gelöscht.',
     
     'quiz_information'    => 'Kategorie Informationen',
     'type' => ' Typ',
     'name' => 'Kategoriename EN',
 
  'name_de' => 'Kategoriename DE',
    /*  <!------------------------> */


];
