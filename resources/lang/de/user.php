<?php

return [

  /*
    |--------------------------------------------------------------------------
    | Users Language Lines
    |--------------------------------------------------------------------------
    |
  
    |
    */
  'name'             => 'Name',
  'email'            => 'E-Mail',
  'management'       => 'Benutzerverwaltung',
  'subHead'          => 'Hier können Sie Nutzer verwalten',
  'addUser'          => 'Neuer Nutzer',
  'created_at' =>'Angelegt am',

  'actions'          => 'Aktionen',
  'role'            => 'Role',
  'created_success'     => 'Nutzer erfolgreich angelegt.',
  'updated_success'     => 'Nutzer erfolgreich geändert.',
  'delete_title'        => 'Löschen bestätigen',
  'delete_confirmation' => 'Möchten Sie diesen Nutzer wirklich löschen?',
  'delete_sucess'       => 'Nutzer erfolgreich gelöscht.',

  /*  <!------------------------> */


];
