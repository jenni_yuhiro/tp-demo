<?php

return [

  /*
    |--------------------------------------------------------------------------
    | quiz Language Lines
    |--------------------------------------------------------------------------
    |
  
    |
    */
  'quiz'                => 'Quiz',
  'quiz'           		=> 'Quiz',
  'management'          => 'Quiz Verwaltung',
  'subHead'             => 'Hier können Sie Ihre Rückfragen verwalten.',
  'addQuestion'         => 'Neues Quiz',
  'updateQuestion'      => 'Quiz bearbeiten',

  'actions'             => 'Aktionen',
  'category'            => 'Kategorie',
  'created_success'     => 'Neuen Quiz-Eintrag erfolgreich erstellt.',
  'updated_success'     => 'Eintrag erfolgreich geändert.',
  'delete_title'        => 'Löschen bestätigen',
  'delete_confirmation' => 'Möchten Sie diesen Eintrag wirklich löschen?',
  'delete_sucess'       => 'Eintrag erfolgreich gelöscht.',
  
  'quiz_information'    => 'Quiz Information',
  'possible_answers'    => 'Mögliche Antworten',
 
 
  'question_de'  => 'Frage DE',  
  'question'      => 'Frage  ',  
 'answer'        => 'Antwort',
 'no_infos'            => 'There is no information/video added for the Usergroup and Category.',
 'multi_answers'  => 'Have Multiple Choice Answers?'
  
];
