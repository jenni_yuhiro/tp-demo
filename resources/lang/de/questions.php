<?php

return [

  /*
    |--------------------------------------------------------------------------
    | Questions Language Lines
    |--------------------------------------------------------------------------
    |
  
    |
    */
  'questions'           => 'Fragen & Antworten',
  'question'            => 'Frage',
  'management'          => 'Nutzergruppen',
  'subHead'             => 'Hier können Sie Fragen & Antworten für bestimmte Nutzergruppen verwalten',
  'user_type'           => 'Nutzergruppe',
  'addQuestion'         => 'Neu',
  'updateQuestion'      => 'Aktualisieren',

  'actions'             => 'Aktionen',
  'category'            => 'Kategorie',
  'created_success'     => 'Neuer Eintrag erfolgreich erstellt.',
  'updated_success'     => 'Eintrag erfolgreich geändert.',
  'delete_title'        => 'Löschen bestätigen',
  'delete_confirmation' => 'Möchten Sie diesen Eintrag wirklich löschen?',
  'delete_sucess'       => 'Eintrag erfolgreich gelöscht.',
  
'questions_information' => 'Frage & Antwort Informationen',
'possible_answers' 		=> 'Mögliche Antworten',

'question_en'            => 'Frage EN',
'question_de'            => 'Frage DE',
'answer_en'              => 'Antwort EN',
'answer_de'              => 'Antwort DE',

  /*  <!------------------------> */

  'exvideo'          => 'Erklärendes Video',
  'video_management' => 'Video Management',
  'video'            => 'Video',


];
