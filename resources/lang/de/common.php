<?php

return [

  /*
    |--------------------------------------------------------------------------
    | Users Language Lines
    |--------------------------------------------------------------------------
    |
  
    |
    */

  'dashboard'           => 'Startmenü',
  'welcome_text'       => 'Willkommen im TP Checkin Portal',
  'categories' =>   'Kategorien',
  'select_type' => '--- Typ wählen ---',
  'select_category' => '--- Kategorie wählen ---',
  'select_user_group'  => '--- Nutzergruppe wählen ---',
  'confirm_lbl' => 'Ja',
  'cancel_confirm_lbl' => 'Abbrechen',

  'visitor' => 'Besucher',
  'foreign_worker' =>  'Fremdarbeiter',
  'truck_driver'  => 'LKW-Fahrer',
   'save' =>'Save'

  /*  <!------------------------> */


];
