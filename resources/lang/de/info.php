<?php

return [

  /*
    |--------------------------------------------------------------------------
    | infos Language Lines
    |--------------------------------------------------------------------------
    |
  
    |
    */
  'infos'           => 'Infos ',
  'info'            => 'Unterweisungen',
  'management'          => 'Unterweisungs Verwaltung',
  'subHead'             => 'Hier können Sie Unterweisungen zu Ihren Kategorien verwalten.',
  'addInfo'               => 'Neu',

  'updateInformation'   => 'Aktualisieren',

  'categories'          => 'Kategorie',
  'info_de'             => 'Anweisung DE',
  'info_en'             => 'Anweisung EN',
  'updateinfo'          => 'Aktualisieren',
  'info_desc_de'        => 'Beschreibung DE',
  'info_desc'           => 'Beschreibung',
  'image'               => 'Bild',
  'actions'             => 'Aktionen',
  'category'            => 'Kategorie',
  'created_success'     => 'Beschreibung erfolgreich hinzugefügt.',
  'updated_success'     => 'Beschreibung erfolgreich gändert.',
  'delete_title'        => 'Löschen bestätigen',
  'delete_confirmation' => 'Sind Sie sicher, dass Sie die Beschreibung löschen möchten?',
  'delete_sucess'       => 'Beschreibung erfolgreich gelöscht.',
  
'infos_information'     => 'Beschreibungen zu Fragen & Antworten',
'delete_img'            =>   'Lösche Bild',
'page'                  =>   'Page'
 
 


];
