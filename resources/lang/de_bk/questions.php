<?php

return [

  /*
    |--------------------------------------------------------------------------
    | Questions Language Lines
    |--------------------------------------------------------------------------
    |
  
    |
    */

    'questions'           => 'Fragen',
    'question'            => 'Frage',
    'management'          => 'Fragen Management',
    'subHead'             => 'Hier können Sie die Fragen managen',
    'addQuestion'         => 'Neue Frage erstellen',
    'updateQuestion'         =>'Frage aktualisieren',
  
    'actions'             => 'Aktionen',
    'category'            => 'Kategorie',
    'created_success'     => 'Frage erfolgreich hinzugefügt.',
    'updated_success'     => 'Frage erfolgreich aktualisiert.',
    'delete_title'        => 'Bestätigung',
    'delete_confirmation' => 'Sind Sie sicher?',
    'delete_sucess'       => 'Frage gelöscht.',
    
'questions_information' => 'Informatojnen zur Frage',
'possible_answers' =>'Mögliche Antworten',
  
    /*  <!------------------------> */
  
    'exvideo'          => 'Video',
    'video_management' => ' Video Management',
    'video'            => 'Video',
  ];
 
