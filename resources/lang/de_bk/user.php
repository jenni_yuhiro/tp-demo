<?php

return [

  /*
    |--------------------------------------------------------------------------
    | Users Language Lines
    |--------------------------------------------------------------------------
    |
  
    |
    */
  
    'name'             => 'Name',
    'email'            => 'E-Mail',
    'management'       => 'Benutzer Management',
    'subHead'          => 'Hier können Sie die Benutzer verwalten',
    'addUser'          => 'Benutzer hinzufügen',
    'created_at' =>'Erstellt am',
  
    'actions'          => 'Aktionen',
    'role'            => 'Rolle',
    'created_success'     => 'Erfolgreich angelegt.',
    'updated_success'     => 'Erfolgreich aktualisiert.',
    'delete_title'        => 'Bestätigung',
    'delete_confirmation' => 'Sind Sie sicher',
    'delete_sucess'       => 'Erfolgreich gelöscht.',
  
    /*  <!------------------------> */


];
