<?php

return [

  /*
    |--------------------------------------------------------------------------
    | Users Language Lines
    |--------------------------------------------------------------------------
    |
  
    |
    */
  'exvideo'             => 'Erklärendes Video',
  'language'            =>'Sprache',
  'select_language'      =>'Select Sprache',
  'personal_information'            => 'Persönliche Informationen',
  'personal_details'     => 'Persönliche Informationen',
  'review'              => 'Review ',
  'others'            => 'Andere',
 

  'actions'          => 'Aktionen',
  'role'            => 'Rolle',
 

  'step' => 'Schritt',
  'to_complete' =>'fertigstellen' ,
  'complete' =>'abgeschlossen' ,
  'of' =>'von',

  'next' => 'Next',
  'back' => 'Neustart',

  /*  <!------------------------> */
 
  'user_type' => 'User Types',
  'user_type_lbl' => 'User Type',
  'surname' => 'Surname',
  'firstname' => 'Vorname',
  'company' => 'Company',

   
  'surname_error' =>'Bitte geben Sie Ihren Nachnamen ein.',
  'firstname_error' => 'Bitte geben Sie Ihren Vornamen ein.',  

  'quiz_failed_confirmation' => 'Bestätigung',
  'quiz_failed_confirmation_txt' => "You did not answer the question correctly! <br> Are you sure you want to reload the infomation?",
  'yes' => 'Ja',
  'close' => 'Schließen'
];
