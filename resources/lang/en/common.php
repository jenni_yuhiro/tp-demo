<?php

return [

  /*
    |--------------------------------------------------------------------------
    | Users Language Lines
    |--------------------------------------------------------------------------
    |
  
    |
    */

  'dashboard'           => 'Dashboard',
  'welcome_text'       => 'Welcome to TP Checkin Portal',
  'categories' =>   'Categories',
  'select_type' => '--- Select Type ---',
  'select_category' => '--- Select Category ---',
  'confirm_lbl' => 'Yes',
  'cancel_confirm_lbl' => 'Cancel',

     'visitor' => 'Visitor' ,
     'foreign_worker' =>  'Foreign worker',
      'truck_driver'  => 'Truck driver',
      'save' =>'Save'
  /*  <!------------------------> */


];
