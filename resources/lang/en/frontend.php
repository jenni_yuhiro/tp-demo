<?php

return [

  /*
    |--------------------------------------------------------------------------
    | Users Language Lines
    |--------------------------------------------------------------------------
    |
  
    |
    */
  'exvideo'             => 'Explanatory Video',
  'language'            => 'Language',
  'select_language'      => 'Select Language',
  'personal_information'            => 'Personal Information',
  'personal_details'     => 'Personal Information',
  'review'              => 'Review ',
  'others'            => 'Others',

  'step' => 'Step',
  'to_complete' => 'to complete',
  'complete' => 'complete',
  'of' => 'of',
  'next' => 'Next',
  'back' => 'Back',
  'neu_starten'  => 'Restart',

  'actions'          => 'Actions',
  'role'             => 'Role',
  //----------------------------------
  'contact_details' =>'Please enter your personal information.',
  'user_type'        => 'In what capacity are you visiting our company?',
  'short_intro'      => 'Thank you for signing up!',
  'short_instructions' => 'Please read the following instructions carefully. You will be asked a few questions afterwards.',
  'understand_btn'    =>'Understood',
  'user_type_lbl' => 'User Group',
  'surname' => 'Surname',
  'firstname' => 'First Name',
  'company' => 'Company',
  'surname_error' => 'Please enter your Surname.',
  'firstname_error' => 'Please enter your First Name.',
  'user_type_error'   => 'Please enter User groups',
  'quiz_failed_confirmation' => 'Confirmation',
  'quiz_failed_confirmation_txt' => "You did not answer the question correctly! <br> Are you sure you want to reload the infomation?",
  'yes' => 'Yes',
  'close' => 'Close',
  'print' => 'Print',
  'video_confirmation' => 'Alert',
  'video_confirmation_txt' => 'Please watch full video.',
  'video_restarten' => 'Video Neu Starten',
  'review_details' =>
  [
    'cs_valid_date' => 'Dieses Sicherheitszertifikat ist gültig bis: :name',
    'user_type' => 'Funktion: :name',
    'company' => 'Company: :name',
    'firstname' => 'Vorname: :name',
    'surname' => 'Name: :name',

    'review_content' => 'Sie haben erfolgreich die Einweisung auf unserem Online-CHECK-IN-Portal durchgeführt.<br>
Am Besuchstag melden Sie sich bitte mit diesem Online-Zertifikat an der Anmeldung.<br>
Danach erhalten Sie Ihren Besucherausweis.',
 
    'greet' => 'Sehr geehrte Frau/Herr :name',
    'date' => 'Datum :name',

    'security_certificate' => 'Sicherheitszertifikat',

  ],


  /*  <!------------------------> */

'language_head' =>'HERZLICH <br> WILLKOMMEN',
'language_footer'=>'Bitte wählen Sie Ihre Sprache',
  'languageSelect' => 'Please select your Language',
];
