<?php

return [

  /*
    |--------------------------------------------------------------------------
    | Users Language Lines
    |--------------------------------------------------------------------------
    |
  
    |
    */
  'name'             => 'Name',
  'email'            => 'Email',
  'management'       => 'User Management',
  'subHead'          => 'Here you can Manage Users',
  'addUser'          => 'Add User',
  'created_at' =>'Created At',

  'actions'          => 'Actions',
  'role'            => 'Role',
  'created_success'     => 'Successfully Added a User.',
  'updated_success'     => 'Successfully Updated a User.',
  'delete_title'        => 'Confirmation',
  'delete_confirmation' => 'Are you sure you want to delete the User?',
  'delete_sucess'       => 'Successfully Deleted a User.',

  /*  <!------------------------> */


];
