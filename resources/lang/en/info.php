<?php

return [

  /*
    |--------------------------------------------------------------------------
    | infos Language Lines
    |--------------------------------------------------------------------------
    |
  
    |
    */
  'infos'           => 'infos ',
  'info'            => 'Information',
  'management'          => 'Info Management ',
  'subHead'             => 'Here you can manage Information for Questions ',
  'addInfo'               => 'Add',

  'updateInformation'   => 'Update',

  'categories'          => 'Categories',
  'info_de'             => 'Info DE',
  'info_en'             => 'Info EN',
  'updateinfo'          => 'Update',
  'info_desc_de'        => 'Description DE',
  'info_desc'           => 'Description',
  'image'               => 'Image',
  'actions'             => 'Actions',
  'category'            => 'Category',
  'created_success'     => 'Successfully Added an Infomation.',
  'updated_success'     => 'Successfully Updated an Infomation.',
  'delete_title'        => 'Confirmation',
  'delete_confirmation' => 'Are you sure you want to delete the infomation?',
  'delete_sucess'       => 'Successfully Deleted an Infomation.',

  'infos_information'     => 'Information for Questions & Answers',
  'delete_img'            =>   'Delete Image',
  'page'                  =>   'Page',



];
