<?php

return [

  /*
    |--------------------------------------------------------------------------
    | quiz Language Lines
    |--------------------------------------------------------------------------
    |
  
    |
    */
  'quiz'                => 'Quiz',
  'quiz'                => 'Quiz',
  'management'          => 'Quiz Management',
  'subHead'             => 'Here you can manage Questions & Answers',
  'addQuestion'         => 'Add Quiz',
  'updateQuestion'      => 'Update Quiz',

  'actions'             => 'Actions',
  'category'            => 'Category',
  'created_success'     => 'Successfully Added a Quiz.',
  'updated_success'     => 'Successfully Updated a Quiz.',
  'delete_title'        => 'Confirmation',
  'delete_confirmation' => 'Are you sure you want to delete the Quiz?',
  'delete_sucess'       => 'Successfully Deleted a Quiz.',
  
  'quiz_information'    => 'Quiz Information',
  'possible_answers'    =>'Possible Answers',
 
  'question_de'        => 'Question DE',  
  'question'           => 'Question',  
 'answer'              => 'Answer',
 'no_infos'            => 'There is no information/video added for the Usergroup and Category.',
  
];
