<?php

return [

  /*
    |--------------------------------------------------------------------------
    | Questions Language Lines
    |--------------------------------------------------------------------------
    |
  
    |
    */
  'questions'           => 'Questions & Answers',
  'question'            => 'Question',
  'management'          => 'QA - User Types ',
  'subHead'             => 'Here you can manage Questions and Answers for User Types ',
  'user_type'           =>  'User Type',
  'addQuestion'         =>  'Add',
  'updateQuestion'      =>  'Update',

  'actions'             => 'Actions',
  'category'            => 'Category',
  'created_success'     => 'Successfully Added a Question & Answer.',
  'updated_success'     => 'Successfully Updated a Question & Answer.',
  'delete_title'        => 'Confirmation',
  'delete_confirmation' => 'Are you sure you want to delete the Question & Answer?',
  'delete_sucess'       => 'Successfully Deleted a Question & Answer.',
  
'questions_information' => 'Question & Answer Information',
'possible_answers' =>'Possible Answers',
'question_en'            => 'Question EN',
'question_de'            => 'Question DE',
'answer_en'              => 'Answer EN',
'answer_de'              => 'Answer DE',
  /*  <!------------------------> */

  'exvideo'          => 'Explanatory Video',
  'video_management' => 'Video Management',
  'video'            => 'Video',


];
