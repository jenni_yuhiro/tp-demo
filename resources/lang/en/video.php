<?php

return [

  /*
    |--------------------------------------------------------------------------
    | videos Language Lines
    |--------------------------------------------------------------------------
    |
  
    |
    */
  'videos'           => 'videos',
  'video'            => 'Video',
  'management'          => 'Video Management ',
  'subHead'             => 'Here you can manage video for Questions ',
  'addVideo'         => 'Add',
  'categories' => 'Categories',
  'video_de' => 'Video DE',
  'video_en' => 'Video EN',
  'updatevideo'         => 'Update',

  'video_information'    => 'Video Information',
  'title_de' => 'Title (DE)',
  'title' =>  'Title ',
  'actions'             => 'Actions',
  'category'            => 'Category',
  'created_success'     => 'Successfully Added a video .',
  'updated_success'     => 'Successfully Updated a video .',
  'delete_title'        => 'Confirmation',
  'delete_confirmation' => 'Are you sure you want to delete the video?',
  'delete_sucess'       => 'Successfully Deleted a video .',



  /*  <!------------------------> */


  'video_management' => '  Video Management',
  'video'            => 'Video',

  'validation' => 
  [ 'category_required' => 'The category field is required',
    'videoName_required'  =>'The video is required',
  ],
];
