<?php

return [

  /*
    |--------------------------------------------------------------------------
    | Users Language Lines
    |--------------------------------------------------------------------------
    |
  
    |
    */
   
 
     'categories' =>   'Categories',
  
     'management'          => 'Categories Management',
     'subHead'             => 'Here you can manage Categories',
     'addCategory'         => 'Add ',
     'updateCategory'      => 'Update ',
   
     'actions'             => 'Actions',
     'category'            => 'Category',
     'created_success'     => 'Successfully Added a Category.',
     'updated_success'     => 'Successfully Updated a Category.',
     'delete_title'        => 'Confirmation',
     'delete_confirmation' => 'Are you sure you want to delete the Category?',
     'delete_sucess'       => 'Successfully Deleted a Category.',
     
     'quiz_information'    => 'Category Information',
     'type' => ' Type',
     'name' => 'Category Name',
 
  'name_de' => 'Category Name(DE)',
    /*  <!------------------------> */


];
