@extends('layouts.app', ['activePage' => 'add_categories', 'activeMod' => 'categories' , 'titlePage' => __('categories.management')])

@section('content')
<style>
  .form-control,
  .custom-select {
    width: 80% !important;
    display: inline;
  }

  .btn-link {
    padding: 2px;
  }
</style>
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <form method="post" action="{{  route('categories.store')}}" autocomplete="off" class="form-horizontal">
          @csrf
          @method('post')
          <div class="card ">
            <div class="card-header card-header-primary">
              <h4 class="card-title">{{ __('categories.addCategory') }}</h4>
              <p class="card-category">{{ __('categories.quiz_information') }}</p>
            </div>
            <div class="card-body ">
              @if (session('status'))
              <div class="row">
                <div class="col-sm-12">
                  <div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <i class="material-icons">close</i>
                    </button>
                    <span> {{ session('status') }}</span>
                  </div>
                </div>
              </div>
              @endif

              <div class="row">
                <label class="col-sm-2 col-form-label">{{ __('categories.type') }}</label>
                <div class="col-sm-7">
                  <div class="form-group{{ $errors->has('type') ? ' has-danger' : '' }}">
                    <select name="type" class="  browser-default custom-select">  
                    <option value="">{{ __('common.select_type') }}</option>
                    @foreach( $types as $key => $type)
                      <option value="{{$key}}">{{$type }}</option>
                      @endforeach                    
                
                    </select>

                    @if ($errors->has('type'))
                    <br><span id="type-error" class="error text-danger" for="input-type">{{ $errors->first('type') }}</span>
                    @endif
                  </div>
                </div>
              </div>


           

              <div class="row">
                <label class="col-sm-2 col-form-label">{{ __('categories.name') }}</label>
                <div class="col-sm-7">
                  <div class="form-group{{ $errors->has('category_name') ? ' has-danger' : '' }}">
                    <input class="form-control{{ $errors->has('category_name') ? ' is-invalid' : '' }}" name="category_name" id="input-question" type="text" placeholder="{{ __('categories.name') }}" value="{{ old('category_name' ) }}" aria-required="true" />
                    @if ($errors->has('category_name'))
                    <br><span id="question-error" class="error text-danger" for="input-question">{{ $errors->first('category_name') }}</span>
                    @endif
                  </div>
                </div>
              </div>
               
             
              <div class="row">
                <label class="col-sm-2 col-form-label">{{ __('categories.name_de') }}</label>
                <div class="col-sm-7">
                  <div class="form-group{{ $errors->has('category_name_de') ? ' has-danger' : '' }}">
                    <input class="form-control{{ $errors->has('category_name_de') ? ' is-invalid' : '' }}" name="category_name_de" id="input-category_name_de" type="text" placeholder="{{ __('categories.name_de') }}" value="{{ old('category_name_de' ) }}" aria-required="true" />
                    @if ($errors->has('category_name_de'))
                    <br><span id="category_name_de-error" class="error text-danger" for="input-category_name_de">{{ $errors->first('category_name_de') }}</span>
                    @endif
                  </div>
                </div>
              </div>

              <div class="card-footer ml-auto mr-auto">
                <button type="submit" class="btn btn-primary">{{ __('Save') }}</button>
              </div>
            </div>
        </form>
      </div>
    </div>
  </div>
</div>


@endsection
@push('js')

<script>
  jQuery(document).ready(function() {
 
 
    
  });
</script>
@endpush