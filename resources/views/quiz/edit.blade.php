@extends('layouts.app', ['activePage' => 'quiz', 'activeMod' => 'quiz' , 'titlePage' => __('quiz.management')])

@section('content')
<style>
  .form-control,
  .custom-select {
    width: 80% !important;
    display: inline;
  }

  .btn-link {
    padding: 2px;
  }

  .multiAnsSel {
    padding: 6px;
  }
</style>

<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <form method="POST" action="{{  route('quiz.update',$quiz->id)}}" autocomplete="off" class="form-horizontal">
          @csrf
          @method('PUT')
          <div class="card ">
            <div class="card-header card-header-primary">
              <h4 class="card-title">{{ __('quiz.updateQuestion') }}</h4>
              <p class="card-category">{{ __('quiz.quiz_information') }}</p>
            </div>
            <div class="card-body ">
              @if (session('status'))
              <div class="row">
                <div class="col-sm-12">
                  <div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <i class="material-icons">close</i>
                    </button>
                    <span> {{ session('status') }}</span>
                  </div>
                </div>
              </div>
              @endif
              @if ($errors->has('no_infos'))

              <div class="row">
                <div class="col-sm-12">
                  <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <i class="material-icons">close</i>
                    </button>
                    <span> {{ $errors->first('no_infos') }}</span>
                  </div>
                </div>
              </div>
              @endif
              <div class="row">
                <label class="col-sm-2 col-form-label">{{ __('questions.user_type') }}</label>
                <div class="col-sm-7">
                  <div class="form-group{{ $errors->has('user_group') ? ' has-danger' : '' }}">

                    <select name="user_group" id="user_group" class="   browser-default custom-select   ">
                      <option value="">{{ __('common.select_user_group') }}</option>
                      @foreach(\App\Questions::userTypes() as $key => $category )
                      <option value="{{ $key }}" @if( $key==$quiz->user_group || $key == old('user_group' ) ) selected @endif>{{ $category }}</option>
                      @endforeach
                    </select>
                    @if ($errors->has('user_group'))
                    <br><span id="user_group-error" class="error text-danger" for="input-user_group">{{ $errors->first('user_group') }}</span>
                    @endif
                  </div>
                </div>
              </div>

              <div class="row">
                <label class="col-sm-2 col-form-label">{{ __('questions.category') }}</label>
                <div class="col-sm-7">
                  <div class="form-group{{ $errors->has('question') ? ' has-danger' : '' }}">

                    <select name="category" class="  browser-default custom-select">
                      <option value="">{{ __('common.select_category') }}</option>
                      @foreach( $categories as $key => $category)
                      <option value="{{$category->id}}" @if( $category->id == $quiz->category_id || $category->id == old('category' ) )selected @endif>{{$category->name}} </option>
                      @endforeach
                    </select>
                    @if ($errors->has('category'))
                    <br><span id="category-error" class="error text-danger" for="input-category">{{ $errors->first('category') }}</span>
                    @endif
                  </div>
                </div>
              </div>
              <!--
              <div class="row">
                <label class="col-sm-2 col-form-label">{{ __('quiz.question_de') }}</label>
                <div class="col-sm-7">
                  <div class="form-group{{ $errors->has('question') ? ' has-danger' : '' }}">

                    <input class="form-control{{ $errors->has('question_de') ? ' is-invalid' : '' }}" name="question_de" id="input-question-de" type="text" placeholder="{{ __('quiz.question_de') }}" value="{{ old('question_de'  , $quiz->question_de ) }}" aria-required="true" />
                    @if ($errors->has('question_de'))
                    <br><span id="question-error" class="error text-danger" for="input-question-de">{{ $errors->first('question_de') }}</span>
                    @endif
                  </div>
                </div>
              </div>-->
              <div class="row">
                <label class="col-sm-2 col-form-label">{{ __('questions.question') }}</label>
                <div class="col-sm-7">
                  <div class="form-group{{ $errors->has('question') ? ' has-danger' : '' }}">

                    <input class="form-control{{ $errors->has('question') ? ' is-invalid' : '' }}" name="question" id="input-question" type="text" placeholder="{{ __('quiz.question') }}" value="{{ old('question'  , $quiz->question ) }}" aria-required="true" />
                    @if ($errors->has('question'))
                    <br><span id="question-error" class="error text-danger" for="input-question">{{ $errors->first('question') }}</span>
                    @endif
                  </div>
                </div>
              </div>
              <!------------------>
       
              <div class="row multiAnsCls ">
                <label class="col-sm-2 col-form-label">{{ __('quiz.multi_answers') }}</label>
                <div class="col-sm-7   ">
                  <div>
                    <input class="form-radio-input multi_answers_cls" type="radio" value="1" name="multi_answers"  @if($quizMultiAns->count() > 0) checked  @endif >
                    <label for="answer1">Yes</label> &nbsp; &nbsp;

                    <input class="form-radio-input multi_answers_cls " type="radio" name="multi_answers" value="0"  @if($quizMultiAns->count() == 0) checked  @endif >
                    <label for="answer2">No</label>
                  </div>
                </div>
              </div>
              <!------------------>
       
             
                 <!------------------>
              @if($quizMultiAns->count() > 0)  
               @foreach($quizMultiAns as $key => $valmq)
               <div class="row  ansDivEdit" >
                <label class="col-sm-2 col-form-label">{{ __('questions.possible_answers') }}</label>

                <div class="col-sm-7 inc ">
                  <div class=" controls form-group{{ $errors->has('answers') ? ' has-danger' : '' }}">
                    <input class=" form-control{{ $errors->has('answers') ? ' is-invalid' : '' }}" name="answers[{{$valmq->id}}]" id="input-answers" type="text" placeholder="{{ __('questions.answer_de') }}" value="{{ old('question' ,  $valmq->question ) }}" />
                    <select class="multiAnsSel" name="multi_answer_sel[{{$valmq->id}}]">
                      <option value="1"   @if( $valmq->answer == 1 ) selected  @endif >Yes</option>
                      <option value="0"   @if( $valmq->answer == 0 ) selected  @endif >No</option>
                    </select>

                    <a rel="tooltip" data-del-count="2" class="btn btn-danger btn-link remove_this " href="javascript:void(0);" data-original-title="" title="">                  <i class="material-icons">delete_forever</i><div class="ripple-container"></div><div class="ripple-container"></div></a>

                    
                  </div>
                </div>
              </div>
               @endforeach
              @endif 
              <!---------------------->
               <!---------------------->

               <div class="row multiAnswersCls"  @if($quizMultiAns->count() <= 0)   style="display: none;" @endif>
                <label class="col-sm-2 col-form-label">{{ __('questions.possible_answers') }}</label>

                <div class="col-sm-7 inc ansDiv">
                  <div class=" controls form-group{{ $errors->has('answers') ? ' has-danger' : '' }}">
                    <input class=" form-control{{ $errors->has('answers') ? ' is-invalid' : '' }}" name="answers[]" id="input-answers" type="text" placeholder="{{ __('questions.answer_de') }}" value="{{ old('answers.en.0' ) }}" />
                    <select class="multiAnsSel" name="multi_answer_sel[]">
                      <option value="1">Yes</option>
                      <option value="0">No</option>
                    </select>
                    <a rel="tooltip" class="btn btn-success btn-link" href="javascript:void(0);" data-original-title="" id="append" title="Add More Answers">
                      <i class="material-icons">add_circle</i>
                      <div class="ripple-container"></div>
                    </a>
                  </div>
                </div>
              </div>
              <!------------------>
              <div class="row answersCls"     @if($quizMultiAns->count() > 0)   style="display: none;" @endif>
                <label class="col-sm-2 col-form-label">{{ __('quiz.answer') }}</label>
                <div class="col-sm-7 inc ">

                  <div>
                    <div class="form-group md-radio">
                      <input class="form-radio-input" id="answer1" type="radio" value="1" name="answer" @if( $quiz->answer === 1) checked @endif>
                      <label for="answer1">Yes</label>
                    </div>
                    <div class="md-radio">
                      <input class="form-radio-input " id="answer2" type="radio" name="answer" value="0" @if( $quiz->answer === 0) checked @endif >
                      <label for="answer2">No</label>
                    </div>
                  </div>

                </div>

              </div>

              <div class="card-footer ml-auto mr-auto">
                <button type="submit" class="btn btn-primary">{{ __('Save') }}</button>
              </div>
            </div>
        </form>
      </div>
    </div>
  </div>
</div>


@endsection
@push('js')

<script>
  jQuery(document).ready(function() {

    /********************** */

    jQuery(document).on('click', '.multi_answers_cls', function() {
      if ($(this).val() == 1) {
        $('.multiAnswersCls').show();
        $('.ansDivEdit').show();
        $('.answersCls').hide();
      } else {
        $('.ansDivEdit').hide();
        $('.multiAnswersCls').hide();
        $('.answersCls').show();
      }
    });
    /********************** */

    var answersCnt = 1;
    jQuery(document).on('click', '#append', function(e) {
      answersCnt++;


      e.preventDefault();
      var html = ' <div class="row  answerApp "><label class="col-sm-2 col-form-label"> </label>\
                      <div class="col-sm-7   "><div class="controls form-group ">\
                      <input class="form-control" type="text" name="answers[]" placeholder="Antwort">\
               <select name="multi_answer_sel[]" class="multiAnsSel"><option value="1">Yes</option><option value="0">No</option></select>\
                  <a rel="tooltip" data-del-count="' + answersCnt + '" class="btn btn-danger btn-link remove_this " href="javascript:void(0);" data-original-title="" title="">\
                  <i class="material-icons">delete_forever</i><div class="ripple-container"></div></a>  </div> </div></div>';
      //  $(".answers").append();


      if ($('.answerApp').length > 0) {

        $(html).insertAfter(".answerApp:last");

      } else {
        $(html).insertAfter(".multiAnswersCls");
      }

      return false;
    });

    jQuery(document).on('click', '.remove_this', function() {


      jQuery(this).parent().parent().parent().remove();



      return false;
    });
    $("input[type=submit]").click(function(e) {
      e.preventDefault();
      $(this).next("[name=textbox]")
        .val(
          $.map($(".answersCls :text"), function(el) {
            return el.value
          }).join(",\n")
        )
    })
  });
</script>
@endpush