@extends('layouts.app', ['activePage' => 'quiz', 'activeMod' => 'quiz' , 'titlePage' => __('quiz.management')])

@section('content')
<style>
  [type="search"] {
    border: 1px solid #ccc;
  }

  [name="datatable-question_length"] {
    border: 1px solid #ccc;
    padding: 5px;
  }

  table.dataTable.no-footer {

    border-bottom: 1px solid #ccc;
  }

  table.dataTable thead th,
  table.dataTable tfoot th {
    font-weight: 300;
  }
</style>
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-primary">
            <h4 class="card-title ">{{__('quiz.quiz')}}</h4>
            <p class="card-category">{{__('quiz.subHead')}}</p>
          </div>
          <div class="card-body">
            @if (session('message'))
            <div class="row">
              <div class="col-sm-12">
                <div class="alert alert-success">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <i class="material-icons">close</i>
                  </button>
                  <span> {{ session('message') }}</span>
                </div>
              </div>
            </div>
            @endif
            <div class="row">
              <div class="col-12 text-right">
                <a href="{{ route('quiz.create') }}" class="btn btn-sm btn-primary"> {{__('quiz.addQuestion')}}</a>
              </div>
            </div>
            <div class="table-responsive">

              <table class="table   dataTable no-footer" id="datatable-question" role="grid" aria-describedby="datatable-editable_info">

                <thead class=" text-primary">
                  <tr role="row">
                    <th width="30px">#</th>
                    <th style=" ">{{ __('questions.user_type') }} </th>
                    <th>{{ __('quiz.category') }} </th>
                    <th style="width:50%">{{ __('quiz.question') }} </th>
                    <th style="">{{ __('quiz.answer') }} </th>
                    <th class="text-right"> {{ __('quiz.actions') }} </th>
                  </tr>
                </thead>
                <tbody id="tablecontents">


                  @foreach($quiz as $key => $qui)
                  <tr class="row1" data-id="{{$qui->id}}">
                    <td class="pl-3"><i class="fa fa-sort"></i></td>
                    <td>
                    {{ \App\Questions::userTypes()[$qui->user_group]  ?? '' }} 
                    </td>
                    <td>
                      {{$qui->categories->name}}
                    </td>
                    <td> {{$qui->question}} </td>
                    <td>
                    @if($qui->answer===1)Yes @else No @endif
                      
                    </td>

                    <td class="td-actions text-right">
                      <a rel="tooltip" class="btn btn-success btn-link deleteBtn" href="#" data-queid="{{$qui->id}}" data-original-title="" title="Delete">
                        <i class="material-icons">delete</i>
                        <div class="ripple-container"></div>
                      </a>
                      <a rel="tooltip" class="btn btn-success btn-link" href="{{ route('quiz.edit' ,$qui->id ) }}" data-original-title="" title="">
                        <i class="material-icons">edit</i>
                        <div class="ripple-container"></div>
                      </a>
                    </td>
                  </tr>
                  @endforeach


                </tbody>
              </table>
            </div>
          </div>
        </div>

      </div>
    </div>
    <!-----------------Confirmation ---------->



    <!-- Modal -->
    <div class="modal fade" id="selectConfirmation" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel"> {{ __('quiz.delete_title') }}</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            {{ __('quiz.delete_confirmation') }}
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('common.cancel_confirm_lbl') }}</button>
            <button type="button" class="btn btn-primary" id="selectConfirmBtn" data-question_id="">{{ __('common.confirm_lbl') }}</button>
          </div>
        </div>
      </div>
    </div>

    <!-----------------Confirmation End ---------->
    @endsection

    @push('js')

    <script>
      $(document).on("click", ".deleteBtn", function() {
        $('.alertDiv').remove();
        $('#selectConfirmation').modal();
        $('#selectConfirmBtn').attr('data-question_id', $(this).data('queid'));
      });

      $(document).on("click", "#selectConfirmBtn", function() {

        $('#selectConfirmation').modal();
        deleteQuestion($(this).attr('data-question_id'));
      });


      function deleteQuestion(Id) {
        var url = "{{ url('admin/quiz') }}/" + Id;
        var token = $('meta[name="csrf-token"]').attr('content');
        $.ajax({
          type: "DELETE",
          dataType: "json",
          url: url,
          data: {
            _token: token
          },
          success: function(response) {
            $('#selectConfirmation').modal('toggle');
            $('.card-body').prepend(' <div class="row alertDiv">\
              <div class="col-sm-12">\
                <div class="alert alert-success">\
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">\
                    <i class="material-icons">close</i>\
                  </button>\
                  <span> ' + response.message + '</span>\
                </div>              </div>            </div>');

            $('tr[data-Id="' + Id + '"').remove();

          }
        });




        //sendOrderToServer();

      }
      $(function() {
      var t =  $("#datatable-question").DataTable({
        "order": [  [ 1, 'asc' ]],
          "aoColumnDefs": [{
              "bSortable": false,
              "aTargets": [0,5]
            },

          ],
          "language": {
            "sEmptyTable":    "Keine Daten in der Tabelle vorhanden",
            "sInfo":          "_START_ bis _END_ von _TOTAL_ Einträgen",
            "sInfoEmpty":     "0 bis 0 von 0 Einträgen",
            "sInfoFiltered":  "(gefiltert von _MAX_ Einträgen)",
            "sInfoPostFix":   "",
            "sInfoThousands":   ".",
            "sLengthMenu":    "_MENU_ Einträge anzeigen",
            "sLoadingRecords":  "Wird geladen...",
            "sProcessing":    "Bitte warten...",
            "sSearch":        "Suchen",
            "sZeroRecords":   "Keine Einträge vorhanden.",
            "oPaginate": {
                "sFirst":     "Erste",
                "sPrevious":  "Zurück",
                "sNext":      "Nächste",
                "sLast":      "Letzte"
            },
            "oAria": {
                "sSortAscending":  ": aktivieren, um Spalte aufsteigend zu sortieren",
                "sSortDescending": ": aktivieren, um Spalte absteigend zu sortieren"
            }
        }
        });

        t.on( 'order.dt search.dt', function () {
        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();

/*

        $("#tablecontents").sortable({
          items: "tr",
          cursor: 'move',
          opacity: 0.6,
          update: function() {
            sendOrderToServer();
          }
        });

        function sendOrderToServer() {
          var order = [];
          var token = $('meta[name="csrf-token"]').attr('content');
          $('tr.row1').each(function(index, element) {
            order.push({
              id: $(this).attr('data-id'),
              position: index + 1
            });
          });

          $.ajax({
            type: "POST",
            dataType: "json",
            url: "{{ url('admin/question-sortable') }}",
            data: {
              order: order,
              _token: token
            },
            success: function(response) {
              if (response.status == "success") {
                console.log(response);
              } else {
                console.log(response);
              }
            }
          });
        } */
      });
    </script>
    @endpush