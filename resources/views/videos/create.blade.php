@extends('layouts.app', ['activePage' => 'add_video', 'activeMod' => 'video' , 'titlePage' => __('video.management')])

@section('content')
<style>
  .form-control,
  .custom-select {
    width: 80% !important;
    display: inline;
  }

  .btn-link {
    padding: 2px;
  }
</style>
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <form method="post" action="{{  route('uploadVideo')}}" id="frmUploadVideo" autocomplete="off" class="form-horizontal" enctype="multipart/form-data">
          @csrf
          @method('post')
          <div class="card ">
            <div class="card-header card-header-primary">
              <h4 class="card-title">{{ __('video.addVideo') }}</h4>
              <p class="card-category">{{ __('video.video_information') }}</p>
            </div>
            <div class="card-body ">
              @if (session('status'))
              <div class="row">
                <div class="col-sm-12">
                  <div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <i class="material-icons">close</i>
                    </button>
                    <span> {{ session('status') }}</span>
                  </div>
                </div>
              </div>
              @endif
              <div class="row">
                <label class="col-sm-2 col-form-label">{{ __('questions.user_type') }}</label>
                <div class="col-sm-7">
                  <div class="form-group{{ $errors->has('user_group') ? ' has-danger' : '' }}">

                    <select name="user_group" id="user_group" class="   browser-default custom-select   ">
                    <option value="">{{ __('common.select_user_group') }}</option> 
                      @foreach(\App\Questions::userTypes() as $key => $category )
                      <option value="{{ $key }}"  >{{ $category }}</option>
                      @endforeach
                    </select>
                    @if ($errors->has('user_group'))
                    <br><span id="user_group-error" class="error text-danger" for="input-user_group">{{ $errors->first('user_group') }}</span>
                    @endif
                  </div>
                </div>
              </div>


              <div class="row">
                <label class="col-sm-2 col-form-label">{{ __('quiz.category') }}</label>
                <div class="col-sm-7">
                  <div class="form-group{{ $errors->has('question') ? ' has-danger' : '' }}">

                    <select name="category_id" class="  browser-default custom-select">
                    <option value="">{{ __('common.select_category') }}</option>   
                      @foreach( $categories as $key => $category)
                      <option value="{{$category->id}}">{{$category->name}}</option>
                      @endforeach
                    </select>
                    @if ($errors->has('category_id'))
                    <br><span id="category-error" class="error text-danger" for="input-category">{{ $errors->first('category_id') }}</span>
                    @endif
                  </div>
                </div>
              </div>


              <div class="row">
                <label class="col-sm-2 col-form-label">{{ __('video.title_de') }}</label>
                <div class="col-sm-7">
                  <div class="form-group{{ $errors->has('title_de') ? ' has-danger' : '' }}">

                    <input class="form-control{{ $errors->has('title_de') ? ' is-invalid' : '' }}" name="title_de" id="input-title-de" type="text" placeholder="{{ __('video.title_de') }}" value="{{ old('title_de' ) }}" aria-required="true" />
                    @if ($errors->has('title_de'))
                    <br><span id="title-error" class="error text-danger" for="input-title-de">{{ $errors->first('title_de') }}</span>
                    @endif
                  </div>
                </div>
              </div>

            <div class="row">
                <label class="col-sm-2 col-form-label">{{ __('video.title') }}</label>
                <div class="col-sm-7">
                  <div class="form-group{{ $errors->has('title') ? ' has-danger' : '' }}">

                    <input class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}" name="title" id="input-title-de" type="text" placeholder="{{ __('video.title') }}" value="{{ old('title' ) }}" aria-required="true" />
                    @if ($errors->has('title'))
                    <br><span id="title-error" class="error text-danger" for="input-title-de">{{ $errors->first('title') }}</span>
                    @endif
                  </div>
                </div>
              </div>

              <div class="row">
                <label class="col-sm-2 col-form-label">{{ __('video.video') }}</label>
                <div class="col-sm-7">
                  @if ($errors->has('videoName'))
                  <br><span id="question-error" class="error text-danger" for="input-question-de">{{ $errors->first('videoName') }}</span>
                  @endif
                  <div class="item-wrapper one">
                    <div class="item">
                      <form data-validation="true" action="#" method="post">


                        <div class="item-inner">
                          <div class="item-content">
                            <div class=" "> <label style="cursor: pointer;" for="file_upload">

                                <div class="h-100">
                                  <div class="dplay-tbl">
                                    <div class="dplay-tbl-cell">

                                      <input style="display:none" type="file" name="videoName" id="file_upload" class="image-input" data-traget-resolution="image_resolution" value="">

                                      <span style="padding-right: 70px;" class="spanFileInput form-control inputFileVisible" placeholder="Single File">
                                        Select a Video
                                      </span>
                                      <span type="button" class="btn btn-fab btn-round btn-primary">
                                        <i class="material-icons">attach_file</i>
                                      </span>

                                    </div>
                                  </div>
                                </div>

                              </label> </div>
                          </div>
                          <!--item-content-->
                        </div>
                        <!--item-inner-->
                      </form>
                    </div>
                    <!--item-->
                  </div>

                </div>



              </div>


              <?php

              ?>


              <div class="card-footer ml-auto mr-auto">
                <button type="submit" id="videoUpload" class="btn btn-primary">{{ __('Save') }}</button>
              </div>
            </div>
        </form>
      </div>
    </div>
  </div>
</div>


@endsection

@push('js')

<script>
  $('#videoUpload').on('click', function() {
    $('#frmUploadVideo').submit();
  });
  $('#file_upload').change(function(e) {

    if (e.target.files.length) {
      $(this).next('.spanFileInput').html(e.target.files[0].name);
    }
  });
  jQuery(document).ready(function() {});
</script>
@endpush