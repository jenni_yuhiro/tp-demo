@extends('layouts.app', ['activePage' => 'user',  'activeMod' => 'user' ,  'titlePage' => __('user.management')])

@section('content')
<style>
  .fa {
    color: #DDD;
  }
  .page-link:hover {
    color: #9c27b0;
  }  
  .page-link {
    color: #9c27b0;
  }
  .page-item.active .page-link {
   
    background-color: #9c27b0;
    border-color: #9c27b0;
  }
</style>
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-primary">
            <h4 class="card-title ">Users</h4>
            <p class="card-category">{{trans('user.subHead') }}</p>
          </div>
          <div class="card-body">
            <div class="row">
              <div class="col-12 text-right">
                <!-- <input type="text" name="search" > 
                <a href="#" class="btn btn-sm btn-primary">Add user</a>-->
              </div>
            </div>
            <div class="table-responsive">
              <table class="table">
                <thead class=" text-primary">
                  <tr>
                    <th>

                      @sortablelink('name' , trans('user.name'))
                    </th>
                    <th>
                      @sortablelink('email' , trans('user.email'))
                    </th>
                    <th>
                      @sortablelink('role' , trans('user.role'))
                    </th>

                    <th class="text-primary "> @sortablelink('created_at' , trans('user.created_at'))
                    </th>

                    </th>
                    <th class="text-right">
                      Actions
                    </th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($users ?? '' as $user)
                  <tr>
                    <td>
                      {{$user->name}}
                    </td>
                    <td> {{$user->email}}
                    </td>
                    <td>
                      {{$user->role}}
                    </td>
                    <td>
                      {{$user->created_at}}
                    </td>
                    <td class="td-actions text-right">
                      <a rel="tooltip" class="btn btn-success btn-link" href="#" data-original-title="" title="">
                        <i class="material-icons">edit</i>
                        <div class="ripple-container"></div>
                      </a>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>

              <br>
              <ul class="pagination justify-content-center">
                {!! $users->appends(\Request::except('page'))->render() !!}
              </ul>


            </div>
          </div>
        </div>

      </div>
    </div>
    @endsection