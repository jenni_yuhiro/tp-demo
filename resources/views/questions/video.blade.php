@extends('layouts.app', ['activePage' => 'explanatory_video', 'activeMod' => 'questions' , 'titlePage' => __('questions.video_management')])

@section('content')
<style>
  .form-control,
  .custom-select {
    width: 80% !important;
    display: inline;
  }

  .btn-link {
    padding: 2px;
  }
</style>
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">

        <div class="card ">
          <form method="post" action="{{  route('uploadVideo')}}" id="frmUploadVideo" autocomplete="off" class="form-horizontal" enctype="multipart/form-data">
            @csrf
            @method('post')

            <div class="card-header card-header-primary">
              <h4 class="card-title">{{ __('questions.video_management') }}</h4>
              <p class="card-category">{{ __('questions.exvideo') }}</p>
            </div>
            <div class="card-body ">
              @if (session('status'))
              <div class="row">
                <div class="col-sm-12">
                  <div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <i class="material-icons">close</i>
                    </button>
                    <span> {{ session('status') }}</span>
                  </div>
                </div>
              </div>
              @endif
              <div class="row">
                <label class="col-sm-2 col-form-label">{{ __('quiz.category') }}</label>
                <div class="col-sm-7">
                  <div class="form-group{{ $errors->has('question') ? ' has-danger' : '' }}">

                    <select name="category_id" class="  browser-default custom-select">
                      @foreach( $categories as $key => $category)
                      <option value="{{$category->id}}">{{$category->name}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
              </div>

              <div class="row">


                <label class="col-sm-2 col-form-label">{{ __('questions.video') }}</label>
                <div class="col-sm-7">

                  <div class="item-wrapper one">
                    <div class="item">
                      <form data-validation="true" action="#" method="post">

                     
                        <div class="item-inner">
                          <div class="item-content">
                            <div class=" "> <label style="cursor: pointer;" for="file_upload">

                                <div class="h-100">
                                  <div class="dplay-tbl">
                                    <div class="dplay-tbl-cell">

                                      <input style="display:none" type="file" name="videoName" id="file_upload" class="image-input" data-traget-resolution="image_resolution" value="">

                                      <span style="padding-right: 70px;" class="spanFileInput form-control inputFileVisible" placeholder="Single File">
                                        Select a Video
                                      </span>
                                      <span type="button" class="btn btn-fab btn-round btn-primary">
                                        <i class="material-icons">attach_file</i>
                                      </span>

                                    </div>
                                  </div>
                                </div>

                              </label> </div>
                          </div>
                          <!--item-content-->
                        </div>
                        <!--item-inner-->
                      </form>
                    </div>
                    <!--item-->
                  </div>

                </div>
              </div>
            </div>
            <div class="card-footer ml-auto mr-auto">
              <button type="submit" id="videoUpload" class="btn btn-primary">{{ __('Save') }}</button>
            </div>
          </form>
        </div>
      </div>
    </div>

  @if($video  !== null)
    <div class="row">
      <div class="col-md-12">
        <div class="card ">
          <div class="card-header card-header-primary">
            <h4 class="card-title">{{ __('questions.exvideo') }}</h4>
          </div>
          <div class="card-body ">         
            <div class="row">
              <div class="col-sm-3"></div>
              <div class="col-sm-6">  

                <div class="embed-responsive embed-responsive-16by9 z-depth-1-half">
                  <iframe class="embed-responsive-item" src="{{ url('/') }}/{{$video->video}}" allowfullscreen></iframe>
                </div>
                <div class="col-sm-3"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  @endif
  @endsection
  @push('js')

  <script>
    $('#videoUpload').on('click', function() {
      $('#frmUploadVideo').submit();
    });
    $('#file_upload').change(function(e) {

      if (e.target.files.length) {
        $(this).next('.spanFileInput').html(e.target.files[0].name);
      }
    });
    jQuery(document).ready(function() {});
  </script>
  @endpush