@extends('layouts.app', ['activePage' => 'add_questions', 'activeMod' => 'questions' , 'titlePage' => __('questions.management')])

@section('content')
<style>
  .form-control,
  .custom-select {
    width: 80% !important;
    display: inline;
  }

  .btn-link {
    padding: 2px;
  }

  .ansDiv {
    border: 1px solid #AAA;
    padding: 10px;
  }

  .ansDivApp {
    border: 1px solid #AAA;
    border-top: none;
    padding: 10px;
  }
</style>
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <form method="post" action="{{  route('questions.store')}}" autocomplete="off" class="form-horizontal">
          @csrf
          @method('post')
          <div class="card ">
            <div class="card-header card-header-primary">
              <h4 class="card-title">{{ __('questions.addQuestion') }}</h4>
              <p class="card-category">{{ __('questions.questions_information') }}</p>
            </div>
            <div class="card-body ">
              @if (session('status'))
              <div class="row">
                <div class="col-sm-12">
                  <div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <i class="material-icons">close</i>
                    </button>
                    <span> {{ session('status') }}</span>
                  </div>
                </div>
              </div>
              @endif



              <div class="row">
                <label class="col-sm-2 col-form-label">{{ __('questions.user_type') }}</label>
                <div class="col-sm-7">
                  <div class="form-group{{ $errors->has('question') ? ' has-danger' : '' }}">
                    <select name="user_type" id="user_type" class="  browser-default custom-select">
                      @foreach(\App\Questions::userTypes() as $key => $category )
                      <option value="{{ $key }}">{{ $category }}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
              </div>


              <div class="row">
                <label class="col-sm-2 col-form-label">{{ __('questions.question_en') }}</label>
                <div class="col-sm-7">
                  <div class="form-group{{ $errors->has('question') ? ' has-danger' : '' }}">
                    <input class="form-control{{ $errors->has('question') ? ' is-invalid' : '' }}" name="question" id="input-question" type="text" placeholder="{{ __('questions.question_en') }}" value="{{ old('question' ) }}" aria-required="true" />
                    @if ($errors->has('question'))
                    <br><span id="question-error" class="error text-danger" for="input-question">{{ $errors->first('question') }}</span>
                    @endif
                  </div>
                </div>
              </div>

              <div class="row">
                <label class="col-sm-2 col-form-label">{{ __('questions.question_de') }}</label>
                <div class="col-sm-7">
                  <div class="form-group{{ $errors->has('question_de') ? ' has-danger' : '' }}">
                    <input class="form-control{{ $errors->has('question_de') ? ' is-invalid' : '' }}" name="question_de" id="input-question_de" type="text" placeholder="{{ __('questions.question_de') }}" value="{{ old('question_de' ) }}" aria-required="true" />
                    @if ($errors->has('question_de'))
                    <br><span id="question_de-error" class="error text-danger" for="input-question_de">{{ $errors->first('question_de') }}</span>
                    @endif
                  </div>
                </div>
              </div>

              <div class="row answersCls">
                <label class="col-sm-2 col-form-label">{{ __('questions.possible_answers') }}</label>
                   <?php $en_var = "en" ; ?>
                <div class="col-sm-7 inc ansDiv">
                  <div class=" controls form-group{{ $errors->has('answers') ? ' has-danger' : '' }}">
                    <input class=" form-control{{ $errors->has('answers') ? ' is-invalid' : '' }}" name="answers[{{$en_var}}][]" id="input-answers" type="text" placeholder="{{ __('questions.answer_en') }}" value="{{ old('answers.en.0' ) }}" />
                    <br>
                    <input class=" form-control{{ $errors->has('answers_de') ? ' is-invalid' : '' }}" name="answers[de][]" id="input-answers-de" type="text" placeholder="{{ __('questions.answer_de') }}" value="{{ old('answers.de.0' ) }}" />
                    <a rel="tooltip" class="btn btn-success btn-link" href="javascript:void(0);" data-original-title="" id="append" title="Add More Answers">
                      <i class="material-icons">add_circle</i>
                      <div class="ripple-container"></div>
                    </a>
                  </div>
                </div>     


                <!--
                  <div class="col-md-1 text-sm-left ">
                  <a rel="tooltip" class="btn btn-success btn-link" href="javascript:void(0);" data-original-title="" id="append"  title="Add More Answers">
                        <i class="material-icons">add_circle</i>  
                        <div class="ripple-container"></div>
                      </a>
                  </div>-->
              </div>
              <?php

             
              if ((old('answers')  !== null)) {
                foreach (old('answers')['en']  as $key => $val) {
                  if ($key != 0) {
                    $val_de =  old('answers')['de'][$key] ;
              ?>
                    <div class="row  answerApp">
                      <label class="col-sm-2 col-form-label"> </label>
                      <div class="col-sm-7   ">
                        <div class="controls form-group ">
                          <input class="form-control" type="text" name="answers[en][]" placeholder="{{ __('questions.answer_en') }}" value="{{$val}}"><br>
                          <input class="form-control" type="text" name="answers[de][]" placeholder="{{ __('questions.answer_de') }}" value="{{$val_de}}">
                          <a rel="tooltip" data-del-count="{{$key}}" class="btn btn-danger btn-link remove_this " href="javascript:void(0);" data-original-title="" title="">
                            <i class="material-icons">delete_forever</i>
                            <div class="ripple-container"></div>
                          </a> </div>
                      </div>
                    </div>

              <?php
                  }
                }
              }
              ?>


              <div class="card-footer ml-auto mr-auto">
                <button type="submit" class="btn btn-primary">{{ __('Save') }}</button>
              </div>
            </div>
        </form>
      </div>
    </div>
  </div>
</div>


@endsection
@push('js')

<script>
  jQuery(document).ready(function() {
    var answersCnt = 1; var de_var = "de"; var en_var = "en";
     var en_placeholder = "{{ __('questions.answer_en') }}";
     var de_placeholder = "{{ __('questions.answer_de') }}";
    jQuery(document).on('click', '#append', function(e) {
      answersCnt++;


      e.preventDefault();
      var html = ' <div class="row  answerApp "><label class="col-sm-2 col-form-label"> </label>\
                      <div class="col-sm-7  ansDivApp "><div class="controls form-group ">\
                <input class="form-control" type="text" name="answers['+en_var+'][]" placeholder="'+en_placeholder+'">\    <input class="form-control" type="text" name="answers['+de_var+'][]" placeholder="'+de_placeholder+'">\
                <a rel="tooltip" data-del-count="' + answersCnt + '" class="btn btn-danger btn-link remove_this " href="javascript:void(0);" data-original-title="" title="">\
                  <i class="material-icons">delete_forever</i><div class="ripple-container"></div></a>  </div> </div></div>';
      //  $(".answers").append();


      if ($('.answerApp').length > 0) {

        $(html).insertAfter(".answerApp:last");

      } else {
        $(html).insertAfter(".answersCls");
      }

      return false;
    });

    jQuery(document).on('click', '.remove_this', function() {


      jQuery(this).parent().parent().parent().remove();



      return false;
    });
    $("input[type=submit]").click(function(e) {
      e.preventDefault();
      $(this).next("[name=textbox]")
        .val(
          $.map($(".answersCls :text"), function(el) {
            return el.value
          }).join(",\n")
        )
    })
  });
</script>
@endpush