@extends('layouts.app-frontend', ['activePage' => 'dashboard', 'titlePage' => __('Dashboard')])

@section('content')

<style>
 

  #user_details,
  #user_type_details {
    margin: 2px;
  }

  /* media css */
  @media screen and (max-width: 991.98px) {
    .stepsWrapper {
      height: 600px !important;
    }


    .multisteps-form__progress-btn {
      width: 0%;
      text-align: center;

    }

    .wizard-topper {
      position: relative !important;
      top: -2%;
      left: 10% !important;
      width: 80% !important;
      margin-bottom: 26px;
    }

  }

  /* media css ends  */

  .contentArea {
    height: auto;
  }

  .multisteps-form__panel ::-webkit-scrollbar {
    width: 10px;
    padding-right: 134px;
  }

  /* width */
  .stepsWrapper1::-webkit-scrollbar {
    width: 10px;
    padding-right: 134px;
  }

  /* Track */
  .stepsWrapper1::-webkit-scrollbar-track {
    box-shadow: inset 0 0 5px grey;
    border-radius: 10px;
  }

  /* Handle */
  .stepsWrapper1::-webkit-scrollbar-thumb {
    background: #F5F7F8;
    border-radius: 10px;

  }

  /* Handle on hover */
  .stepsWrapper1::-webkit-scrollbar-thumb:hover {
    background: grey;
  }

  .wizard-v3-progress {
    background-color: #263ea5 !important;
    background-image: url(../img/bg.jpg) !important;
    background-size: cover !important;
    bottom: -19px !important;
    width: 375px !important;
    right: -441px !important;
  }

  .wizard-topper {
    position: relative;
    top: -2%;
    left: 75%;
    width: 30%;
  }

  .wizard-topper .wizard-progress .progress .progress-bar {
    top: 2px;
    left: 2px;
    width: 50%;
    height: 18px;
    position: absolute;
    border-radius: 80px;
    background-color: #2244A5 !important;
    /*
    background-color: #5756a2; */
  }

  .wizard-topper .wizard-progress .progress {
    height: 27px;
    padding: 2px;
    position: relative;
    border-radius: 80px;
    border: 2px solid #e3e3e3;
    background-color: transparent;
  }

  /*
 .wizard-v3-progress{
  background-color: #263ea5 !important;
    background-image: url(../img/bg.jpg) !important;
    background-size: cover!important;
    right: 0px !important;
   
  
 }
.multisteps-form__progress{
  padding-bottom: 20px !important;
} */

  .social-media-find .social-find-caret:after,
  .select-caret:after,
  .wizard-note-subject .select-option-area:after {
    top: 54px;
    right: 34px;

  }

  .input-custom-class {
    width: 100% !important;
    height: 50px !important;
    border: 2px solid #AAA !important;
    border-radius: 15px;
    padding-left: 20px;
  }

  .wrapper {
    margin-top: -10px !important;
  }

  .flagActive {
    border: 5px solid #45ea01;
    border-radius: 25px;
  }


  .wizard-form-input select {
    height: 50px;
  }

  .wizard-form-field .wizard-form-input label {
    text-align: left !important;
  }

  .wizard-form-input {

    display: inline-block !important;
  }

  .inner-button-cls {
    height: 55px;
    width: 180px;
    color: #fff;
    display: block;
    font-size: 18px;
    font-weight: 500;
    border-radius: 0px;
    line-height: 55px;
    text-align: center;
    background-color: #72bb4c !important;
    cursor: pointer;
    border-color: #72bb4c !important;
    position: absolute;
    right: 50px;

  }

  
 
  .loading {
    display: inline-block;
    width: 50px;
    height: 50px;
    border: 3px solid rgba(35, 75, 161, .3);
    border-radius: 50%;
    border-top-color: #fff;
    animation: spin 1s ease-in-out infinite;
    -webkit-animation: spin 1s ease-in-out infinite;
    position: relative;
    text-align: center;
    left: 50%;

  }

  .w-15 {
    width: 0% !important;
  }

  .w-85 {
    width: 100% !important;
  }

  .wizard-form-field {
    margin-right: 10%;
    margin-left: 10% !important;
  }


  
</style>
 

<div class="wrapper wizard d-flex clearfix multisteps-form position-relative ">

<div class="  w-15 stepsWrapperDel" style="display: none;">
    <div class="multisteps-form__progress">
      <div>
        <span class="multisteps-form__progress-btn  js-active " title="">  </span>
        <span class="multisteps-form__progress-btn    "><i class=" far fa-user"></i><span>{{ __('frontend.user_type_lbl') }}</span></span>
        <span class="multisteps-form__progress-btn " title="{{ __('frontend.user_type_lbl') }}"><i class=" far fa-user"></i><span>{{ __('frontend.user_type_lbl') }}</span></span>
        <span class="multisteps-form__progress-btn " title="{{ __('frontend.user_type_lbl') }}"><i class=" far fa-user"></i><span>{{ __('frontend.user_type_lbl') }}</span></span>
 
    
      </div>
    </div>
  </div>
 


  <form class="multisteps-form__form w-85 order-1 stepsWrapper" method="POST" action="#" autocomplete="off" id="frmquestioniar">
    @csrf

    <div class="form-area position-relative">
      <!-- div 1 -->
      <div class="multisteps-form__panel    js-active   " data-animation="slideHorz">
        <div class="wizard-forms section-padding">

          <div class="row ">
            <div class="col-sm-2 "></div>
            <div class="col-sm-8 text-center">
              <div class="mb-80 contentArea">
                <div class="row " style="margin-bottom: 100px;;">
                  <div class="col-sm-12  text-center">
                  <img src="{{ url('/') }}/images/Welcome.jpg">

                  </div>
                
                </div>
                <div class="row ">
                <div class="col-sm-12  text-center">
                  Haben Sie bereits ein Online-Zertifikat oder einen gültigen
Besucherausweis?</div>

                </div>

              </div>
            </div>
          </div>

          <div class="inner pb-100 clearfix">
            <div class="row ">
              <div class="col-sm-2 "></div>
              <div class="col-sm-8 text-center">
                <div class="actions" style="width: 25%;">
                  <ul>
                    <li><span class="js-btn-next" title="{{ __('frontend.checkin_now') }} ">Jetzt einchecken </span></li>
                  </ul>
                </div>
              </div>

           
            </div>
            <div class="col-sm-2 "></div>
          </div>
        </div>
        <!-- /.inner -->

        <div class="actions">
          <!--<ul>
              <li><span class="js-btn-next" title="NEXT" id="language_sel">{{ __('frontend.next') }} <i class="fa fa-arrow-right"></i></span></li>
            </ul>-->
        </div>
      </div>
    </div>
    <!-- div 1 ends -->

    <!-- div--2 -->

    <div class="multisteps-form__panel     " data-animation="slideHorz">
      <div class="wizard-forms section-padding" id="user_details_section">
        <div class="inner  clearfix">
          <div class="wizard-title text-center">
            <h3>{{ __('frontend.user_type') }}</h3>

          </div>

          <div class="wizard-form-field mb-85 contentArea">


            <div class="row ">

              <div class="col-sm-6 text-center ">
                <img width="70%" data-value="visitor" src="{{ url('/') }}/images/visitor_icon.jpg">
                <div class="actions userGroupDiv d-flex justify-content-center " data-user-group="visitor">
                  <ul>
                    <li><span class="js-btn-next user-group-span" title="{{ __('frontend.visitor') }} ">visitor </span></li>
                  </ul>
                </div>
              </div>
              <div class="col-sm-6 text-center ">
                <img width="70%" data-value="foreign_worker" src="{{ url('/') }}/images/foreign-worker-icon.jpg">
                <div class="actions   userGroupDiv d-flex justify-content-center" data-user-group="foreign_worker">
                  <ul>
                    <li><span class="js-btn-next user-group-span" title="{{ __('frontend.foreign_worker') }} ">fremdarbeiter</span></li>
                  </ul>
                </div>
              </div>

            </div>

          </div>
        </div>
        <!-- /.inner -->

        <div class="action-div actions  ">
          <ul>

            <li><span class="js-btn-prev neu_starten" title="neu_starten"> {{ __('frontend.neu_starten') }} </span></li>

            <li><span class="js-btn-prev" title="BACK"> {{ __('frontend.back') }} </span></li>
          </ul>
        </div>
      </div>
    </div>

    <!-- div 2 ends -->


    <!-- div-- 3-->

     

    <!-- div 1 -->


    <!-- div-- 3-->

    

    <!-- div 1 -->



    <!-- div 3 -->
    <!-- /.inner -->

    <!-- div 3 -->
 
    

 
    <!-- div 4 -->
    <div class="multisteps-form__panel  " data-animation="slideHorz">
      <div class="wizard-forms" id="review_div" style=" ">
        <div class="inner pb-200 clearfix">
          <div class="wizard-title text-center">
            <h3>{{ __('frontend.review') }}</h3>
            <div id="review_area">
            </div>
          </div>
        </div>
        <!-- /.inner -->

        <div class="actions">
          <ul>
            <!--<li><span class="js-btn-prev" title="BACK"><i class="fa fa-arrow-left"></i> {{ __('frontend.back') }} </span></li>-->
            <li><button title="NEXT" onclick="printFunc();return false;"> {{ __('frontend.print') }} </button></li>
          </ul>
          </ul>
        </div>
      </div>
    </div>
    <!-- div 5 -->
</div>
 
</form>



</div>


<!-----------------Confirmation ---------->

<!-- Modal -->
<div class="modal fade" id="videoConfirmation" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="videoModalLabel">{{ __('frontend.video_confirmation') }}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        {!! __('frontend.video_confirmation_txt') !!}
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal"> {{ __('frontend.close') }}</button>
      </div>
    </div>
  </div>
</div>

<!-----------------Confirmation ---------->

<!-- Modal -->
<div class="modal fade" id="selectConfirmation" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">{{ __('frontend.quiz_failed_confirmation') }}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        {!! __('frontend.quiz_failed_confirmation_txt') !!}
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal"> {{ __('frontend.close') }}</button>
        <button type="button" class="btn btn-primary" id="selectConfirmBtn" data-cat_Id=""> {{ __('frontend.yes') }}</button>
      </div>
    </div>
  </div>
</div>


<!-----------------Confirmation End ---------->


@endsection

@push('js')

<script>
  /********************************** */ 
  /********************************** */

  /********************************** */
  function printFunc() {

    var divToPrint = document.getElementById("review_area");
    // console.log(divToPrint.outerHTML);
    newWin = window.open("");
    newWin.document.write(divToPrint.outerHTML);
    newWin.print();
    // newWin.close();
    // similar behavior as an HTTP redirect

    window.location.replace("{{URL::to('/')}}");
    return false;
  }

  /********************************** */

 

  /********************************** */
 

  /********************************** */


  /********************************** */
 
  /**************************** */
  /**************************** */
 


  /*************  *  ******* ** ********** */
  $('.neu_starten').on('click', function() {
    window.location.href = "{{url('/')}}/";
  });
  /************************** */
 



  /***************************** */
  $('#btnSubmit').on('click', function() {
    $('#frmquestioniar').submit();
  });

  

 
  
</script>
@endpush