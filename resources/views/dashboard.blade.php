@extends('layouts.app', ['activePage' => 'dashboard',  'activeMod' => 'dashboard' , 'titlePage' => __('Dashboard')])

@section('content')
<div class="content">
  <div class="container-fluid">
    <div class="card">
      <div class="card-header card-header-primary">
        <h4 class="card-title">TP Checkin Portal</h4>
        <p class="card-category">Checkin Portal</p>
      </div>
      <div class="card-body">
        <div id="typography">
          <div class="card-title">
            <h2>{{ __('common.dashboard') }}</h2>
          </div>
          <div class="row">
            <div class="tim-typo">
              <h1>
                <span class="tim-note"> </span>  {{ __('common.welcome_text') }} </h1>
            </div>   
       
            
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection