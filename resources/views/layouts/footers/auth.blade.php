<footer class="footer">
  <div class="container-fluid">
     
    <div class="copyright float-right">
      &copy;
      <script>
        document.write(new Date().getFullYear())
      </script> 
      ,TP checkin Portal
    </div>
  </div>
</footer>