<div class="sidebar" data-color="orange" data-background-color="white" data-image="{{ asset('material') }}/img/sidebar-3.jpg">
  <!--
      Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

      Tip 2: you can also add an image using data-image tag
  -->
  <div class="logo">
    <a href="#" class="simple-text logo-normal">
      {{ __('TP Checkin Portal') }}
    </a>
  </div>
  <div class="sidebar-wrapper">
    <ul class="nav">
      <li class="nav-item{{ $activePage == 'dashboard' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('home') }}">
          <i class="material-icons">dashboard</i>
            <p>{{ __('common.dashboard') }}</p>
        </a>
      </li>
   <!--
      <li class="nav-item{{ $activePage == 'user' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('user.index') }}">
          <i class="material-icons">person</i>
          <span class="sidebar-normal"> {{ __('user.management') }} </span>
        </a>
      </li>
-->
    
      <li class="nav-item{{ $activePage == 'questions' ? ' active' : '' }}">
        <a class="nav-link"  data-toggle="collapse" href="#questionNav"   href="{{ route('questions.index') }}">
          <i class="material-icons">question_answer</i> 
          <p>{{ __('questions.management') }}  <b class="caret"></b></p>
        </a>
     
        <div class="collapse  {{ $activeMod == 'questions' ? ' show' : '' }}  " id="questionNav">
          <ul class="nav">
            <li class="nav-item{{ $activePage == 'questions' ? ' active' : '' }}">
              <a class="nav-link" href="{{ route('questions.index') }}">
                <span class="sidebar-mini">     <i class="material-icons">navigate_next</i> </span>
                <span class="sidebar-normal">{{ __('questions.questions') }} </span>
              </a>
            </li>
            <li class="nav-item{{ $activePage == 'add_questions' ? ' active' : '' }}">
              <a class="nav-link" href="{{ route('questions.create') }}">
                <span class="sidebar-mini">   <i class="material-icons">navigate_next</i>  </span>
                <span class="sidebar-normal"> {{ __('questions.addQuestion') }} </span>
              </a>
            </li>
                

          </ul>
        </div>
      </li>

      <li class="nav-item{{ $activePage == 'categories' ? ' active' : '' }}">
        <a class="nav-link  "  data-toggle="collapse" href="#categoriesNav"   href="{{ route('categories.index') }}">
          <i class="material-icons">category</i> 
          <p>{{ __('categories.management') }}<b class="caret"></b></p>
        </a>
     
        <div class="collapse  {{ $activeMod == 'categories' ? ' show' : '' }}  " id="categoriesNav">
          <ul class="nav">
            <li class="nav-item{{ $activePage == 'categories' ? ' active' : '' }}">
              <a class="nav-link" href="{{ route('categories.index') }}">
                <span class="sidebar-mini">     <i class="material-icons">navigate_next</i> </span>
                <span class="sidebar-normal">{{ __('categories.categories') }} </span>
              </a>
            </li>
            <li class="nav-item{{ $activePage == 'add_categories' ? ' active' : '' }}">
              <a class="nav-link" href="{{ route('categories.create') }}">
                <span class="sidebar-mini">   <i class="material-icons">navigate_next</i>  </span>
                <span class="sidebar-normal"> {{ __('categories.addCategory') }} </span>
              </a>
            </li>          
          </ul>
        </div>
      </li>


      <li class="nav-item{{ $activePage == 'info' ? ' active' : '' }}">
        <a class="nav-link"  data-toggle="collapse" href="#infoNav"   href="{{ route('information.index') }}">
          <i class="material-icons">info</i> 
          <p>{{ __('info.management') }}  <b class="caret"></b></p>
        </a>
          <div class="collapse  {{ $activeMod == 'info' ? ' show' : '' }}  " id="infoNav">
          <ul class="nav">
          <li class="nav-item{{ $activePage == 'information' ? ' active' : '' }}">
              <a class="nav-link" href="{{ route('information.index') }}">
                <span class="sidebar-mini">     <i class="material-icons">navigate_next</i> </span>
                <span class="sidebar-normal">{{ __('info.info') }} </span>
              </a>
            </li>
          <li class="nav-item{{ $activePage == 'add_info' ? ' active' : '' }}">
              <a class="nav-link" href="{{ route('information.create') }}">
                <span class="sidebar-mini">   <i class="material-icons">navigate_next</i>  </span>
                <span class="sidebar-normal"> {{ __('info.addInfo') }} </span>
              </a>
            </li>                      
           </ul>
        </div>
      </li>


      <li class="nav-item{{ $activePage == 'quiz' ? ' active' : '' }}">
        <a class="nav-link"  data-toggle="collapse" href="#quiznNav"   href="{{ route('quiz.index') }}">
          <i class="material-icons">question_answer</i> 
          <p>{{ __('quiz.management') }}  <b class="caret"></b></p>
        </a>
     <div class="collapse  {{ $activeMod == 'quiz' ? ' show' : '' }}  " id="quiznNav">
          <ul class="nav">
            <li class="nav-item{{ $activePage == 'quiz' ? ' active' : '' }}">
              <a class="nav-link" href="{{ route('quiz.index') }}">
                <span class="sidebar-mini">     <i class="material-icons">navigate_next</i> </span>
                <span class="sidebar-normal">{{ __('quiz.quiz') }} </span>
              </a>
            </li>
            <li class="nav-item{{ $activePage == 'add_quiz' ? ' active' : '' }}">
              <a class="nav-link" href="{{ route('quiz.create') }}">
                <span class="sidebar-mini">   <i class="material-icons">navigate_next</i>  </span>
                <span class="sidebar-normal"> {{ __('quiz.addQuestion') }} </span>
              </a>
            </li> 
            <!--        
            <li class="nav-item{{ $activePage == 'explanatory_video' ? ' active' : '' }}">
              <a class="nav-link" href="{{ route('explanatoryVideo') }}">
                <span class="sidebar-mini">   <i class="material-icons">navigate_next</i>  </span>
                <span class="sidebar-normal"> {{ __('questions.exvideo') }} </span>
              </a>
            </li>   -->  
          </ul>
        </div>
      </li>
     
      <li class="nav-item{{ $activePage == 'video' ? ' active' : '' }}">
        <a class="nav-link"  data-toggle="collapse" href="#videoNav"   href="{{ route('video.index') }}">
          <i class="material-icons">video_library</i> 
          <p>{{ __('video.management') }}  <b class="caret"></b></p>
        </a>
     
        <div class="collapse  {{ $activeMod == 'video' ? ' show' : '' }}  " id="videoNav">
          <ul class="nav">
            <li class="nav-item{{ $activePage == 'video' ? ' active' : '' }}">
              <a class="nav-link" href="{{ route('video.index') }}">
                <span class="sidebar-mini">     <i class="material-icons">navigate_next</i> </span>
                <span class="sidebar-normal">{{ __('video.video') }} </span>
              </a>
            </li>
            <li class="nav-item{{ $activePage == 'add_video' ? ' active' : '' }}">
              <a class="nav-link" href="{{ route('video.create') }}">
                <span class="sidebar-mini">   <i class="material-icons">navigate_next</i>  </span>
                <span class="sidebar-normal"> {{ __('video.addVideo') }} </span>
              </a>
            </li> 
                    
          
          </ul>
        </div>
      </li>

    </ul>
  </div>
</div>