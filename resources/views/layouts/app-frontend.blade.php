<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ __('Securitas Checkin Portal') }}</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="{{ asset('material') }}/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{ asset('material') }}/assets/css/animate.min.css">
    <link rel="stylesheet" href="{{ asset('material') }}/assets/css/fontawesome-all.css">
    <link rel="stylesheet" href="{{ asset('material') }}/assets/css/style.css">

    <link rel="stylesheet" type="text/css" href="{{ asset('material') }}/assets/css/colors/switch.css">
    <!-- Color Alternatives 
    <link href="{{ asset('material') }}/assets/css/colors/color-2.css" rel="alternate stylesheet" type="text/css" title="color-2">
    <link href="{{ asset('material') }}/assets/css/colors/color-3.css" rel="alternate stylesheet" type="text/css" title="color-3">
    <link href="{{ asset('material') }}/assets/css/colors/color-4.css" rel="alternate stylesheet" type="text/css" title="color-4">
    <link href="{{ asset('material') }}/assets/css/colors/color-5.css" rel="alternate stylesheet" type="text/css" title="color-5">
    -->
    <style>
      
    </style>

</head>

<body>

    @include('layouts.page_templates.guest')

    <script src="{{ asset('material') }}/assets/js/jquery-3.3.1.min.js"></script>
    <script src="{{ asset('material') }}/assets/js/popper.min.js"></script>
    <script src="{{ asset('material') }}/assets/js/bootstrap.min.js"></script>
    <script src="{{ asset('material') }}/assets/js/slick.min.js"></script>
    <script src="{{ asset('material') }}/assets/js/main.js"></script>
    <script src="{{ asset('material') }}/assets/js/switch.js"></script>
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('#profile-image')
                        .attr('src', e.target.result)
                        .width(150)
                        .height(200);
                };

                reader.readAsDataURL(input.files[0]);
            }
        };
        $("#customFile").change(function() {
            filename = this.files[0].name
        });
    </script>
    <script src="{{ asset('material') }}/js/checkin-portal.js"></script>

    <script>


    </script>
    @stack('js')
</body>

</html>