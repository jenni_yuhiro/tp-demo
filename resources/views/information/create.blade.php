@extends('layouts.app', ['activePage' => 'add_info', 'activeMod' => 'info' , 'titlePage' => __('info.management')])

@section('content')
<style>
  .form-control,
  .custom-select {
    width: 80% !important;
    display: inline;
  }

  .btn-link {
    padding: 2px;
  }
</style>
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <form method="post" action="{{  route('information.store')}}" autocomplete="off" class="form-horizontal" enctype="multipart/form-data">
          @csrf
          @method('post')
          <div class="card ">
            <div class="card-header card-header-primary">
              <h4 class="card-title">{{ __('info.addInfo') }}</h4>
              <p class="card-category">{{ __('info.infos_information') }}</p>
            </div>
            <div class="card-body ">
              @if (session('status'))
              <div class="row">
                <div class="col-sm-12">
                  <div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <i class="material-icons">close</i>
                    </button>
                    <span> {{ session('status') }}</span>
                  </div>
                </div>
              </div>
              @endif
              <div class="row">
                <label class="col-sm-2 col-form-label">{{ __('questions.user_type') }}</label>
                <div class="col-sm-7">
                  <div class="form-group{{ $errors->has('user_group') ? ' has-danger' : '' }}">

                    <select name="user_group" id="user_group" class="   browser-default custom-select   ">
                      <option value="">{{ __('common.select_user_group') }}</option>
                      @foreach(\App\Questions::userTypes() as $key => $category )
                      <option value="{{ $key }}">{{ $category }}</option>
                      @endforeach
                    </select>
                    @if ($errors->has('user_group'))
                    <br><span id="user_group-error" class="error text-danger" for="input-user_group">{{ $errors->first('user_group') }}</span>
                    @endif
                  </div>
                </div>
              </div>
              <div class="row">
                <label class="col-sm-2 col-form-label">{{ __('info.category') }}</label>
                <div class="col-sm-7">
                  <div class="form-group{{ $errors->has('category') ? ' has-danger' : '' }}">

                    <select name="category" class="  browser-default custom-select">
                      <option value="">{{ __('common.select_category') }}</option>
                      @foreach( $categories as $key => $category)
                      <option value="{{$category->id}}">{{$category->name}}</option>
                      @endforeach
                    </select>
                    @if ($errors->has('category'))
                    <br><span id="category-error" class="error text-danger" for="input-category">{{ $errors->first('category') }}</span>
                    @endif
                  </div>
                </div>
              </div>

              <div class="row">
                <label class="col-sm-2 col-form-label">{{ __('info.page') }}</label>
                <div class="col-sm-7">
                  <div class="form-group{{ $errors->has('page') ? ' has-danger' : '' }}">
                    <select name="page" class="  browser-default custom-select">

                      <option value="1">Page 1</option>
                      <option value="2">Page 2</option>
                      <option value="3">Page 3</option>
                    </select>
                    @if ($errors->has('page'))
                    <br><span id="page-error" class="error text-danger" for="input-page">{{ $errors->first('page') }}</span>
                    @endif
                  </div>
                </div>
              </div>


              <div class="row">
                <label class="col-sm-2 col-form-label">{{ __('info.info_de') }}</label>
                <div class="col-sm-7">
                  <div class="form-group{{ $errors->has('info_de') ? ' has-danger' : '' }}">

                    <input class="form-control{{ $errors->has('info_de') ? ' is-invalid' : '' }}" name="info_de" id="input-info-de" type="text" placeholder="{{ __('info.info_de') }}" value="{{ old('info_de' ) }}" aria-required="true" />
                    @if ($errors->has('info_de'))
                    <br><span id="info-error" class="error text-danger" for="input-info-de">{{ $errors->first('info_de') }}</span>
                    @endif
                  </div>
                </div>
              </div>
              <div class="row">
                <label class="col-sm-2 col-form-label">{{ __('info.info_en') }}</label>
                <div class="col-sm-7">
                  <div class="form-group{{ $errors->has('info') ? ' has-danger' : '' }}">
                    <input class="form-control{{ $errors->has('info') ? ' is-invalid' : '' }}" name="info" id="input-info" type="text" placeholder="{{ __('info.info_en') }}" value="{{ old('info' ) }}" aria-required="true" />
                    @if ($errors->has('info'))
                    <br><span id="info-error" class="error text-danger" for="input-info">{{ $errors->first('info') }}</span>
                    @endif
                  </div>
                </div>
              </div>
              <!-------------------->
              <div class="row">
                <label class="col-sm-2 col-form-label">{{ __('info.image') }}</label>
                <div class="col-sm-7">
                  <div class="file-field">
                    <div class="btn btn-primary btn-sm float-left">
                      <input type="file" name="image">
                    </div>
                  </div>
                </div>
              </div>
              <!--------------------->
              <div id="desc_div">

              </div>
              <!--------------------->
              <div class="row">
                <label class="col-sm-2 col-form-label">{{ __('info.info_desc_de') }}</label>
                <div class="col-sm-7">
                  <div class="form-group{{ $errors->has('info_desc_de') ? ' has-danger' : '' }}">
                    <textarea name="info_desc_de" id="info_desc_de" class=" editor " rows="10" columns="20"></textarea>
                    @if ($errors->has('info_desc_de'))
                    <br><span id="info_desc-error" class="error text-danger" for="input-info_desc">{{ $errors->first('info_desc_de') }}</span>
                    @endif
                  </div>
                </div>
              </div>
              <!--------------------->

              <!--------------------->

              <div class="row">
                <label class="col-sm-2 col-form-label">{{ __('info.info_desc') }}</label>
                <div class="col-sm-7">
                  <div class="form-group{{ $errors->has('info_desc') ? ' has-danger' : '' }}">
                    <textarea name="info_desc" id="info_desc" class=" editor " rows="10" columns="20"></textarea>
                    @if ($errors->has('info_desc'))
                    <br><span id="info_desc-error" class="error text-danger" for="input-info_desc">{{ $errors->first('info_desc') }}</span>
                    @endif
                  </div>
                </div>
              </div>
              <!--------------------->
              <div class="card-footer ml-auto mr-auto">
                <button type="submit" class="btn btn-primary">{{ __('Save') }}</button>
              </div>
            </div>
        </form>
      </div>
    </div>
  </div>
</div>
<?php /*

 <script src="https://cdn.tiny.cloud/1/osfkdt1ojkxd15ilmt85047w28nolsxwtioti6d8nen6tz64/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
 
*/
?>

@endsection
@push('js')
<!-- include libraries(jQuery, bootstrap)  
 
 -->
<script src="{{ URL::to('Libraries/ckeditor/ckeditor.js') }}"></script>
<script>
  window.onload = function() {
   
      $('.editor').each(function(e) {

            CKEDITOR.replace(this.name, {

              filebrowserUploadUrl: '{{ route("upload",["_token" => csrf_token() ]) }}',
              filebrowserUploadMethod: 'form',

              // Define the toolbar groups as it is a more accessible solution.
              toolbarGroups: [{
                  "name": "basicstyles",
                  "groups": ["basicstyles"]
                },
                {
                  "name": "links",
                  "groups": ["links"]
                },
                {
                  "name": "paragraph",
                  "groups": ["list", "blocks"]
                },
                {
                  "name": "document",
                  "groups": ["mode"]
                },
                {
                  "name": "insert",
                  "groups": ["insert"]
                },
                {
                  "name": "styles",
                  "groups": ["styles"]
                },
                {
                  "name": "about",
                  "groups": ["about"]
                }
              ],
              // Remove the redundant buttons from toolbar groups defined above.
              removeButtons: 'Print,ExportPdf,Strike,Subscript,Superscript,Anchor,Styles,Specialchar'
            });
          });
          };
</script>



<script>
  $(document).ready(function() {


    //  $('.summernote').summernote();

    var token = "{{ csrf_token() }}"; // for passing with ajax calls
    $.ajax({
      type: "POST",
      dataType: "json",
      url: "{{ url('admin/get-info-desc-html') }}",
      data: {

        _token: token
      },
      success: function(response) {
        console.log(response);
        if (response.status == "success") {

          $('#desc_divf').html(response.html);

          // console.log(response);
        } else {
          // console.log(response);
        }
      }
    });

  });

  /********************************** */

  jQuery(document).ready(function() {
    var answersCnt = 1;
    jQuery(document).on('click', '#append', function(e) {
      answersCnt++;


      e.preventDefault();
      var html = ' <div class="row  answerApp "><label class="col-sm-2 col-form-label"> </label>\
                      <div class="col-sm-7   "><div class="controls form-group ">\
                <input class="form-control" type="text" name="answers[]" placeholder="Answer">\
                <a rel="tooltip" data-del-count="' + answersCnt + '" class="btn btn-danger btn-link remove_this " href="javascript:void(0);" data-original-title="" title="">\
                  <i class="material-icons">delete_forever</i><div class="ripple-container"></div></a>  </div> </div></div>';
      //  $(".answers").append();


      if ($('.answerApp').length > 0) {

        $(html).insertAfter(".answerApp:last");

      } else {
        $(html).insertAfter(".answersCls");
      }

      return false;
    });

    jQuery(document).on('click', '.remove_this', function() {


      jQuery(this).parent().parent().parent().remove();



      return false;
    });
    $("input[type=submit]").click(function(e) {
      e.preventDefault();
      $(this).next("[name=textbox]")
        .val(
          $.map($(".answersCls :text"), function(el) {
            return el.value
          }).join(",\n")
        )
    })
  });
</script>
@endpush