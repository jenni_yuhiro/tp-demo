@extends('layouts.app', ['activePage' => 'add_info', 'activeMod' => 'info' , 'titlePage' => __('info.management')])

@section('content')
<style>
  .form-control,
  .custom-select {
    width: 80% !important;
    display: inline;
  }

  .btn-link {
    padding: 2px;
  }
</style>
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <form method="post" action="{{  route('information.store')}}" autocomplete="off" class="form-horizontal">
          @csrf
          @method('post')
          <div class="card ">
            <div class="card-header card-header-primary">
              <h4 class="card-title">{{ __('info.addInfo') }}</h4>
              <p class="card-category">{{ __('info.infos_information') }}</p>
            </div>
            <div class="card-body ">
              @if (session('status'))
              <div class="row">
                <div class="col-sm-12">
                  <div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <i class="material-icons">close</i>
                    </button>
                    <span> {{ session('status') }}</span>
                  </div>
                </div>
              </div>
              @endif

              <div class="row">
                <label class="col-sm-2 col-form-label">{{ __('info.category') }}</label>
                <div class="col-sm-7">
                  <div class="form-group{{ $errors->has('category') ? ' has-danger' : '' }}">

                    <select name="category" class="  browser-default custom-select">
                      @foreach( $categories as $key => $category)
                      <option value="{{$category->id}}">{{$category->name}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
              </div>

              <div class="row">
                <label class="col-sm-2 col-form-label">{{ __('info.info_de') }}</label>
                <div class="col-sm-7">
                  <div class="form-group{{ $errors->has('info_de') ? ' has-danger' : '' }}">

                    <input class="form-control{{ $errors->has('info_de') ? ' is-invalid' : '' }}" name="info_de" id="input-info-de" type="text" placeholder="{{ __('info de') }}" value="{{ old('info_de' ) }}" aria-required="true" />
                    @if ($errors->has('info_de'))
                    <br><span id="info-error" class="error text-danger" for="input-info-de">{{ $errors->first('info_de') }}</span>
                    @endif
                  </div>
                </div>
              </div>
              <div class="row">
                <label class="col-sm-2 col-form-label">{{ __('info.info') }}</label>
                <div class="col-sm-7">
                  <div class="form-group{{ $errors->has('info') ? ' has-danger' : '' }}">

                    <input class="form-control{{ $errors->has('info') ? ' is-invalid' : '' }}" name="info" id="input-info" type="text" placeholder="{{ __('info') }}" value="{{ old('info' ) }}" aria-required="true" />
                    @if ($errors->has('info'))
                    <br><span id="info-error" class="error text-danger" for="input-info">{{ $errors->first('info') }}</span>
                    @endif
                  </div>
                </div>
              </div>


              <!-------------------->
              <div class="row">
                <label class="col-sm-2 col-form-label">{{ __('info.image') }}</label>
                <div class="col-sm-7">




                  <div class="file-field">
                    <div class="btn btn-primary btn-sm float-left">

                      <input type="file" name="image">
                    </div>

                  </div>


                </div>
              </div>
              <!--------------------->
              <div class="row">
                <label class="col-sm-2 col-form-label">{{ __('info.info_desc_de') }}</label>
                <div class="col-sm-7">
                  <div class="form-group{{ $errors->has('info_desc_de') ? ' has-danger' : '' }}">
                    <textarea name="info_desc_de" class="tinymce-editor" rows="10" columns="20"></textarea>
                    @if ($errors->has('info_desc_de'))
                    <br><span id="info_desc-error" class="error text-danger" for="input-info_desc">{{ $errors->first('info_desc_de') }}</span>
                    @endif
                  </div>
                </div>
              </div>
              <!--------------------->

              <!--------------------->

              <div class="row">
                <label class="col-sm-2 col-form-label">{{ __('info.info_desc') }}</label>
                <div class="col-sm-7">
                  <div class="form-group{{ $errors->has('info_desc') ? ' has-danger' : '' }}">
                    <textarea name="info_desc" class="tinymce-editor" rows="10" columns="20"></textarea>
                    @if ($errors->has('info_desc'))
                    <br><span id="info_desc-error" class="error text-danger" for="input-info_desc">{{ $errors->first('info_desc') }}</span>
                    @endif
                  </div>
                </div>
              </div>
              <!--------------------->
              <div class="card-footer ml-auto mr-auto">
                <button type="submit" class="btn btn-primary">{{ __('Save') }}</button>
              </div>
            </div>
        </form>
      </div>
    </div>
  </div>
</div>


@endsection
@push('js')
<script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<script type="text/javascript">
  tinymce.init({
    selector: 'textarea.tinymce-editor',
    height: 300,
    menubar: false,
    plugins: [
      'advlist autolink lists link image charmap print preview anchor',
      'searchreplace visualblocks code fullscreen',
      'insertdatetime media table paste code help wordcount'
    ],
    toolbar: 'undo redo | formatselect | ' +
      'bold italic backcolor | alignleft aligncenter ' +
      'alignright alignjustify | bullist numlist outdent indent | ' +
      'removeformat | help',
    content_css: '//www.tiny.cloud/css/codepen.min.css'
  });
</script>
<script>
  jQuery(document).ready(function() {
    var answersCnt = 1;
    jQuery(document).on('click', '#append', function(e) {
      answersCnt++;


      e.preventDefault();
      var html = ' <div class="row  answerApp "><label class="col-sm-2 col-form-label"> </label>\
                      <div class="col-sm-7   "><div class="controls form-group ">\
                <input class="form-control" type="text" name="answers[]" placeholder="Answer">\
                <a rel="tooltip" data-del-count="' + answersCnt + '" class="btn btn-danger btn-link remove_this " href="javascript:void(0);" data-original-title="" title="">\
                  <i class="material-icons">delete_forever</i><div class="ripple-container"></div></a>  </div> </div></div>';
      //  $(".answers").append();


      if ($('.answerApp').length > 0) {

        $(html).insertAfter(".answerApp:last");

      } else {
        $(html).insertAfter(".answersCls");
      }

      return false;
    });

    jQuery(document).on('click', '.remove_this', function() {


      jQuery(this).parent().parent().parent().remove();



      return false;
    });
    $("input[type=submit]").click(function(e) {
      e.preventDefault();
      $(this).next("[name=textbox]")
        .val(
          $.map($(".answersCls :text"), function(el) {
            return el.value
          }).join(",\n")
        )
    })
  });
</script>
@endpush