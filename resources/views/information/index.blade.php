@extends('layouts.app', ['activePage' => 'information', 'activeMod' => 'info' , 'titlePage' => __('info.management')])

@section('content')
<style>
  [type="search"] {
    border: 1px solid #ccc;
  }

  [name="datatable-question_length"] {
    border: 1px solid #ccc;
    padding: 5px;
  }

  table.dataTable.no-footer {

    border-bottom: 1px solid #ccc;
  }

  table.dataTable thead th,
  table.dataTable tfoot th {
    font-weight: 300;
  }
</style>
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-primary">
            <h4 class="card-title ">{{__('info.info')}}</h4>
            <p class="card-category">{{__('info.subHead')}}</p>
          </div>
          <div class="card-body">
            @if (session('message'))
            <div class="row">
              <div class="col-sm-12">
                <div class="alert alert-success">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <i class="material-icons">close</i>
                  </button>
                  <span> {{ session('message') }}</span>
                </div>
              </div>
            </div>
            @endif
            <div class="row">
              <div class="col-12 text-right">
                <a href="{{ route('information.create') }}" class="btn btn-sm btn-primary"> {{__('info.addInfo')}}</a>
              </div>
            </div>
            <div class="table-responsive">

              <table class="table   dataTable no-footer" id="datatable-question" role="grid" aria-describedby="datatable-editable_info">

                <thead class=" text-primary">
                  <tr role="row">
                    <th width="30px">#</th>
                    <th style=" ">{{ __('questions.user_type') }} </th>
                    <th style=" ">{{ __('info.categories') }} </th>
                    <th style=" ">{{ __('info.info_de') }} </th>
                    <th style=" ">{{ __('info.info_en') }} </th>
                    <th class="text-right" width="20%"> {{ __('info.actions') }} </th>
                  </tr>
                </thead>
                <tbody id="tablecontents">


                  @foreach($info as $key => $qui)
                  <tr class="row1" data-id="{{$qui->id}}">
                    <td class="pl-3"><i class="fa fa-sort"></i></td>
                    <td>
                      {{ \App\Questions::userTypes()[$qui->user_group]  ?? '' }}
                    </td>
                    <td>
                      {{$qui->categories->name}}
                    </td>
                    <td> {{$qui->info_de}} </td>
                    <td> {{$qui->info}} </td>
                    <td class="td-actions text-right">

                      <a rel="tooltip" class="btn btn-primary addQuizBtn" href="javascript:void(0)" data-queid="{{$qui->id}}" data-original-title="" title="{{__('quiz.addQuestion')}}">
                        {{__('quiz.addQuestion')}}
                      </a>

                      <a rel="tooltip" class="btn btn-success btn-link deleteBtn" href="#" data-queid="{{$qui->id}}" data-original-title="" title="Delete">
                        <i class="material-icons">delete</i>
                        <div class="ripple-container"></div>
                      </a>
                      <a rel="tooltip" class="btn btn-success btn-link" href="{{ route('information.edit' ,$qui->id ) }}" data-original-title="" title="">
                        <i class="material-icons">edit</i>
                        <div class="ripple-container"></div>
                      </a>
                    </td>
                  </tr>
                  @endforeach


                </tbody>
              </table>
            </div>
          </div>
        </div>

      </div>
    </div>

    <!-----------------Add quiz ---------->
    <!-- Modal -->
    <div class="modal fade" id="addQuizContainer" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel"> {{__('quiz.addQuestion')}}</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>

          <div class="alert alert-danger print-error-msg" style="margin:10px; display:none">
            <ul></ul>
          </div>

          <div class="alert alert-success print-success-msg" style="margin:10px; display:none">

          </div>
          <form id="frmaddQuiz" method="post">
            <div class="modal-body ">
              <div class="row">
                <label class="col-sm-3 col-form-label">{{ __('questions.user_type') }}</label>
                <div class="col-sm-9">
                  <div class="form-group ">

                    <input class="form-control " id="modal_user_group" readonly type="text" value="" aria-required="true" />

                  </div>
                </div>
              </div>
              <div class="row">
                <label class="col-sm-3 col-form-label">{{ __('questions.category') }}</label>
                <div class="col-sm-9">
                  <div class="form-group ">
                    <input class="form-control " id="modal_category" readonly type="text" value="" aria-required="true" />

                  </div>
                </div>
              </div>
              <div class="row">
                <label class="col-sm-3 col-form-label">{{ __('quiz.question_de') }}</label>
                <div class="col-sm-9">
                  <div class="form-group{{ $errors->has('question') ? ' has-danger' : '' }}">

                    <input class="form-control{{ $errors->has('question_de') ? ' is-invalid' : '' }}" name="question_de" id="input-question-de" type="text" placeholder="{{ __('quiz.question_de') }}" value="{{ old('question_de' ) }}" aria-required="true" />
                    @if ($errors->has('question'))
                    <br><span id="question-error" class="error text-danger" for="input-question-de">{{ $errors->first('question_de') }}</span>
                    @endif
                  </div>
                </div>
              </div>
              <div class="row">
                <label class="col-sm-3 col-form-label">{{ __('quiz.question') }}</label>
                <div class="col-sm-9">
                  <div class="form-group{{ $errors->has('question') ? ' has-danger' : '' }}">

                    <input class="form-control{{ $errors->has('question') ? ' is-invalid' : '' }}" name="question" id="input-question" type="text" placeholder="{{ __('quiz.question') }}" value="{{ old('question' ) }}" aria-required="true" />
                    @if ($errors->has('question'))
                    <br><span id="question-error" class="error text-danger" for="input-question">{{ $errors->first('question') }}</span>
                    @endif
                  </div>
                </div>
              </div>
              <div class="row answersCls">
                <label class="col-sm-3 col-form-label">{{ __('quiz.answer') }}</label>
                <div class="col-sm-9 inc ">

                  <div>
                    <div class="form-group md-radio">
                      <input class="form-radio-input" id="answer1" type="radio" value="1" name="answer" checked>
                      <label for="answer1">Yes</label>
                    </div>
                    <div class="md-radio">
                      <input class="form-radio-input " id="answer2" type="radio" name="answer" value="0">
                      <label for="answer2">No</label>
                    </div>
                  </div>

                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('common.cancel_confirm_lbl') }}</button>
              <button type="button" class="btn btn-primary" id="addQuizConfirmBtn" data-question_id="">{{ __('common.save') }}</button>
            </div>
          </form>
        </div>
      </div>
    </div>

    <!-----------------Add quiz  End ---------->

    <!-----------------Confirmation ---------->



    <!-- Modal -->
    <div class="modal fade" id="selectConfirmation" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel"> {{ __('quiz.delete_title') }}</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            {{ __('quiz.delete_confirmation') }}
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('common.cancel_confirm_lbl') }}</button>
            <button type="button" class="btn btn-primary" id="selectConfirmBtn" data-question_id="">{{ __('common.confirm_lbl') }}</button>
          </div>
        </div>
      </div>
    </div>

    <!-----------------Confirmation End ---------->
    @endsection

    @push('js')

    <script>
      $(document).on("click", "#addQuizConfirmBtn", function() {
        $(".print-success-msg").css('display', 'none');
        $(".print-error-msg").css('display', 'none');
        $.ajax({
          type: "POST",
          dataType: "json",
          url: "{{ url('admin/addQuiz') }}",
          data: {
            data: $('#frmaddQuiz').serialize() + '&qId=' + $(this).attr('data-question_id'),
            _token: $('meta[name="csrf-token"]').attr('content'),
          },
          success: function(response) {

            if ($.isEmptyObject(response.errors)) {
              if (response.status == "success") {
                printSuccessMsg("{{__('quiz.created_success')}}");
                $('#input-question-de').val('');
                $('#input-question').val('');
              }
            } else {
              printErrorMsg(response.errors);
            }


          }
        });

      });



      function printSuccessMsg(msg) {
        $(".print-success-msg").html('');
        $(".print-success-msg").css('display', 'block');

        $(".print-success-msg").html(' ' + msg + ' ');

      }


      function printErrorMsg(msg) {
        $(".print-error-msg").find("ul").html('');
        $(".print-error-msg").css('display', 'block');
        $.each(msg, function(key, value) {
          $(".print-error-msg").find("ul").append('<li>' + value + '</li>');
        });
      }


      $(document).on("click", ".addQuizBtn", function() {
        $('.alertDiv').remove();
        $('#modal_category').val('');
        $('#modal_user_group').val('');
        $('#addQuizConfirmBtn').attr('data-question_id', $(this).data('queid'));
        $.ajax({
          type: "POST",
          dataType: "json",
          url: "{{ url('admin/info-details') }}",
          data: {
            queid: $(this).data('queid'),
            _token: $('meta[name="csrf-token"]').attr('content'),
          },
          success: function(response) {

            if (response.status == "success") {
              $('#modal_category').val(response.categories.name);
              $('#modal_user_group').val(response.user_group);

            } else {
              //  console.log(response);
            }
          }
        });

        $('#addQuizContainer').modal();
        $('#addQuizConfirmBtn').attr('data-question_id', $(this).data('queid'));
      });


      $(document).on("click", ".deleteBtn", function() {
        $('.alertDiv').remove();
        $('#selectConfirmation').modal();
        $('#selectConfirmBtn').attr('data-question_id', $(this).data('queid'));
      });

      $(document).on("click", "#selectConfirmBtn", function() {

        $('#selectConfirmation').modal();
        deleteQuestion($(this).attr('data-question_id'));
      });


      function deleteQuestion(Id) {
        var url = "{{ url('admin/information') }}/" + Id;
        var token = $('meta[name="csrf-token"]').attr('content');
        $.ajax({
          type: "DELETE",
          dataType: "json",
          url: url,
          data: {
            _token: token
          },
          success: function(response) {
            $('#selectConfirmation').modal('toggle');
            $('.card-body').prepend(' <div class="row alertDiv">\
              <div class="col-sm-12">\
                <div class="alert alert-success">\
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">\
                    <i class="material-icons">close</i>\
                  </button>\
                  <span> ' + response.message + '</span>\
                </div>              </div>            </div>');

            $('tr[data-Id="' + Id + '"').remove();

          }
        });




        //sendOrderToServer();

      }
      $(function() {
        $("#datatable-question").DataTable({
          "order": [
            [1, 'asc']
          ],
          "aoColumnDefs": [{
            "bSortable": false,
            "aTargets": [0, 5]
          }, ],
          "language": {
            "sEmptyTable": "Keine Daten in der Tabelle vorhanden",
            "sInfo": "_START_ bis _END_ von _TOTAL_ Einträgen",
            "sInfoEmpty": "0 bis 0 von 0 Einträgen",
            "sInfoFiltered": "(gefiltert von _MAX_ Einträgen)",
            "sInfoPostFix": "",
            "sInfoThousands": ".",
            "sLengthMenu": "_MENU_ Einträge anzeigen",
            "sLoadingRecords": "Wird geladen...",
            "sProcessing": "Bitte warten...",
            "sSearch": "Suchen",
            "sZeroRecords": "Keine Einträge vorhanden.",
            "oPaginate": {
              "sFirst": "Erste",
              "sPrevious": "Zurück",
              "sNext": "Nächste",
              "sLast": "Letzte"
            },
            "oAria": {
              "sSortAscending": ": aktivieren, um Spalte aufsteigend zu sortieren",
              "sSortDescending": ": aktivieren, um Spalte absteigend zu sortieren"
            }
          }
        });

        $("#tablecontents").sortable({
          items: "tr",
          cursor: 'move',
          opacity: 0.6,
          update: function() {
            sendOrderToServer();
          }
        });

        function sendOrderToServer() {
          var order = [];
          var token = $('meta[name="csrf-token"]').attr('content');
          $('tr.row1').each(function(index, element) {
            order.push({
              id: $(this).attr('data-id'),
              position: index + 1
            });
          });

          $.ajax({
            type: "POST",
            dataType: "json",
            url: "{{ url('admin/info-sortable') }}",
            data: {
              order: order,
              _token: token
            },
            success: function(response) {
              if (response.status == "success") {
                console.log(response);
              } else {
                console.log(response);
              }
            }
          });
        }
      });
    </script>
    @endpush