@extends('layouts.app-frontend', ['activePage' => 'dashboard', 'titlePage' => __('Dashboard')])

@section('content')
<style>
body {

background-image: url("{{ url('/') }}/images/Securitas_Hintergrund.jpg");
}
</style>
<div class="wrapper wizard d-flex clearfix multisteps-form position-relative ">

 


  <form class="multisteps-form__form w-85 order-1 stepsWrapper" method="POST" action="#" autocomplete="off" id="frmquestioniar">
    @csrf




    <div class="form-area position-relative">

      <!-- div 1 -->
      <div class="multisteps-form__panel   js-active  " data-animation="slideHorz">
        <div class="wizard-forms section-padding">


          <div class="inner pb-100 clearfix">
            <div class="wizard-title text-center">           
              <h3 style="font-size: 36px;">{!! __('frontend.language_head') !!}</h3>
              <p> </p>
            </div>


            <div class="row ">
              <div class="col-sm-12 text-center">
                <div class="mb-70 contentArea">
                  <div class="row ">
                    <div class="col-sm-4">                 

                    </div>
                    <div class="col-sm-2">
                      <img class="languageCls" data-value="de" width="100px" src="{{ url('/') }}/images/Button_DE_idle.png">
                    </div>
                    <div class="col-sm-2">
                      <img class="languageCls" data-value="en" width="100px" src="{{ url('/') }}/images/Button_EN_idle.png">
                    </div>
                    <input type="hidden" name="language" id="language" value="de" />

                    <div class="col-sm-4"></div>
                  </div>
                  <div class="row ">
                    <div class="col-sm-12 text-center">
                      <h4>{{ __('frontend.language_footer') }}</h4>
                      <h5>{{ __('frontend.languageSelect') }}</h5>
                      
                    </div>
                  </div>
                </div>
              </div>
            </div>

          </div>
          <!-- /.inner -->

        </div>
      </div>     



 

      <div class="multisteps-form__panel  " data-animation="slideHorz">
        <div class="wizard-forms section-padding">

          <div class="inner  clearfix" id="usersection-div-intro">
            <div class="wizard-title text-center">
              <h3>{{ __('frontend.short_intro') }}</h3>
            </div>
            <div class="wizard-form-field mb-85 contentArea  ">
              <div class="row short_instructions" style="">
                {{ __('frontend.short_instructions') }}              

              </div>
             
            </div>
          </div> 

        </div>
      </div>  
      <div class="multisteps-form__panel  " data-animation="slideHorz">
        <div class="wizard-forms section-padding">

          <div class="inner  clearfix" id="usersection-div-intro">
            <div class="wizard-title text-center">
              <h3></h3>
            </div>
            <div class="wizard-form-field mb-85 contentArea  ">
              <div class="row short_instructions" style="">    &nbsp;


                <div class="actions" style="margin-top: 80px;">
                  <ul>
                    <li><span class="js-btn-next" title="{{ __('frontend.understand_btn') }}">{{ __('frontend.understand_btn') }}</span></li>
                  </ul>
                </div>

              </div>
            </div>
          </div> 

        </div>
      </div> 


    </div> 
  </form>
</div>
@endsection
@push('js')

<script>
  /************************ */
 
 
  $('.languageCls').on('click', function() {

    if ($(this).data('value') == 'de') {
      $(this).attr("src", "{{ url('/') }}/images/Button_DE_pressed.png");
    } else {
      $(this).attr("src", "{{ url('/') }}/images/Button_EN_pressed.png");
    }
    $('#language').val($(this).data('value'))

    var token = "{{ csrf_token() }}"; // for passing with ajax calls
 
    var language = $('#language').val();
   // window.location.href = "{{url('locale')}}/" + language + "?tab=2";
   $('#frmquestioniar').attr('action', "{{url('/')}}/" +language);
       $('#frmquestioniar').submit();
     // $('#tab_2').trigger('click');
  });

   

  /********************************** */ 
</script>


@endpush