@extends('layouts.app-frontend', ['activePage' => 'dashboard', 'titlePage' => __('Dashboard')])

@section('content')

<style>
  .pb-30 {
    padding-bottom: 30px;
  }

  #user_details,
  #user_type_details {
    margin: 2px;
  }

  /* media css */
  @media screen and (max-width: 991.98px) {
    .stepsWrapper {
      height: 600px !important;
    }


    .multisteps-form__progress-btn {
      width: 0%;
      text-align: center;

    }

    .wizard-topper {
      position: relative !important;
      top: -2%;
      left: 10% !important;
      width: 80% !important;
      margin-bottom: 26px;
    }

  }

  /* media css ends  */

  .contentArea {
    height: auto;
  }

  .multisteps-form__panel ::-webkit-scrollbar {
    width: 10px;
    padding-right: 134px;
  }

  /* width */
  .stepsWrapper1::-webkit-scrollbar {
    width: 10px;
    padding-right: 134px;
  }

  /* Track */
  .stepsWrapper1::-webkit-scrollbar-track {
    box-shadow: inset 0 0 5px grey;
    border-radius: 10px;
  }

  /* Handle */
  .stepsWrapper1::-webkit-scrollbar-thumb {
    background: #F5F7F8;
    border-radius: 10px;

  }

  /* Handle on hover */
  .stepsWrapper1::-webkit-scrollbar-thumb:hover {
    background: grey;
  }

  .wizard-v3-progress {
    background-color: #263ea5 !important;
    background-image: url(../img/bg.jpg) !important;
    background-size: cover !important;
    bottom: -19px !important;
    width: 375px !important;
    right: -441px !important;
  }

  .wizard-topper {
    position: relative;
    top: -2%;
    left: 75%;
    width: 30%;
  }

  .wizard-topper .wizard-progress .progress .progress-bar {
    top: 2px;
    left: 2px;
    width: 50%;
    height: 18px;
    position: absolute;
    border-radius: 80px;
    background-color: #2244A5 !important;
    /*
    background-color: #5756a2; */
  }

  .wizard-topper .wizard-progress .progress {
    height: 27px;
    padding: 2px;
    position: relative;
    border-radius: 80px;
    border: 2px solid #e3e3e3;
    background-color: transparent;
  }

  /*
 .wizard-v3-progress{
  background-color: #263ea5 !important;
    background-image: url(../img/bg.jpg) !important;
    background-size: cover!important;
    right: 0px !important;
   
  
 }
.multisteps-form__progress{
  padding-bottom: 20px !important;
} */

  .social-media-find .social-find-caret:after,
  .select-caret:after,
  .wizard-note-subject .select-option-area:after {
    top: 54px;
    right: 34px;

  }

  .input-custom-class {
    width: 100% !important;
    height: 50px !important;
    border: 2px solid #AAA !important;
    border-radius: 15px;
    padding-left: 20px;
  }

  .wrapper {
    margin-top: -10px !important;
  }

  .flagActive {
    border: 5px solid #45ea01;
    border-radius: 25px;
  }


  .wizard-form-input select {
    height: 50px;
  }

  .wizard-form-field .wizard-form-input label {
    text-align: left !important;
  }

  .wizard-form-input {

    display: inline-block !important;
  }

  .inner-button-cls {
    height: 55px;
    width: 180px;
    color: #fff;
    display: block;
    font-size: 18px;
    font-weight: 500;
    border-radius: 0px;
    line-height: 55px;
    text-align: center;
    background-color: #72bb4c !important;
    cursor: pointer;
    border-color: #72bb4c !important;
    position: absolute;
    right: 50px;

  }

  table {
    width: 100%;

    border-collapse: collapse;
    border-spacing: 0px;
  }

  table td {
    border: 3px solid #b4d4e4;
    padding: 8px 8px;
  }

  .languageCls {
    margin-bottom: 20px;
  }

  .loading {
    display: inline-block;
    width: 50px;
    height: 50px;
    border: 3px solid rgba(35, 75, 161, .3);
    border-radius: 50%;
    border-top-color: #fff;
    animation: spin 1s ease-in-out infinite;
    -webkit-animation: spin 1s ease-in-out infinite;
    position: relative;
    text-align: center;
    left: 50%;

  }

  .w-15 {
    width: 0% !important;
  }

  .w-85 {
    width: 100% !important;
  }

  .wizard-form-field {
    margin-right: 10%;
    margin-left: 10% !important;
  }


  @keyframes spin {
    to {
      -webkit-transform: rotate(360deg);
    }
  }

  @-webkit-keyframes spin {
    to {
      -webkit-transform: rotate(360deg);
    }
  }
</style>
@php
$total_tabs = $no_catergories + 5 ;
$progress_percent = round(100/$total_tabs+1) ;

$tab = isset(request()->route()->parameters['tab'])?request()->route()->parameters['tab']:'';
$user_group = isset(request()->route()->parameters['user_group'])? request()->route()->parameters['user_group']:'' ;

use App\Http\Controllers\FrontendController;
@endphp

@php $cat_i = 5;

@endphp

<div class="wrapper wizard d-flex clearfix multisteps-form position-relative ">


  <div class="  w-15 stepsWrapperDel" style="display: none;">
    <div class="multisteps-form__progress">
      <div>
        <span class="multisteps-form__progress-btn  js-active " title="{{ __('frontend.language') }}"><i class="far fa-user"></i><span>{{ __('frontend.language') }}</span></span>
        <span class="multisteps-form__progress-btn    "><i class=" far fa-user"></i><span>{{ __('frontend.user_type_lbl') }}</span></span>

        @if( $user_group == "foreign_worker")
        <span class="multisteps-form__progress-btn " title=""><span>user QA</span></span>
        @endif
        <span class="multisteps-form__progress-btn " title=""></i><span>{{ __('frontend.user_type_lbl') }}</span></span>
        <span class="multisteps-form__progress-btn " title=""></i><span>{{ __('frontend.user_type_lbl') }}</span></span>

        @foreach($questions as $key => $category)

        <span class="multisteps-form__progress-btn   " title='{{ FrontendController::getCategoryById($key)->category_name }}' style="pointer-events:none;"><i class="far fa-user"></i><span> {{ $key  }} === {{ FrontendController::getCategoryById($key)->category_name }}</span></span>

        @endforeach
        <span class="multisteps-form__progress-btn" title="{{ __('frontend.review') }}" style="pointer-events:none;"><i class="far fa-user"></i><span>{{ __('frontend.review') }}</span></span>

      </div>
    </div>
  </div>


  <form class="multisteps-form__form w-85 order-1 stepsWrapper" method="POST" action="#" autocomplete="off" id="frmquestioniar">
    @csrf

    <div class="form-area position-relative">
      <!-- div 1 -->
      <div class="multisteps-form__panel     @if($tab  !=  2  && $tab  !=  3  ) js-active  @endif " data-animation="slideHorz">
        <div class="wizard-forms section-padding">

          <div class="row ">
            <div class="col-sm-2 "></div>
            <div class="col-sm-8 text-center">
              <div class="mb-80 contentArea">
                <div class="row welcome-image">
                  <div class="col-sm-12  text-center">
                    <img src="{{ url('/') }}/images/Welcome.jpg">
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="inner pb-30 clearfix">
            <div class="row ">
              <div class="col-sm-2 "></div>
              <div class="col-sm-8 text-center">
                <div class="actions welcome-action">
                  <ul>
                    <li><span class="js-btn-next" title="{{ __('frontend.checkin_now') }} ">Jetzt&nbsp;einchecken </span></li>
                  </ul>
                </div>
              </div>

              <input type="hidden" name="language" id="language" value="de" />
            </div>
            <div class="col-sm-2 "></div>
          </div>
        </div>
        <!-- /.inner -->

        <div class="actions">
          <!--<ul>
              <li><span class="js-btn-next" title="NEXT" id="language_sel">{{ __('frontend.next') }} <i class="fa fa-arrow-right"></i></span></li>
            </ul>-->
        </div>
      </div>
    </div>
    <!-- div 1 ends -->

    <!-- div--2 -->

    <div class="multisteps-form__panel  @if($tab  ==  2 ) js-active  @endif " data-animation="slideHorz">
      <div class="wizard-forms section-padding" id="user_details_section">
        <div class="inner  clearfix">
          <div class="wizard-title text-center" style="    padding-bottom: 25px;">
            <h3>{{ __('frontend.user_type') }}</h3>

          </div>

          <div class="wizard-form-field   contentArea" style="margin-bottom:30px;">


            <div class="row ">

              <div class="col-sm-6 text-center ">
                <img width="60%" data-value="visitor" src="{{ url('/') }}/images/visitor_icon.jpg">
                <div class="actions userGroupDiv d-flex justify-content-center " data-user-group="visitor">
                  <ul>
                    <li><span class="js-btn-next user-group-span" title="{{ __('frontend.visitor') }} ">Besucher </span></li>
                  </ul>
                </div>
              </div>
              <div class="col-sm-6 text-center ">
                <img width="60%" data-value="foreign_worker" src="{{ url('/') }}/images/foreign-worker-icon.jpg">
                <div class="actions   userGroupDiv d-flex justify-content-center" data-user-group="foreign_worker">
                  <ul>
                    <li><span class="js-btn-next user-group-span" title="{{ __('frontend.foreign_worker') }} ">fremdarbeiter</span></li>
                  </ul>
                </div>
              </div>

            </div>

          </div>
        </div>
        <!-- /.inner -->

        <div class="action-div actions  ">
          <ul>

            <li><span class="js-btn-prev neu_starten" title="neu_starten"> {{ __('frontend.neu_starten') }} </span></li>

            <li><span class="js-btn-prev" title="BACK"> {{ __('frontend.back') }} </span></li>
          </ul>
        </div>
      </div>
    </div>

    <!-- div 2 ends -->

    <!-- div--2 -->
    @if( $user_group == "foreign_worker")
    <div class="multisteps-form__panel  @if($tab  ==  3 ) js-active  @endif " data-animation="slideHorz">
      <div class="wizard-forms section-padding" id="user_qa_section">
        <div class="inner  clearfix">
          <div class="wizard-title text-center" style="    padding-bottom: 25px;">
            <h3>{{ __('frontend.user_type') }}</h3>

          </div>

          <div class="wizard-form-field   contentArea" style="margin-bottom:30px;">


            <div class="row ">

              <div class="col-sm-12 text-center ">
                rtert
              </div>


            </div>

          </div>
        </div>
        <!-- /.inner -->

        <div class="action-div actions  ">
          <ul>

            <li><span class="js-btn-prev neu_starten" title="neu_starten"> {{ __('frontend.neu_starten') }} </span></li>

            <li><span class="js-btn-prev" title="BACK"> {{ __('frontend.back') }} </span></li>
          </ul>
        </div>
      </div>
    </div>

    <!-- div 2 ends -->
    @endif

    <!-- div-- 3-->

    <div class="multisteps-form__panel  @if($tab  ==  3 && $user_group != 'foreign_worker' ) js-active  @endif " data-animation="slideHorz">
      <div class="wizard-forms section-padding" id="user_details_section">
        <div class="inner  clearfix" id="usersection-div-hidden" style="max-height: 200px; display:none">

          <!--Big blue loading-->
          <div class="loading">

          </div>

        </div>
        <div class="inner pb-30 clearfix" id="usersection-div">
          <div class="wizard-title text-center">
            <h3>{{ __('frontend.contact_details') }}</h3>
          </div>
          <div class="wizard-form-field mb-85 contentArea  ">
            <div class="row ">
              <div class="wizard-form-input">
                <input type="hidden" name="user_type" value="{{$user_group}}">
                <input class="input-custom-class" name="surname" id="input-surname" placeholder="{{ __('frontend.surname') }} " value="@php echo isset($_POST['surname'])?$_POST['surname']:'' @endphp">


                <span id="surname-error" class="error text-danger" for="input-surname" style="display: none;"> {{ __('frontend.surname_error') }} </span>

              </div>

              <div class="wizard-form-input">
                <input class="input-custom-class" name="firstname" id="input-firstname" placeholder="{{ __('frontend.firstname') }} " value="@php echo isset($_POST['firstname'])?$_POST['firstname']:'' @endphp">
                <span id="firstname-error" class="error text-danger" for="input-firstname" style="display: none;"> {{ __('frontend.firstname_error') }} </span>

              </div>
              <div class="wizard-form-input">
                <input class="input-custom-class" name="company" id="input-company" placeholder="{{ __('frontend.company') }}" value="@php echo isset($_POST['company'])?$_POST['company']:'' @endphp">
              </div>
            </div>
          </div>
        </div>
        <!-- /.inner -->
        <div class="action-div actions user-details-actions actions-btn-three">
          <ul>
            <li><span class="js-btn-prev neu_starten" title="neu_starten"> {{ __('frontend.neu_starten') }} </span></li>
            <li><span class="js-btn-prev" title="BACK"> {{ __('frontend.back') }} </span></li>

            <li><span class="js-btn-next " title="NEXT" id="contact_details">{{ __('frontend.next') }} </span></li>

          </ul>
        </div>
      </div>
    </div>

    <!-- div 1 -->


    <!-- div-- 3-->

    <div class="multisteps-form__panel  " data-animation="slideHorz">
      <div class="wizard-forms section-padding">

        <div class="inner  clearfix" id="usersection-div">
          <div class="wizard-title text-center">
            <h3>{{ __('frontend.short_intro') }}</h3>
          </div>
          <div class="wizard-form-field mb-85 contentArea  ">
            <div class="row short_instructions" style="">

              {{ __('frontend.short_instructions') }}


              <div class="actions" style="margin-top: 80px;">
                <ul>
                  <li><span class="js-btn-next" title="{{ __('frontend.understand_btn') }}">{{ __('frontend.understand_btn') }}</span></li>
                </ul>
              </div>

            </div>
          </div>
        </div>
        <!-- /.inner -->
        <div class="action-div actions user-details-actions  ">
          <ul>
            <li><span class="js-btn-prev neu_starten" title="neu_starten"> {{ __('frontend.neu_starten') }} </span></li>


          </ul>
        </div>
      </div>
    </div>

    <!-- div 1 -->



    <!-- div 3 -->
    <!-- /.inner -->

    <!-- div 3 -->

    @php
    $temp_prev_id = 'user_details_section';
    @endphp
    @foreach($questions as $key => $category)
    <?php

    if ($cat_i  ==  2) $prev_btn_id = 'user_details_section';
    elseif ($cat_i >= 3) {
      $prev_btn_id = $temp_prev_id;

      $temp_prev_id =  $key;
    }


    ?>
    <div class="multisteps-form__panel    " data-animation="slideHorz">
      <div class="wizard-forms section-padding">




        <div class="inner pb-100 clearfix">
          <div class="wizard-title text-center" style="    padding-bottom: 50px;">
            <h3> {{ FrontendController::getCategoryById($key)->category_name }}</h3>
            <p> </p>
          </div>
          <div class="wizard-form-field mb-85 contentArea">
            <div id="{{$key}}">


              @foreach($category as $key_quiz => $quiz)
              <div id="{{$key}}_infos">
                @php $nextPage = $quiz->page + 1;


                @endphp
                @if($quiz->table_name == 'information' )


                <div style="  padding-bottom: 20px;" id="{{$key}}_step" data-page="{{$nextPage}}">
                  @if( $quiz->image != "")
                  @if(file_exists(public_path( $quiz->image )))
                  <img src="{{ url('/') }}/{{$quiz->image}}" width="100px">
                  @endif
                  @endif

                  <strong>{{$quiz->title}}</strong>
                  <p style="padding: 20px;;">
                    {!!$quiz->info_description!!}
                  </p>
                </div>
                @endif
              </div>

              @if($quiz->table_name == 'videos' )
              <div class="pb-100" id="{{$key}}_step" data-page="{{$nextPage}}">
                <div class="embed-responsive embed-responsive-16by9 ">
                  <video id="video_{{$key}}" class="video" controls="controls">
                    <source src="{{ url('/') }}/{{$quiz->title}}" type="video/mp4">
                  </video>
                </div>


              </div>
              @endif


              @endforeach
            </div>

            <div id="{{$key}}_content" style="display: none;">

            </div>

          </div>

          <!----------Rausgewaehlt // Fausgewaehlt ----------->
          <!--
            <div class="wizard-v3-progress">
              <span>{{$cat_i}} to {{$total_tabs}} {{ __('frontend.step') }}</span>
              <h3>
                @php $progress_per = ($cat_i-1) * $progress_percent; @endphp

                {{$progress_per}}% {{ __('frontend.to_complete') }}</h3>
              <div class="progress">
                <div class="progress-bar" style='width:{{$progress_per}}%'>
                </div>
              </div>
            </div>-->
        </div>
        <!-- ./inner -->

        <div class="actions  actions-btn-three " id="{{$key}}_btn">

          <ul>
            <li><span class="js-btn-prev neu_starten" title="neu_starten"> {{ __('frontend.neu_starten') }} </span></li>
            <li><span class="js-btn-prev" title="BACK" data-prev-container-id="{{$prev_btn_id }}"> {{ __('frontend.back') }} </span></li>
            <li><span class="js-btn-next nextEventCls @if( $total_tabs-1 == $cat_i ) nextReviewArea   @endif" data-cat_Id="{{$key}}" q_count="" title="NEXT">{{ __('frontend.next') }} </span></li>
          </ul>
        </div>
      </div>
    </div>



    @php $cat_i++ ; @endphp
    @endforeach
    <!-- div 4 -->
    <div class="multisteps-form__panel  " data-animation="slideHorz">
      <div class="wizard-forms" id="review_div">
        <div class="inner   clearfix">
          <div class="wizard-title text-center">
            <h3>{{ __('frontend.review') }}</h3>
            <div id="review_area" style="height:400px; ">
            </div>
          </div>
        </div>
        <!-- /.inner -->

        <div class="actions">
          <ul>
            <!--<li><span class="js-btn-prev" title="BACK"><i class="fa fa-arrow-left"></i> {{ __('frontend.back') }} </span></li>-->
            <li><button title="NEXT" onclick="printFunc();return false;"> {{ __('frontend.print') }} </button></li>
          </ul>
          </ul>
        </div>
      </div>
    </div>
    <!-- div 5 -->
</div>
<input type="hidden" name="no_of_cat" id="no_of_cat" value="{{$no_catergories}}" />
</form>



</div>


<!-----------------Confirmation ---------->

<!-- Modal -->
<div class="modal fade" id="videoConfirmation" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="videoModalLabel">{{ __('frontend.video_confirmation') }}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        {!! __('frontend.video_confirmation_txt') !!}
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal"> {{ __('frontend.close') }}</button>
      </div>
    </div>
  </div>
</div>

<!-----------------Confirmation ---------->

<!-- Modal -->
<div class="modal fade" id="selectConfirmation" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">{{ __('frontend.quiz_failed_confirmation') }}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        {!! __('frontend.quiz_failed_confirmation_txt') !!}
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal"> {{ __('frontend.close') }}</button>
        <button type="button" class="btn btn-primary" id="selectConfirmBtn" data-cat_Id=""> {{ __('frontend.yes') }}</button>
      </div>
    </div>
  </div>
</div>


<!-----------------Confirmation End ---------->


@endsection

@push('js')

<script>
  /********************************** */
  var click_review_btn = 0;
  var supposedCurrentTime = 0;
  var videoended = 0;

  $("video").on("timeupdate", function() {
    if (!this.seeking) {
      supposedCurrentTime = this.currentTime;
    }
  });
  // prevent user from seeking
  $("video").on('seeking', function() {


    // guard agains infinite recursion:
    // user seeks, seeking is fired, currentTime is modified, seeking is fired, current time is modified, ....
    var delta = this.currentTime - supposedCurrentTime;

    if (Math.abs(delta) > 0.01) {
      //console.log("Seeking is disabled");
      this.currentTime = supposedCurrentTime;
    }

  });

  $("video").on("ended", function() {
    // reset state in order to allow for rewind
    videoended = 1;
    supposedCurrentTime = 0;
  });


  /********************************** */

  /********************************** */
  function printFunc() {

    var divToPrint = document.getElementById("review_area");
    // console.log(divToPrint.outerHTML);
    newWin = window.open("");
    newWin.document.write(divToPrint.outerHTML);
    newWin.print();
    // newWin.close();
    // similar behavior as an HTTP redirect

    window.location.replace("{{URL::to('/')}}");
    return false;
  }

  /********************************** */


  var answers = {};
  var answers_count = {};


  function verifyQuiz(catId) {
    if (click_review_btn == 1) {
      click_review_btn = 2;
    }

    var token = "{{ csrf_token() }}"; // for passing with ajax calls
    var xhr = $.ajax({
      type: "POST",
      dataType: "json",
      url: "{{ url('information/verify-quiz') }}",
      data: {
        category_id: catId,
        quizCount: answers_count[catId],
        answers: answers,
        _token: token
      },
      async: false,
      //  success: handelData,
      //  error: handelData,

      error: function(data) {

        $('#selectConfirmation').modal();
        $('#selectConfirmBtn').attr('data-cat_Id', catId);
        click_review_btn = 0;
        //  }
        return false;
        // MessageBoxError('Error');
      }

    });
    //console.log(xhr);

    //  return jqXHR.responseText;

    return xhr.status;
  }


  /********************************** */

  $(document).on('click', '.js-btn-prev', function() {




    var btnId = $(this).parents('ul').parent('div').attr('id');
    var prevId = $(this).attr('data-prev-container-id'); //.show();
    var res = btnId.split("_");
    //$('#' + prevId + '_step').attr('data-page', 1);

    retryAnswer(prevId);

    retryAnswer(res[0]);
    //console.log(btnId);
    //checkMorePages(prevId); 
    // $('#'+prevId).show();
    // $('#'+prevId + '_content').html('');
    // window.location.replace("{{URL::to('/')}}");
  });

  /********************************** */


  /********************************** */

  function retryAnswer(catId) {
    click_review_btn = 0;
    $('.getQuizBtn').show();
    $('#' + catId + '').show();
    $('#' + catId + '_step').attr('data-page', 1);
    checkMorePages(catId);
    $('#' + catId + '_content').hide();


  }
  $(document).on('click', '#selectConfirmBtn', function() {

    retryAnswer($(this).attr('data-cat_Id'));
    $('#selectConfirmation').modal('hide');

  });
  /**************************** */
  /**************************** */

  $('.nextReviewArea').on('click', function() {

    if (click_review_btn != 0) {

      var data = $('form').serialize();
      var token = "{{ csrf_token() }}"; // for passing with ajax calls
      $.ajax({
        type: "POST",
        dataType: "json",
        url: "{{ url('information/review') }}",
        data: {
          data: data,
          _token: token
        },
        success: function(response) {
          if (response.status == "success") {
            $('#review_area').html(response.html);

          }
        }
      });
    }
    click_review_btn = 1;

  });
  /**************************** */


  /**************************** */
  var wrongAnsCnt = 0;
  $('.nextEventCls').on('click', function() {
    if ($("#" + $(this).attr('data-cat_Id') + "_content").is(':visible') == false) {

      if ($("video").parents(".js-active ").length == 1 && videoended == 0) {
        $('#videoConfirmation').modal();
        return false;
      }
    }

    var cat_id = $(this).attr('data-cat_Id');


    console.log($(this).attr('q_count'));

    if ($("#" + $(this).attr('data-cat_Id') + "_content").is(':visible') == true) {
      var tbl_id = $(this).parents('table').attr('data-tbl_cat_id');

      if ($(this).attr('q_count') != 0) {
        if (verifyQuiz(cat_id) == 400) {
          wrongAnsCnt++;
          if (wrongAnsCnt > 3) {
            window.location.href = "{{url('/3/')}}";
          }
          return false;
        } else {
          wrongAnsCnt = 0; // reinitialize
          return true;
        }
      } else {
        wrongAnsCnt = 0; // reinitialize
        return true;
      }
      return false;
    } else {

      if (checkMorePages(cat_id) == false) {

        return false;
      } else {

        getQuiz(cat_id);
        return false;
      }
    }

  });

  /**************************** */
  function checkMorePages(catId) {
    var next_page = $('#' + catId + '_step').attr('data-page');

    if (next_page != "") {
      var token = "{{ csrf_token() }}"; // for passing with ajax calls
      var xhr = $.ajax({
        type: "POST",
        dataType: "json",
        url: "{{ url('information/get-more-information') }}",
        data: {
          category_id: catId,
          page: next_page,
          user_group: "{{$user_group}}",
          _token: token
        },
        async: false,
        //  success: handelData,
        //  error: handelData,
        success: function(response) {

          if (response.status == "success") {

            $('#' + catId + '_infos').html(response.html);

          } else {
            // console.log(response);
          }
        },
        error: function(data) {


          // $('#selectConfirmation').modal();
          // $('#selectConfirmBtn').attr('data-cat_Id', catId);
          //  }
          return false;
          // MessageBoxError('Error');
        }
      });

      return xhr.responseJSON.getquize;
    }
  }
  /**************************** */
  function getQuiz(catId) {
    answers = {};
    answers_count = {};
    //  console.log(answers);


    //console.log( catId );
    $('#' + catId + '_content').html('');
    $('#' + catId + '_content').show();
    $('#' + catId + '').hide();
    var token = "{{ csrf_token() }}"; // for passing with ajax calls
    $.ajax({
      type: "POST",
      dataType: "json",
      url: "{{ url('information/get-quiz-details') }}",
      data: {
        category_id: catId,
        user_group: "{{$user_group}}",
        _token: token
      },
      success: function(response) {

        if (response.status == "success") {

          $('#' + catId + '_content').html(response.html);
          $('.getQuizBtn').hide();

          var qus_height = parseInt($('.stepsWrapper').height()) + parseInt($('#' + catId + '_content').height());

          $('.nextEventCls[data-cat_id="' + catId + '"]').attr('q_count', 1); // todo

          $('.stepsWrapper').height(qus_height);

          $(window).scrollTop(100);

          // console.log(response);
        } else {
          // console.log(response .status);
          $('.nextEventCls[data-cat_id="' + catId + '"]').attr('q_count', 0);
          $('.nextEventCls[data-cat_id="' + catId + '"]').trigger('click');
        }
      }
    });


  }



  /**************************** */

  $(document).on('click', '.optionsCls', function() {
    var td_id = $(this).parents('td').attr('id');
    var tbl_id = $(this).parents('table').attr('data-tbl_cat_id');
    var quizCount = $(this).parents('table').attr('data-quiz_count');
    // console.log(tbl_id);
    if (answers[tbl_id] == undefined) {
      // answers[tbl_id] =[];
    }
    answers_count[tbl_id] = quizCount;
    // answers[tbl_id][td_id] = $(this).attr('data-optionvalue');
    ///console.log(  answers );
    //console.log( answers[tbl_id][td_id]);
    answers[td_id] = $(this).attr('data-optionvalue');
    if ($(this).is(".optionsCls.option-no")) {

      $('#' + td_id).find('.option-yes').attr("src", "{{ url('/') }}/images/R.gif");
    }
    if ($(this).is(".optionsCls.option-yes")) {
      $('#' + td_id).find('.option-no').attr("src", "{{ url('/') }}/images/F.gif");
    }

    if ($(this).attr('data-optionValue') == 1) {
      var img1 = "{{ url('/') }}/images/R.gif",
        img2 = "{{ url('/') }}/images/Rausgewaehlt.gif";
    } else {
      var img1 = "{{ url('/') }}/images/F.gif",
        img2 = "{{ url('/') }}/images/Fausgewaehlt.gif";
    }
    $(this).attr("src", ($(this).attr('src') === img1) ? img2 : img1);
    // console.log($(this).attr('src'));
    // console.log(img1);
    if ($(this).attr('src') === img1) {
      delete answers[td_id];

    }
    // console.log(answers_count);
    // console.log(answers);
  });
  /************************** */


  /************************** */

  function change() {
    var img1 = "{{ url('/') }}/images/R.gif",
      img2 = "{{ url('/') }}/images/Rausgewaehlt.gif";
    var imgElement = document.getElementById('test');

    imgElement.src = (imgElement.src === img1) ? img2 : img1;
  }
  /************************** */
  $(document).on('click', '.user-details-fetch', function() {
    $('#user_details').show();
    if ($(window).width() <= 991) {
      $('#frmquestioniar').height($('#user_details_section').height() + 200);
    }
    $('.stepsWrapper').height($('#user_details_section').height() + 200);
    $('.user-details-actions').show();
    $('.user-details-fetch').hide();

    /************************** */


  });

  $(document).ready(function() {
    @if($user_group !== null)
    $("#user_type").trigger("change");
    $('#usersection-div-hidden').hide();
    $('#usersection-div').show();
    @endif
  });
  /************************** */
  $('#user_type').on('change', function() {

    var token = "{{ csrf_token() }}"; // for passing with ajax calls
    $.ajax({
      type: "POST",
      dataType: "json",
      url: "{{ url('question/user-type-details') }}",
      data: {
        user_type: $('#user_type').val(),
        _token: token
      },
      success: function(response) {
        if (response.status == "success") {
          $('#user_type_details').html(response.html);
          $('.user-details-fetch').trigger("click");
          if ($(window).width() <= 991) {
            $('#frmquestioniar').height($('#user_details_section').height() + 200);
          }
          $('.stepsWrapper').height($('#user_details_section').height() + 200);
        } else {
          //  console.log(response);
        }
      }
    });
  });
  /************************** */
  $('.neu_starten').on('click', function() {
    window.location.href = "{{url('/')}}/";
  });
  /************************** */

  $('#user_typed').on('change', function() {

    $('#usersection-div').hide();
    $('#usersection-div-hidden').show();
    var userGrp = "{{ $user_group}}";
    if (userGrp != $('#user_type').val()) {
      window.location.href = "{{url('/2/')}}/" + $('#user_type').val();
    }
    @if($user_group === null)
    window.location.href = "{{url('/2/')}}/" + $('#user_type').val();
    @else

    $('.error').hide();
    $('.user-details-actions').hide();
    $('#user_details').hide();



    var token = "{{ csrf_token() }}"; // for passing with ajax calls
    $.ajax({
      type: "POST",
      dataType: "json",
      url: "{{ url('question/user-type-details') }}",
      data: {
        user_type: $('#user_type').val(),
        _token: token
      },
      success: function(response) {
        if (response.status == "success") {
          $('#user_type_details').html(response.html);
          $('.user-details-fetch').trigger("click");
          if ($(window).width() <= 991) {
            $('#frmquestioniar').height($('#user_details_section').height() + 200);
          }
          $('.stepsWrapper').height($('#user_details_section').height() + 200);
        } else {
          //  console.log(response);
        }
      }
    });
    @endif
  });
  /************************ */
  $('#contact_details').click(function() {

    var res = true;
    // here I am checking for textFields, password fields, and any 
    // drop down you may have in the form
    $("input[name='surname'],input[name='firstname']  ").each(function() {
      if ($(this).val().trim() == "") {
        // console.log( $(this));
        $('#' + $(this).attr('name') + '-error').show();
        res = false;
      }
    });
    // $('.stepsWrapper').height(600);
    //  console.log(res);
    return res;

    /*  if (res == true) {
       $('#frmquestioniar').attr('action', "{{url('/3/')}}/" + $('#user_type').val());
       $('#frmquestioniar').submit();
       // window.location.href = "{{url('/3/')}}/" + $('#user_type').val();
     } */

  });


  $('#language_sel').click(function() {
    var token = "{{ csrf_token() }}"; // for passing with ajax calls
    $('#usersection-div-hidden').show();
    $('#usersection-div').hide();
    var language = $('#language').val();
    window.location.href = "{{url('locale')}}/" + language + "?tab=2";
  });

  $('.userGroupDiv').click(function() {
    //if ($(this).attr('data-user-group') != 'foreign_worker') {

    $('#frmquestioniar').attr('action', "{{url('/3/')}}/" + $(this).attr('data-user-group'));
    $('#frmquestioniar').submit();
    // }else{

    //  }

  });



  /***************************** */
  $('#btnSubmit').on('click', function() {
    $('#frmquestioniar').submit();
  });

  $('.languageCls').on('click', function() {
    $('.languageCls').removeClass('flagActive');
    $(this).addClass('flagActive');
    $('#language').val($(this).data('value'))

    var token = "{{ csrf_token() }}"; // for passing with ajax calls
    $('#usersection-div-hidden').show();
    $('#usersection-div').hide();
    var language = $('#language').val();
    window.location.href = "{{url('locale')}}/" + language + "?tab=2";

    // $('#tab_2').trigger('click');
  });

  $('#language').on('change', function() {
    $('#tab_2').trigger('click');

  });
  $('.questionBtn').on('click', function() {
    var nextTab = $(this).data('next-tab');
    $('#' + nextTab).trigger('click');


    if (nextTab == 'tab_4') {
      $('#name_summary').text($('#input-name').val());
      $('#email_summary').text($('#input-email').val());
      $('#company_summary').text($('#input-company').val());

    }
  });
</script>
@endpush