@extends('layouts.app-frontend', ['activePage' => 'dashboard', 'titlePage' => __('Dashboard')])

@section('content')

<style>
  .pb-30 {
    padding-bottom: 30px;
  }

  #user_details,
  #user_type_details {
    margin: 2px;
  }

  /* media css */
  @media screen and (max-width: 991.98px) {
    .stepsWrapper {
      height: auto !important;
    }


    .multisteps-form__progress-btn {
      width: 0%;
      text-align: center;

    }

    .wizard-topper {
      position: relative !important;
      top: -2%;
      left: 10% !important;
      width: 80% !important;
      margin-bottom: 26px;
    }

  }

  /* media css ends  */

  .contentArea {
    height: auto;
  }

  .multisteps-form__panel ::-webkit-scrollbar {
    width: 10px;
    padding-right: 134px;
  }

  /* width */

  .wizard-v3-progress {
    background-color: #263ea5 !important;
    background-image: url(../img/bg.jpg) !important;
    background-size: cover !important;
    bottom: -19px !important;
    width: 375px !important;
    right: -441px !important;
  }

  .wizard-topper {
    position: relative;
    top: -2%;
    left: 75%;
    width: 30%;
  }

  .wizard-topper .wizard-progress .progress .progress-bar {
    top: 2px;
    left: 2px;
    width: 50%;
    height: 18px;
    position: absolute;
    border-radius: 80px;
    background-color: #2244A5 !important;
    /*
    background-color: #5756a2; */
  }

  .wizard-topper .wizard-progress .progress {
    height: 27px;
    padding: 2px;
    position: relative;
    border-radius: 80px;
    border: 2px solid #e3e3e3;
    background-color: transparent;
  }

  /*
 .wizard-v3-progress{
  background-color: #263ea5 !important;
    background-image: url(../img/bg.jpg) !important;
    background-size: cover!important;
    right: 0px !important;
   
  
 }
.multisteps-form__progress{
  padding-bottom: 20px !important;
} */

  .social-media-find .social-find-caret:after,
  .select-caret:after,
  .wizard-note-subject .select-option-area:after {
    top: 54px;
    right: 34px;

  }

  .input-custom-class {
    width: 100% !important;
    height: 50px !important;
    border: 2px solid #AAA !important;
    border-radius: 15px;
    padding-left: 20px;
  }

  .wrapper {
    margin-top: -10px !important;
  }




  .wizard-form-input select {
    height: 50px;
  }

  .wizard-form-field .wizard-form-input label {
    text-align: left !important;
  }

  .wizard-form-input {

    display: inline-block !important;
  }

  .inner-button-cls {
    height: 55px;
    width: 180px;
    color: #fff;
    display: block;
    font-size: 18px;
    font-weight: 500;
    border-radius: 0px;
    line-height: 55px;
    text-align: center;
    background-color: #72bb4c !important;
    cursor: pointer;
    border-color: #72bb4c !important;
    position: absolute;
    right: 50px;

  }

  table {
    width: 100%;

    border-collapse: collapse;
    border-spacing: 0px;
  }

  table td {
    border: 3px solid #2dabe2;
    padding: 8px 8px;
  }

  .languageCls {
    margin-bottom: 20px;
  }

  .loading {
    display: inline-block;
    width: 50px;
    height: 50px;
    border: 3px solid rgba(35, 75, 161, .3);
    border-radius: 50%;
    border-top-color: #fff;
    animation: spin 1s ease-in-out infinite;
    -webkit-animation: spin 1s ease-in-out infinite;
    position: relative;
    text-align: center;
    left: 50%;

  }

  .w-15 {
    width: 0% !important;
  }

  .w-85 {
    width: 100% !important;
  }

  .wizard-form-field {
    margin-right: 10%;
    margin-left: 10% !important;
  }


  @keyframes spin {
    to {
      -webkit-transform: rotate(360deg);
    }
  }

  @-webkit-keyframes spin {
    to {
      -webkit-transform: rotate(360deg);
    }
  }

  /************Print  */

  body {

    background-image: url("{{ url('/') }}/images/Securitas_Hintergrund.jpg");
  }

  /*************** */
  .submit_btn_click {
    background: url("{{ url('/') }}/images/hook.png") 0;
    background-size: 25px 25px;
    background-position: 13px 13px;
    background-repeat: no-repeat;
    background-color: #f86524 !important;
    ;
  }

  h5,
  h4 {
    color: #fff;
  }
</style>
@php


$tab = isset(request()->route()->parameters['tab'])?request()->route()->parameters['tab']:'';
$user_group = isset(request()->route()->parameters['user_group'])? request()->route()->parameters['user_group']:'' ;

use App\Http\Controllers\FrontendController;

@endphp


<div class="wrapper wizard d-flex clearfix multisteps-form position-relative ">


  <div class="  w-15 stepsWrapperDel" style="display: none;">
    <div class="multisteps-form__progress">
      <div>


        @foreach($questions as $key => $category)

        <span class="multisteps-form__progress-btn  js-active  " title='{{ FrontendController::getCategoryById($key)->category_name }}' style="pointer-events:none;"><i class="far fa-user"></i><span> {{ $key  }} === {{ FrontendController::getCategoryById($key)->category_name }}</span></span>

        @endforeach
        <span class="multisteps-form__progress-btn" title="{{ __('frontend.review') }}" style="pointer-events:none;"><i class="far fa-user"></i><span>{{ __('frontend.review') }}</span></span>

      </div>
    </div>
  </div>


  <form class="multisteps-form__form w-85 order-1 stepsWrapper" method="POST" action="#" autocomplete="off" id="frmquestioniar">
    @csrf
    <input type="hidden" name="post_params" value='<?php echo json_encode($post_params) ?>'>



    <div class="form-area position-relative">






      @php
      $total_tabs = $no_catergories + 1 ;
      $cat_i = 0 ;
      $prev_btn_id = 0;
      $temp_prev_id = '';
      @endphp
      @foreach($questions as $key => $category)


      <?php


      $prev_btn_id = $temp_prev_id;

      $temp_prev_id =  $key;



      ?>

      <div class="multisteps-form__panel @if( $cat_i == 0)  js-active  @endif " data-animation="slideHorz">
        <div class="wizard-forms section-padding">




          <div class="inner   clearfix">
            <!--  <div class="wizard-title text-center" style=" ">
        <h3> {{ FrontendController::getCategoryById($key)->category_name }}</h3>
            <p> </p>
          </div>-->
            <div class="wizard-form-field mb-85 contentArea">
              <div id="{{$key}}">


                @foreach($category as $key_quiz => $quiz)

                <div id="{{$key}}_infos">
                  @php $nextPage = $quiz->page + 1;


                  @endphp
                  @if($quiz->table_name == 'information' )


                  <div class="info-details" style="  padding-bottom: 20px;" id="{{$key}}_step" data-page="{{$nextPage}}">
                    @if( $quiz->image != "")
                    @if(file_exists(public_path( $quiz->image )))
                    <img src="{{ url('/') }}/{{$quiz->image}}" width="100px">
                    @endif
                    @endif

                    <strong>{{$quiz->title}}</strong>
                    <p style="padding: 20px;;">
                      {!!$quiz->info_description!!}
                    </p>
                  </div>
                  @endif
                </div>

                @if($quiz->table_name == 'videos' )
                <div class=" " id="{{$key}}_step" data-page="{{$nextPage}}">
                  <div class="embed-responsive embed-responsive-16by9 ">
                    <video id="video_{{$key}}" class="video" controls="controls">
                      <source src="{{ url('/') }}/{{$quiz->title}}" type="video/mp4">
                    </video>
                  </div>
                </div>
                @endif


                @endforeach
              </div>

              <div id="{{$key}}_content" style="display: none;">

              </div>

            </div>

            <!----------Rausgewaehlt // Fausgewaehlt ----------->

          </div>
          <!-- ./inner -->
          @php $cat_i++ ; @endphp
          <div class="actions  actions-btn-three " id="{{$key}}_btn">

            <ul>
              <li><span class="js-btn-prev neu_starten" title="neu_starten"> {{ __('frontend.neu_starten') }} </span></li>
              @if($category[0]->table_name == 'videos' )
              <li><span class="   video-restarten" title="Video New Starten" data-prev-container-id="{{$key }}"> {{ __('frontend.video_restarten') }} </span></li>
              @else
              <li><span class="js-btn-prev" title="BACK" data-prev-container-id="{{$prev_btn_id }}" data-cat_Id="{{$key}}"> {{ __('frontend.back') }} </span></li>

              @endif

              <li>

                <span class="js-btn-next nextEventCls @if( $total_tabs - 1 == $cat_i ) nextReviewArea   @endif" data-cat_Id="{{$key}}" q_count="" title="NEXT">{{ __('frontend.next') }} </span>
              </li>
            </ul>
          </div>
        </div>
      </div>




      @endforeach
      <!-- div 4 -->
      <div class="multisteps-form__panel  " data-animation="slideHorz">
        <div class="wizard-forms" id="review_div">
          <div class="inner   clearfix">
            <div class="wizard-title text-center">
              <h3>{{ __('frontend.review') }}</h3>

              <div id="review_area_wm">
                <div id="review_area" style="height:auto; ">
                </div>
              </div>
            </div>
          </div>
          <!-- /.inner -->

          <div class="actions" style="margin-bottom: 20px;">
            <ul style="text-align: center;">
              <!--<li><span class="js-btn-prev" title="BACK"><i class="fa fa-arrow-left"></i> {{ __('frontend.back') }} </span></li>-->
              <li style="margin-left: 32%;"><button title="NEXT" onclick="printFunc();return false;"> {{ __('frontend.print') }} </button></li>
            </ul>
            </ul>
          </div>
        </div>
      </div>
      <!-- div 5 -->
    </div>
    <input type="hidden" name="no_of_cat" id="no_of_cat" value="{{$no_catergories}}" />
  </form>



</div>


<!-----------------Confirmation ---------->

<!-- Modal -->
<div class="modal fade" id="videoConfirmation" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="videoModalLabel">{{ __('frontend.video_confirmation') }}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        {!! __('frontend.video_confirmation_txt') !!}
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal"> {{ __('frontend.close') }}</button>
      </div>
    </div>
  </div>
</div>

<!-----------------Confirmation ---------->

<!-- Modal -->
<div class="modal fade" id="selectConfirmation" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">{{ __('frontend.quiz_failed_confirmation') }}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        {!! __('frontend.quiz_failed_confirmation_txt') !!}
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal"> {{ __('frontend.close') }}</button>
        <button type="button" class="btn btn-primary" id="selectConfirmBtn" data-cat_Id=""> {{ __('frontend.yes') }}</button>
      </div>
    </div>
  </div>
</div>


<!-----------------Confirmation End ---------->


@endsection

@push('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.5.0-beta4/html2canvas.min.js"></script>

<script>
  /************************ */

  Array.from(document.querySelectorAll('.watermarked')).forEach(function(el) {
    el.dataset.watermark = (el.dataset.watermark + ' ').repeat(300);
  });

  /********************************** */
  var click_review_btn = 0;
  var allquestions = [];
  var videoended = 1;
  
  $("video").on("timeupdate", function() {
    if (!this.seeking) {
      supposedCurrentTime = this.currentTime;
    }
  });
  // prevent user from seeking
  $("video").on('seeking', function() {


    // guard agains infinite recursion:
    // user seeks, seeking is fired, currentTime is modified, seeking is fired, current time is modified, ....
    var delta = this.currentTime - supposedCurrentTime;

    if (Math.abs(delta) > 0.01) {
      //console.log("Seeking is disabled");
      this.currentTime = supposedCurrentTime;
    }

  });

  $("video").on("ended", function() {
    // reset state in order to allow for rewind

    videoended = 1;
    supposedCurrentTime = 0;
  });


  /********************************** */
  
  function printFunc() {

    var divToPrint = document.getElementById('review_area');


    newWin = window.open("");
    newWin.document.write(divToPrint.outerHTML);
    newWin.print();
    // newWin.close();
    // similar behavior as an HTTP redirect

    window.location.replace("{{URL::to('/')}}");
    return false;
  }

  /********************* */

  /********************* */
  function printCanvas() {
    html2canvas($("#review_area"), {
      onrendered: function(canvas) {
        theCanvas = canvas;
        document.body.appendChild(canvas);

        // Convert and download as image 
        $("#review_area_wm").html(canvas);



      }
    });

    // Convert the div to image (canvas)
    html2canvas(document.getElementById("review_area_wm")).then(function(canvas) {

      // Get the image data as JPEG and 0.9 quality (0.0 - 1.0)
      console.log(canvas.toDataURL("image/jpeg", 0.9));
    });

    // return false;

    //https://stackoverflow.com/questions/26402789/html2canvas-combine-2-captured-canvases-into-one




    var dataUrl = $("#review_area_wm").find('canvas')[0].toDataURL(); //attempt to save base64 string to server using this var  
    var windowContent = '<!DOCTYPE html>';
    windowContent += '<html>'
    windowContent += '<head><title>Print Certificate</title></head>';
    windowContent += '<body>'
    windowContent += '<img src="' + dataUrl + '">';

    windowContent += '</body>';
    windowContent += '</html>';
    var printWin = window.open('');


    printWin.document.open();
    printWin.document.write(windowContent);
    printWin.document.close();
    printWin.focus();
    printWin.print();
    return false;
  }

  /********************************** */


  var answers = {};
  var answers_count = {};


  function verifyQuiz(catId) {

    if (click_review_btn == 1) {
      click_review_btn = 2;
    }

    var token = "{{ csrf_token() }}"; // for passing with ajax calls
    var xhr = $.ajax({
      type: "POST",
      dataType: "json",
      url: "{{ url('information/verify-quiz') }}",
      data: {
        category_id: catId,
        quizCount: answers_count[catId],
        answers: answers,
        _token: token
      },
      async: false,
      //  success: handelData,
      //  error: handelData,

      error: function(data) {

        $('#selectConfirmation').modal();
        $('#selectConfirmBtn').attr('data-cat_Id', catId);
        click_review_btn = 0;
        //  }
        return false;
        // MessageBoxError('Error');
      }

    });
    //console.log(xhr);

    //  return jqXHR.responseText;

    return xhr.status;
  }


  /********************************** */




  $(document).on('click', '.js-btn-prev', function() {


    var btnId = $(this).parents('ul').parent('div').attr('id');
    var prevId = $(this).attr('data-prev-container-id'); //.show();
    var res = btnId.split("_");
    //$('#' + prevId + '_step').attr('data-page', 1);
    if (prevId == 'user_details_section') {
      retryAnswer(res[0]);
    }

    retryAnswer(prevId);


    //checkMorePages(prevId); 
    // $('#'+prevId).show();
    // $('#'+prevId + '_content').html('');
    // window.location.replace("{{URL::to('/')}}");
  });

  /********************************** */


  /********************************** */

  function retryAnswer(catId) {
    click_review_btn = 0;
    $('.getQuizBtn').show();
    $('#' + catId + '').show();
    $('#' + catId + '_step').attr('data-page', 1);
    checkMorePages(catId);
    $('#' + catId + '_content').hide();
    $('[data-prev-container-id="' + catId + '"]').show();

  }
  $(document).on('click', '#selectConfirmBtn', function() {

    retryAnswer($(this).attr('data-cat_Id'));
    $('#selectConfirmation').modal('hide');

  });
  /**************************** */
  /**************************** */

  $('.nextReviewArea').on('click', function() {

    if (click_review_btn != 0) {
      allquestions.push(answers);
      var data = $('form').serialize();
      var token = "{{ csrf_token() }}"; // for passing with ajax calls
      $.ajax({
        type: "POST",
        dataType: "json",
        url: "{{ url('information/review') }}",
        data: {
          data: data,
          allquesKey: JSON.stringify(allquestions),
          _token: token
        },
        success: function(response) {
          if (response.status == "success") {
            $('#review_area').html(response.html);


            //    makeCanvasFun();
            // waterMarkFun();
            // makeCanvasFun();




          }
        }
      });
    }
    click_review_btn = 1;

  });
  /**************************** */
  function makeCanvasFun() {
    return html2canvas($("#review_area"), {
      onrendered: function(canvas) {
        theCanvas = canvas;
        document.body.appendChild(canvas);

        // Convert and download as image 
        $("#review_area").html(canvas);
        //  waterMarkFun();


      }
    });
  }

  /**************************** */
  var wrongAnsCnt = 0;
  $('.nextEventCls').on('click', function() {
    if ($("#" + $(this).attr('data-cat_Id') + "_content").is(':visible') == false) {

      if ($("video").parents(".js-active ").length == 1 && videoended == 0) {
        $('#videoConfirmation').modal();
        return false;
      }
    }

    var cat_id = $(this).attr('data-cat_Id');


    // console.log($(this).attr('q_count'));

    if ($("#" + $(this).attr('data-cat_Id') + "_content").is(':visible') == true) {
      var tbl_id = $(this).parents('table').attr('data-tbl_cat_id');

      if ($(this).attr('q_count') != 0) {
        if (verifyQuiz(cat_id) == 400) {
          wrongAnsCnt++;
          if (wrongAnsCnt > 3) {
            window.location.href = "{{url('/')}}";
          }
          return false;
        } else {
          wrongAnsCnt = 0; // reinitialize
          videoended = 0;
          return true;
        }
      } else {
        allquestions.push(answers);
        videoended = 0;
        wrongAnsCnt = 0; // reinitialize
        return true;
      }
      return false;
    } else {

      if (checkMorePages(cat_id) == false) {

        return false;
      } else {

        getQuiz(cat_id);
        return false;
      }
    }

  });

  /**************************** */
  function checkMorePages(catId) {
    var next_page = $('#' + catId + '_step').attr('data-page');

    if (next_page != "") {
      var token = "{{ csrf_token() }}"; // for passing with ajax calls
      var xhr = $.ajax({
        type: "POST",
        dataType: "json",
        url: "{{ url('information/get-more-information') }}",
        data: {
          category_id: catId,
          page: next_page,
          user_group: "{{$user_group}}",
          _token: token
        },
        async: false,
        //  success: handelData,
        //  error: handelData,
        success: function(response) {

          if (response.status == "success") {

            $('#' + catId + '_infos').html(response.html);

          } else {
            // console.log(response);
          }
        },
        error: function(data) {


          // $('#selectConfirmation').modal();
          // $('#selectConfirmBtn').attr('data-cat_Id', catId);
          //  }
          return false;
          // MessageBoxError('Error');
        }
      });

      return xhr.responseJSON.getquize;
    }
  }
  /********************* */
  function json2array(json) {
    var result = [];


    var keys = Object.keys(JSON.parse(json));

    console.log(keys);
    keys.forEach(function(key) {
      result.push(json[key]);
    });
    return result;
  }

  /**************************** */
  function getQuiz(catId) {
    answers = {};
    answers_count = {};
    //  console.log(answers);   
    $('#' + catId + '_content').html('');
    $('#' + catId + '_content').show();
    $('#' + catId + '').hide();
    var token = "{{ csrf_token() }}"; // for passing with ajax calls
    $.ajax({
      type: "POST",
      dataType: "json",
      url: "{{ url('information/get-quiz-details') }}",
      data: {
        category_id: catId,
        user_group: "{{$user_group}}",
        _token: token
      },
      success: function(response) {

        if (response.status == "success") {
          $('[data-prev-container-id="' + catId + '"]').hide();
          $('#' + catId + '_content').html(response.html);

          var ans_json_data = $($.parseHTML(response.html)).filter("#ans_group_" + catId);

          var ans_json_data_val = $.parseJSON(ans_json_data.val());

          if (Object.keys(ans_json_data_val).length) {
            answers = ans_json_data_val;
            answers_count[catId] = Object.keys(answers).length;


          }

          $('.getQuizBtn').hide();



          $('.nextEventCls[data-cat_id="' + catId + '"]').attr('q_count', 1); // todo



          $(window).scrollTop(100);

          // console.log(response);
        } else {
          // console.log(response .status);
          $('.nextEventCls[data-cat_id="' + catId + '"]').attr('q_count', 0);
          $('.nextEventCls[data-cat_id="' + catId + '"]').trigger('click');
        }
      }
    });


  }



  /**************************** */

  $(document).on('click', '.optionsCls', function() {
    var td_id = $(this).parents('td').attr('id');
    var tbl_id = $(this).parents('.quiz_parent_tbl').attr('data-tbl_cat_id');
    var quizCount = $(this).parents('.quiz_parent_tbl').attr('data-quiz_count');
    // console.log(tbl_id);
    if (answers[tbl_id] == undefined) {
      // answers[tbl_id] =[];
    }
    answers_count[tbl_id] = quizCount;
    // answers[tbl_id][td_id] = $(this).attr('data-optionvalue');
    //console.log(  answers_count );
    //console.log( answers[tbl_id][td_id]);
    answers[td_id] = $(this).attr('data-optionvalue');
    if ($(this).is(".optionsCls.option-no")) {

      $('#' + td_id).find('.option-yes').attr("src", "{{ url('/') }}/images/R.png");
    }
    if ($(this).is(".optionsCls.option-yes")) {
      $('#' + td_id).find('.option-no').attr("src", "{{ url('/') }}/images/F.png");
    }

    if ($(this).attr('data-optionValue') == 1) {
      var img1 = "{{ url('/') }}/images/R.png",
        img2 = "{{ url('/') }}/images/Rausgewaehlt.png";
    } else {
      var img1 = "{{ url('/') }}/images/F.png",
        img2 = "{{ url('/') }}/images/Fausgewaehlt.png";
    }
    $(this).attr("src", ($(this).attr('src') === img1) ? img2 : img1);
    // console.log($(this).attr('src'));
    // console.log(img1);
    if ($(this).attr('src') === img1) {

      if ($(this).attr('data-option-multi') != 1) {
        delete answers[td_id];
      } else {
        answers[td_id] = 0;
      }
    }
    // console.log(answers_count);
    // console.log(answers);
  });
  /************************** */


  /************************** */

  function change() {
    var img1 = "{{ url('/') }}/images/R.png",
      img2 = "{{ url('/') }}/images/Rausgewaehlt.png";
    var imgElement = document.getElementById('test');

    imgElement.src = (imgElement.src === img1) ? img2 : img1;
  }
  /************************** */


  $(document).ready(function() {
    $('#usersection-div-hidden').show();
    @if($user_group !== null)

    $('#usersection-div-hidden').hide();
    $('#usersection-div').show();
    @endif
  });
  /************************** */

  /************************** */
  $('.neu_starten').on('click', function() {
    window.location.href = "{{url('/')}}/";
  });
  /************************** */


  /************************ */
  $('#contact_details').click(function() {

    var res = true;
    // here I am checking for textFields, password fields, and any 
    // drop down you may have in the form
    $("input[name='surname'],input[name='firstname']  ").each(function() {
      if ($(this).val().trim() == "") {
        // console.log( $(this));
        $('#' + $(this).attr('name') + '-error').show();
        res = false;
      }
    });
    // $('.stepsWrapper').height(600);
    //  console.log(res);
    return res;

    /*  if (res == true) {
       $('#frmquestioniar').attr('action', "{{url('/3/')}}/" + $('#user_type').val());
       $('#frmquestioniar').submit();
       // window.location.href = "{{url('/3/')}}/" + $('#user_type').val();
     } */

  });

  /*
    $('#language_sel').click(function() {
      var token = "{{ csrf_token() }}"; // for passing with ajax calls
      $('#usersection-div-hidden').show();
      $('#usersection-div').hide();
      var language = $('#language').val();
      window.location.href = "{{url('locale')}}/" + language + "?tab=2";
    });
  */




  /***************************** */
  /********************************** */


  $(document).on('click', '.video-restarten', function() {



    var curId = $(this).attr('data-prev-container-id'); //.show();

    $("video").load();

  });
  /********************************** */
</script>


@endpush