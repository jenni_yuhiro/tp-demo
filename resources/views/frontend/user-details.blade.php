@foreach($questions as $keyQue => $question)
<?php
if (\App::getLocale() == 'de')
  $question_lbl = $question->question_de;
else
  $question_lbl = $question->question;

?>

@if($question->answers()->where('question_id', '=',$question->id)->count() !== 0 )


<!------------->

<div class="col-sm-12 ">
  <h3 style=" width: auto;">{{$question_lbl}}</h3>
</div>

@foreach($question->answers()->where('question_id', '=',$question->id)->orderBy('answer')->get() as $answers)
<?php
if (\App::getLocale() == 'de')
  $answer_lbl = $answers->answer_de;
else
  $answer_lbl = $answers->answer;
?>


<div class="col-sm-6 text-center ">
  <a class="user-type-qa" id="ans_{{$answers->id}}" onClick="return userTypeAns('{{$answers->id}}');return false;" style="width: 100%;">
    {{ $answer_lbl}} </a>
</div>
@endforeach
<!------------->
@else
<div class="wizard-form-input">
  <label style=" width: 720px; text-align: left;">{{$question_lbl}}</label>
  <input class="input-custom-class" name="question_input" id="input-question_input" placeholder="{{$question_lbl}}" value="" />
</div>
@endif
@endforeach
<div style="margin-bottom: 70px; height:20px">&nbsp; </div>