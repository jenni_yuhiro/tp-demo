@extends('layouts.app-frontend', ['activePage' => 'dashboard', 'titlePage' => __('Dashboard')])

@section('content')

<style>
  .pb-30 {
    padding-bottom: 30px;
  }

  #user_details,
  #user_type_details {
    margin: 2px;
  }

  /* media css */
  @media screen and (max-width: 991.98px) {
    .stepsWrapper {
      height: auto !important;
    }


    .multisteps-form__progress-btn {
      width: 0%;
      text-align: center;

    }

    .wizard-topper {
      position: relative !important;
      top: -2%;
      left: 10% !important;
      width: 80% !important;
      margin-bottom: 26px;
    }

  }

  /* media css ends  */

  .contentArea {
    height: auto;
  }

  .multisteps-form__panel ::-webkit-scrollbar {
    width: 10px;
    padding-right: 134px;
  }

  /* width */

  .wizard-v3-progress {
    background-color: #263ea5 !important;
    background-image: url(../img/bg.jpg) !important;
    background-size: cover !important;
    bottom: -19px !important;
    width: 375px !important;
    right: -441px !important;
  }

  .wizard-topper {
    position: relative;
    top: -2%;
    left: 75%;
    width: 30%;
  }

  .wizard-topper .wizard-progress .progress .progress-bar {
    top: 2px;
    left: 2px;
    width: 50%;
    height: 18px;
    position: absolute;
    border-radius: 80px;
    background-color: #2244A5 !important;
    /*
    background-color: #5756a2; */
  }

  .wizard-topper .wizard-progress .progress {
    height: 27px;
    padding: 2px;
    position: relative;
    border-radius: 80px;
    border: 2px solid #e3e3e3;
    background-color: transparent;
  }

  /*
 .wizard-v3-progress{
  background-color: #263ea5 !important;
    background-image: url(../img/bg.jpg) !important;
    background-size: cover!important;
    right: 0px !important;
   
  
 }
.multisteps-form__progress{
  padding-bottom: 20px !important;
} */

  .social-media-find .social-find-caret:after,
  .select-caret:after,
  .wizard-note-subject .select-option-area:after {
    top: 54px;
    right: 34px;

  }

  .input-custom-class {
    width: 100% !important;
    height: 50px !important;
    border: 2px solid #AAA !important;
    border-radius: 15px;
    padding-left: 20px;
  }

  .wrapper {
    margin-top: -10px !important;
  }




  .wizard-form-input select {
    height: 50px;
  }

  .wizard-form-field .wizard-form-input label {
    text-align: left !important;
  }

  .wizard-form-input {

    display: inline-block !important;
  }

  .inner-button-cls {
    height: 55px;
    width: 180px;
    color: #fff;
    display: block;
    font-size: 18px;
    font-weight: 500;
    border-radius: 0px;
    line-height: 55px;
    text-align: center;
    background-color: #72bb4c !important;
    cursor: pointer;
    border-color: #72bb4c !important;
    position: absolute;
    right: 50px;

  }

  table {
    width: 100%;

    border-collapse: collapse;
    border-spacing: 0px;
  }

  table td {
    border: 3px solid #2dabe2;
    padding: 8px 8px;
  }

  .languageCls {
    margin-bottom: 20px;
  }

  .loading {
    display: inline-block;
    width: 50px;
    height: 50px;
    border: 3px solid rgba(35, 75, 161, .3);
    border-radius: 50%;
    border-top-color: #fff;
    animation: spin 1s ease-in-out infinite;
    -webkit-animation: spin 1s ease-in-out infinite;
    position: relative;
    text-align: center;
    left: 50%;

  }

  .w-15 {
    width: 0% !important;
  }

  .w-85 {
    width: 100% !important;
  }

  .wizard-form-field {
    margin-right: 10%;
    margin-left: 10% !important;
  }


  @keyframes spin {
    to {
      -webkit-transform: rotate(360deg);
    }
  }

  @-webkit-keyframes spin {
    to {
      -webkit-transform: rotate(360deg);
    }
  }

  /************Print  */

  body {

    background-image: url("{{ url('/') }}/images/Securitas_Hintergrund.jpg");
  }

  /*************** */
  .submit_btn_click {
    background: url("{{ url('/') }}/images/hook.png") 0;
    background-size: 25px 25px;
    background-position: 13px 13px;
    background-repeat: no-repeat;
    background-color: #f86524 !important;
    ;
  }
  h5,h4{
    color: #fff;
  }
</style>
 @php
$lang = isset(request()->route()->parameters['lang'])? request()->route()->parameters['lang']:'' ;

@endphp
<div class="wrapper wizard d-flex clearfix multisteps-form position-relative ">


  <div class="  w-15 stepsWrapperDel" style="display: none;">
    <div class="multisteps-form__progress">
      <div>
         <span class="multisteps-form__progress-btn    " id="tab_2"><i class=" far fa-user"></i><span>{{ __('frontend.user_type_lbl') }}</span></span>

    
        <span class="multisteps-form__progress-btn " title=""><span>user QA</span></span>
 
        <span class="multisteps-form__progress-btn " title=""></i><span>{{ __('frontend.user_type_lbl') }}</span></span>
        <span class="multisteps-form__progress-btn " title=""></i><span>{{ __('frontend.user_type_lbl') }}</span></span>

       
      
      </div>
    </div>
  </div>


  <form class="multisteps-form__form w-85 order-1 stepsWrapper" method="POST" action="#" autocomplete="off" id="frmquestioniar">
    @csrf




    <div class="form-area position-relative">

      <!-- div 1 -->
       
      <!-- div--2 -->



      <!-- div--2 -->

      <div class="multisteps-form__panel   js-active    " data-animation="slideHorz">
        <div class="wizard-forms section-padding">
          <div class="inner  clearfix"  id="user_group_section">
            <div class="wizard-title text-center " style="">
              <h3>{{ __('frontend.user_type') }}</h3>
            </div>
            <div class="wizard-form-field   contentArea" style=" margin-bottom:50px">


            <div class="row ">
                       
                            <div class="col-sm-4 text-center ">
                                <img width="70%" data-value="visitor" src="{{ url('/') }}/images/visitor.png">
                                <div class="  userGroupContainer d-flex justify-content-center "
                                    data-user-group="visitor">
                                    <ul>
                                        <li class="user-group-key"><span class="  user-group-span"
                                                title="{{ __('common.visitor') }}">{{ __('common.visitor') }} </span></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-sm-4 text-center ">
                                <img width="70%" data-value="foreign_worker"
                                    src="{{ url('/') }}/images/foreign_worker_truck_driver.png">
                                <div class="    userGroupContainer d-flex justify-content-center"
                                    data-user-group="foreign_worker">
                                    <ul>
                                        <li class="user-group-key"><span class="  user-group-span"  
                                                title="{{ __('common.foreign_worker') }} ">{{ __('common.foreign_worker') }}</span></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-sm-4 text-center ">
                                <img width="70%" data-value="Truck driver" src="{{ url('/') }}/images/foreign_worker_truck_driver.png">
                                <div class="  userGroupContainer d-flex justify-content-center "
                                    data-user-group="truck_driver">
                                    <ul>
                                        <li class="user-group-key"><span class="  user-group-span"
                                                title="{{ __('common.Truck driver') }} "> {{__('common.truck_driver')}} </span></li>
                                    </ul>
                                </div>
                            </div>
                           
                            </div>

 
             
            </div>
          </div>


          <!-- /.inner -->
          <div class="inner  clearfix" >
            <div class="wizard-form-field   contentArea" >
              <div class="row"  id="user_type_details" style="display:none ">
                <!--Big blue loading-->
                <div class="loading"></div>
              </div>
            </div>
          </div>
          <!-- second inner -->
          <div  style="margin-bottom: 70px; height:20px">&nbsp; </div>
          <div class="actionContainer"   >
            <div class="action-div actions  actions-btn-three   ">
              <ul>

                <li><span class="js-btn-prev" id="goto_home" title="BACK"> {{ __('frontend.back') }} </span></li>
                <li><span class="js-btn-next " title="NEXT" id="user_group_proceed" data-data_details="">{{ __('frontend.next') }} </span></li>
              </ul>
            </div>


          </div>
        </div>
      </div>

      <!-- div 2 ends -->


      <input type="hidden" name="fw_ug_proceed" id="fw_ug_proceed" value="">





      <!-- div-- 3-->

      <div class="multisteps-form__panel    " data-animation="slideHorz">
        <div class="wizard-forms section-padding" id="user_details_section">
        
          <div class="inner pb-30 clearfix" id="usersection-div">
            <div class="wizard-title text-center">
              <h3>{{ __('frontend.contact_details') }}</h3>
            </div>
            <div class="wizard-form-field mb-85 contentArea  ">


              <div class="row ">

                <div class="wizard-form-input">
          
             
                  <input class="input-custom-class" name="firstname" id="input-firstname" placeholder="{{ __('frontend.firstname') }} " value="@php echo isset($_POST['firstname'])?$_POST['firstname']:'' @endphp">
                  <span id="firstname-error" class="error text-danger" for="input-firstname" style="display: none;"> {{ __('frontend.firstname_error') }} </span>

                </div>

                <div class="wizard-form-input">
                  <input class="input-custom-class" name="surname" id="input-surname" placeholder="{{ __('frontend.surname') }} " value="@php echo isset($_POST['surname'])?$_POST['surname']:'' @endphp">
                  <span id="surname-error" class="error text-danger" for="input-surname" style="display: none;"> {{ __('frontend.surname_error') }} </span>

                </div>


                <div class="wizard-form-input">
                  <input class="input-custom-class" name="company" id="input-company" placeholder="{{ __('frontend.company') }}" value="@php echo isset($_POST['company'])?$_POST['company']:'' @endphp">
                </div>
              </div>
            </div>
          </div>
          <!-- /.inner -->
          <div class="action-div actions user-details-actions actions-btn-three">
            <ul>

              <li><span class="js-btn-prev" title="BACK"> {{ __('frontend.back') }} </span></li>

              <li><span class="js-btn-next " title="NEXT" id="contact_details">{{ __('frontend.next') }} </span></li>

            </ul>
          </div>
        </div>
      </div>

      <!-- div 1 -->


      <!-- div-- 3-->

      <div class="multisteps-form__panel  " data-animation="slideHorz">
        <div class="wizard-forms section-padding">

          <div class="inner  clearfix" id="usersection-div-intro">
            <div class="wizard-title text-center">
              <h3>{{ __('frontend.short_intro') }}</h3>
            </div>
            <div class="wizard-form-field mb-85 contentArea  ">
              <div class="row short_instructions" style="">

                {{ __('frontend.short_instructions') }}


                <div class="actions" style="margin-top: 80px;">
                  <ul>
                    <li><span class="js-btn-next" id="understand_id" title="{{ __('frontend.understand_btn') }}">{{ __('frontend.understand_btn') }}</span></li>
                  </ul>
                </div>

              </div>
            </div>
          </div>
          <!-- /.inner -->

        </div>
      </div>

      <!-- div 1 -->



      <!-- div 3 -->
      <!-- /.inner -->

      <!-- div 3 -->

    
      <!-- div 5 -->
    </div>
 <input type="hidden" name="user_group_txt" id="user_group_txt" >
  </form>



</div>


<!-----------------Confirmation ---------->

 

<!-----------------Confirmation ---------->
 
<!-----------------Confirmation End ---------->


@endsection

@push('js')
 

<script>
  /************************ */
 
  $(document).on('click', '#goto_home', function() { 
  window.location.replace("{{URL::to('/')}}");
});
  /********************************** */
 
  
  $('#understand_id').click(function() {
    $('#frmquestioniar').attr('action', "{{url('/')}}/"  + "{{$lang}}/"  + $('#user_group_txt').val());
     $('#frmquestioniar').submit();
  });

  /********************************** */
  $('#user_group_proceed').click(function() {
   // $('#usersection-div').html('<div class="loading"></div>');

    if(  $('#user_group_txt').val()  ==""){
      alert('Please select User Group'); return false;
    } 
    if ($(this).attr("data-data_details") == 'yes') {
      return true;;
     // $('#frmquestioniar').attr('action', "{{url('/')}}/"  + "{{$lang}}/"  + $('#user_group_txt').val());
     //  $('#frmquestioniar').submit();
      
    } else {
      $('#fw_ug_proceed').val('yes');
      $(this).attr("data-data_details", "yes");


      var token = "{{ csrf_token() }}"; // for passing with ajax calls
      $.ajax({
        type: "POST",
        dataType: "json",
        url: "{{ url('question/user-type-details') }}",
        data: {
          user_type: $('#user_group_txt').val(),
          _token: token
        },
        success: function(response) {
         
          if (response.status == "success") {
            if(response.question_count > 0) {
            
        
            $('#user_type_details').show();
            $('#user_type_details').html(response.html);
            $('#user_group_section').hide();
            } else{
              $('#user_group_proceed' ).trigger('click');
            }
          } else {
            //  console.log(response);
          }
        }
      });
      return false;
    }
    // $('#frmquestioniar').submit();
  });

  /************************** */
  $('.user-group-key').on('click', function() {
     
    var user_group_val = $(this).parents('.userGroupContainer').attr('data-user-group');
   
    $('#user_group_txt').val(user_group_val);
    $('.user-group-key').find('span').css('background-color', '#fca950');
    $(this).find('span').css('background-color', '#f86524');

  });
  /************************** 
  $(document).on('click', '#contact_details', function() {

    var user_group = $('#user_group_txt').val();
    if (user_group == "") user_group = 'visitor';
    // $(this).attr("id","newId");
    window.location.href = "{{url('/3/')}}/" + user_group;
  });
  ************************** */


  function userTypeAns(Id) {


    if ($("#ans_" + Id).hasClass('submit_btn_click') == true) {
      $("#ans_" + Id).removeClass("submit_btn_click");
    } else {
      $("#ans_" + Id).addClass("submit_btn_click");
    }

    var selectedVal = $("input[name=user_work_field]").val();

    if (selectedVal === "") {
      jsonObj = [null];
    }
    console.log(selectedVal);
    if (selectedVal.includes(Id) === true) {
      var indexOfId = jsonObj.indexOf(Id);
      jsonObj.splice(indexOfId, 1);
    } else {
      jsonObj.push(Id);
    }
    jsonString = JSON.stringify(jsonObj);
 console.log(jsonString);
    $('input[name=user_work_field]').val(jsonString);

  }
 
 
 

  /********************************** */

 

  /************************** */
  $('.neu_starten').on('click', function() {
    window.location.href = "{{url('/')}}/";
  });
  /************************** */


  /************************ */
  $('#contact_details').click(function() {

    var res = true;
    // here I am checking for textFields, password fields, and any 
    // drop down you may have in the form
    $("input[name='surname'],input[name='firstname']  ").each(function() {
      if ($(this).val().trim() == "") {
        // console.log( $(this));
        $('#' + $(this).attr('name') + '-error').show();
        res = false;
      }
    });
    // $('.stepsWrapper').height(600);
    //  console.log(res);
    return res;
 

  });

 



 
</script>


@endpush