<table style="width: 100%;   " class="quiz_parent_tbl" data-tbl_cat_id="{{$category_id}}" data-quiz_count="{{$quizCount}}">

   @foreach($quiz as $keyQue => $question)
 
   @if( $question['parent']->count() > 0 )
   <tr>
     <td style="width:40%;"><strong> {{$question->question}} </strong> </td>
     <td colspan="2">
       <table>
       @foreach($question['parent'] as $keymulti => $question_multi)


         <tr>
           <td   ><strong> {{$question_multi->question}} </strong></td>
           <td style="width: 30%;" class="options-td text-center" id="{{$question_multi->id}}">
           <img src="{{ url('/') }}/images/R.png" width="50px" height="50px" class="optionsCls option-yes" data-optionValue="1" data-option-multi="1" />
      
   
           </td>
         </tr>
         @endforeach
       </table>
      
     </td>
     
   </tr>
   @else
   <tr>
     <td><strong> {{$question->question}} </strong> </td>
     <td style="width: 20%;" class="options-td" id="{{$question->id}}">
       <img src="{{ url('/') }}/images/R.png" width="50px" height="50px" class="optionsCls option-yes" data-optionValue="1" />
       <img src="{{ url('/') }}/images/F.png" width="50px" height="50px" class="optionsCls option-no" data-optionValue="0" />
     </td>
   </tr>
   @endif
   @endforeach
 </table>
 <input type="hidden" value="{{json_encode($qa_arr)}}" id="ans_group_{{$category_id}}" >
