<style>
  #review_area_inner {
    background-color: #FFF;
    margin: 3%;
    position: relative;
    margin-top: 20px;
    padding-top: 20px;
    padding-bottom: 20px;
    -moz-user-select: none;
    -webkit-user-select: none;
    -ms-user-select: none;
    user-select: none;
    -o-user-select: none;

  }


  .watermark {
    position: absolute;
    top: 41%;

    -ms-transform: rotate(-20deg);
    /* IE 9 */
    transform: rotate(-30deg);
    font-family: currenta;
    letter-spacing: 10px;
    font-size: 72px;
    color: #fc7900;
    opacity: .3;
  }

  @media print {



    .watermark {
      position: absolute;
      top: 41%;  

      color: #45ea01;
      letter-spacing: 3px;

    }
  }
</style>


<div class="row " unselectable="on" onselectstart="return false;" onmousedown="return false;" id="review_area_inner">
 

  <div class="col-md-2 "></div>
  <div class="col-md-10 text-left">
  <span class="watermark"><img src="{{ url('/') }}/images/logo_transparent.png" width="200px"> </span>
    <h2> {{__('frontend.review_details.security_certificate' )}} </h2><br>
    <h4> {{__('frontend.review_details.date' , ['name' => $data['today'] ])}} </h4> <br><br>

    {{__('frontend.review_details.greet' , ['name' => $data['surname']   ])}}, <br><br>

    {!! __('frontend.review_details.review_content') !!}

    <br><br>
    {{__('frontend.review_details.surname' , ['name' => $data['surname'] ])}} <br>
    {{__('frontend.review_details.firstname' , ['name' => $data['firstname'] ])}} <br>
    {{__('frontend.review_details.company' , ['name' => $data['company'] ])}} <br>
    {{__('frontend.review_details.user_type' , ['name' =>  \App\Questions::userTypes()[$data['user_type']]])}} <br>
    {{__('frontend.review_details.cs_valid_date' , ['name' => $data['validDate'] ])}}

    <br>


    <?php

    //$data_code = http_build_query($data);  
      $data_code = json_encode($dataqr);
    ?>

    <div id="qrcode" style="  margin-top: 20px;">
      {!! QrCode::size(100)->generate($data_code); !!}
    </div>
  </div>

  <br>

</div>

</div>

<script>
  /*
  $(document).ready(function() {

    //Disable cut copy paste

    $('body').bind('cut copy paste', function(e) {

      e.preventDefault();

    });

    //Disable mouse right click

    $("body").on("contextmenu", function(e) {

      return false;

    });

  });
 ******************* */
</script>