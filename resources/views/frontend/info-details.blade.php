@foreach($infos as $key_quiz => $quiz)
@php $nextPage = $quiz->page + 1; @endphp

<div class="info-details" style="  padding-bottom: 20px;" id="{{$quiz->category_id}}_step" data-page="{{$nextPage}}">

  @if( $quiz->image != "")
  @if(file_exists(public_path( $quiz->image )))
  <img src="{{ url('/') }}/{{$quiz->image}}" width="100px">
  @endif
  @endif

  <strong >{{$quiz->title}}</strong>
  <p style="padding: 20px;;">
    {!!$quiz->info_description!!}
  </p>
</div>

<script>
  var info_height = parseInt($('.stepsWrapper').height()) + parseInt($('#' + '{{$quiz->category_id}}_step').height());
 // $('.stepsWrapper').height(info_height);
</script>
@endforeach