<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVisitorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visitors', function (Blueprint $table) {
            $table->uuid('id')->primary();  
            $table->string('name')->nullable();  
            $table->string('last_name')->nullable(); 
            $table->string('company')->nullable(); 
            $table->string('user_group')->nullable(); 
            $table->string('valid_date')->nullable(); 
            $table->tinyInteger('signature_flag')->default(1)->comment('1: Yes, 0: No');     
            $table->tinyInteger('checkin_status')->default(0)->comment('1: Yes, 0: No');               
   
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('visitors');
    }
}
