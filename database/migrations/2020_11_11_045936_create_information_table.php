<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('information', function (Blueprint $table) {
            $table->uuid('id')->primary();       
            $table->uuid('category_id') ;           
            $table->string('info')->nullable();  
            $table->string('info_de')->nullable();  
            $table->text('description')->nullable();  
            $table->text('description_de')->nullable();  
            $table->string('image')->nullable();   
            $table->bigInteger('order_no')->nullable();         
        });
        Schema::table('information', function (Blueprint $table) {
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
     
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
  
        Schema::disableForeignKeyConstraints();  
        Schema::table('information', function (Blueprint $table) {
            $table->dropForeign('information_category_id_foreign');
        });  
        Schema::dropIfExists('information');
        Schema::enableForeignKeyConstraints(); 
    }
}
