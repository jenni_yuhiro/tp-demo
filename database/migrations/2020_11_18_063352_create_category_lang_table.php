<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoryLangTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('category_lang', function (Blueprint $table) {
            $table->uuid('id')->primary();       
            $table->uuid('category_id') ;           
            $table->string('lang')->nullable();  
            $table->string('name')->nullable();  
            
        });
        Schema::table('category_lang', function (Blueprint $table) {
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
     
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
        Schema::disableForeignKeyConstraints();  
        Schema::table('category_lang', function (Blueprint $table) {
            $table->dropForeign('category_lang_category_id_foreign');
        });  
        Schema::dropIfExists('category_lang');
        Schema::enableForeignKeyConstraints(); 
    }
}
