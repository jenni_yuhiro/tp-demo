<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuizTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quiz', function (Blueprint $table) {
            $table->uuid('id')->primary();       
            $table->uuid('category_id') ;           
            $table->string('question')->nullable();  
            $table->tinyInteger('answer')->default(1)->comment('1: Yes, 0: No');          
            $table->bigInteger('order_no')->nullable();         
           
        });

        Schema::table('quiz', function (Blueprint $table) {
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
     
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      

        Schema::disableForeignKeyConstraints();  
        Schema::table('quiz', function (Blueprint $table) {
            $table->dropForeign('quiz_category_id_foreign');
        });  
        Schema::dropIfExists('quiz');
        Schema::enableForeignKeyConstraints(); 

    }
}
