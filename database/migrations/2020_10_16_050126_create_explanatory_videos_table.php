<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExplanatoryVideosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('explanatory_videos', function (Blueprint $table) {
            $table->uuid('id')->primary();   
            $table->string('title')->nullable();              
            $table->string('video')->nullable();          
            $table->tinyInteger('main')->default(0)->comment('1: Main, 0: Normal');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('explanatory_videos');
    }
}
