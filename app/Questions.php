<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Questions extends Model
{
    use \App\Http\Traits\UsesUuid;



    // column name of key
    protected $primaryKey = 'id';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',   'user_type',
        'question', 'question_de', 'order_no'

    ];
    /*
    public function Categories()
    {
        return $this->belongsTo('App\Categories', 'category', 'id' );
    }
*/

    public function answers()
    {
        return $this->hasMany('App\Answers', 'question_id', 'id');
    }


    public static function userTypes()
    {
        return [
            'visitor' => __('common.visitor'),
            'foreign_worker' =>  __('common.foreign_worker'),
            'truck_driver'  => __('common.truck_driver')
        ];
    }
}
