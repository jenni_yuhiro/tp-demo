<?php

namespace App;
use App\Questions;
use Illuminate\Database\Eloquent\Model;

class Answers extends Model
{
    use \App\Http\Traits\UsesUuid;

  

    // column name of key
    protected $primaryKey = 'id';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

     
    protected $fillable = [
        'id',
        'question_id' ,'answer','answer_de'
        
    ];
  
        /**
     * timestamps
     *
     * @var bool
     */
    public $timestamps = false;
     
     
}
