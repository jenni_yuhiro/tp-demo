<?php

namespace App\Http\Controllers;

use App\Information;
use App\Categories;
use App\Quiz;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\Storage;

 

class InformationController extends Controller
{




    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $info  =  Information::all()->sortBy("order_no");

        return view('information.index')->withInfo($info);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories  =  Categories::whereIn('type', ['question'])->get();


        return view('information.create')->withCategories($categories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'category' => 'required',
            'user_group' => 'required',
            'info_de' => 'required',
            'info'   => 'required',
            'image' => 'sometimes|mimes:jpg,jpeg,png,bmp,tiff|max:2048'

        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
 
        $file_path = '';
        if ($request->file() &&  $request->file('image') != "") {
            $file = $request->file('image');
            $file_path = 'uploads/images/info/' . time() . '_' . $file->getClientOriginalName();
            $file->move('uploads/images/info/', time() . '_' . $file->getClientOriginalName());
        }

        $information = Information::create([
            'category_id' =>  $request->category,
            'user_group'  =>   $request->user_group,
            'page'       =>   $request->page,
            'info_de' =>  $request->info_de,
            'info'    =>  $request->info,
            'image'      =>   $file_path,
            'description_de'  =>  $request->info_desc_de,
            'description'  =>  $request->info_desc,
            'image' =>     $file_path,
            'order_no'    => (Information::count()) + 1,

        ]);
        $information->save();


        return redirect('admin/information')
            ->with('message', trans('info.created_success'));
    }


      /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */
    public function uploadImage(Request $request)
    {
        if($request->hasFile('upload')) {
            $originName = $request->file('upload')->getClientOriginalName();
            $fileName = pathinfo($originName, PATHINFO_FILENAME);
            $extension = $request->file('upload')->getClientOriginalExtension();
            $fileName = $fileName.'_'.time().'.'.$extension;

    
        
            $request->file('upload')->move(public_path('uploads/images/info/'), $fileName);
   
            $CKEditorFuncNum = $request->input('CKEditorFuncNum');
            $url = asset('uploads/images/info/'.$fileName); 
            $msg = 'Image uploaded successfully'; 
            $response = "<script>window.parent.CKEDITOR.tools.callFunction($CKEditorFuncNum, '$url', '$msg')</script>";
               
            @header('Content-type: text/html; charset=utf-8'); 
            echo $response;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Information  $information
     * @return \Illuminate\Http\Response
     */
    public function show(Information $information)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Information  $information
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {


        $information   =  Information::find($id);

        $categories  =  Categories::where('type', 'question')->get();
        return view('information.edit')->withInformation($information)->withCategories($categories);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Information  $information
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'category' => 'required',
            'user_group' => 'required',
            'info_de' => 'required',
            'info'   => 'required',
            'image' => 'sometimes|mimes:jpg,jpeg,png,bmp,tiff|max:2048'
        ]);



        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }


        /** Your database saving implementation */
       // $info_de_desctiption = $this->saveImagesEditor($request->info_desc_de);

        /****************** */



        $information   =  Information::find($id);
        if ($request->file() &&  $request->file('image') != "") {

            //   $old_path = public_path() . "/" .   $information->image;
            if (file_exists(public_path($information->image)) &&  $information->image != "") {
                unlink(public_path($information->image));
            }
            $file = $request->file('image');
            $file_path = 'uploads/images/info/' . time() . '_' . $file->getClientOriginalName();
            $file->move('uploads/images/info/', time() . '_' . $file->getClientOriginalName());
            $information->image  =   $file_path;
        }



        $information->category_id    =   $request->category;
        $information->user_group     =   $request->user_group;
        $information->page           =   $request->page;
        $information->info_de        =   $request->info_de;
        $information->info           =   $request->info;
        $information->description_de =   $request->info_desc_de;
        $information->description    =     $request->info_desc;
        $information->save();

        return redirect('admin/information')
            ->with('message', trans('info.updated_success'));
    }

    /********************* */
 
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Information  $information
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $information   =  Information::find($id);
        $image_path   =   $information->image;
        $public_path_image = public_path($image_path);

        if (\File::exists($public_path_image)) {
            \File::delete($public_path_image);
        }
        Information::whereId($id)->delete();
        return response(['message' => trans('info.delete_sucess')], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateOrder(Request $request)
    {
        $information = Information::all();
        foreach ($information as $info) {
            foreach ($request->order as $order) {
                if ($order['id'] == $info->id) {
                    $info->update(['order_no' => $order['position']]);
                }
            }
        }
        return response('Update Successfully.', 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function deleteImage(Request $request)
    {
        $information   =  Information::find($request->infoId);
        $image_path   =   $information->image;
        $public_path_image = public_path($image_path);
        $information->image  =  '';
        if (\File::exists($public_path_image)) {
            \File::delete($public_path_image);
        }
        $information->save();
        return response(['status' => trans('success'), 'infoId' => $request->infoId], 200);
    }
    /*****************
     *  infoDetailsById
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function infoDetailsById(Request $request)
    {
        $information   =  Information::find($request->queid);
        $categories    =  Categories::where('id', $information->category_id)->first();
        return response(['status' => trans('success'), 'categories' => $categories, 'user_group' => \App\Questions::userTypes()[$information->user_group]], 200);
    }

    /*****************
     *  infoDetailsById
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function addQuiz(Request $request)
    {

        $params = array();
        parse_str($request->data, $params);
        foreach ($params  as $key => $value) {
            $request->request->add([$key  => $value]);
        }
        $information   =  Information::find($request->qId);
        $categories    =  Categories::where('id', $information->category_id)->first();
        if (!empty($categories)) {
            $request->request->add(['category'  =>  $categories->name]);
        }
        $request->request->add(['user_group'  =>  $information->user_group]);
        $validator = Validator::make($request->all(), [
            'category' => 'required',  'user_group' => 'required',
            'question_de' => 'required',
            'question' => 'required',
            'answer'   => 'required'

        ]);
        if ($validator->fails()) {
            return response(['status' => trans('failed'), 'errors' => $validator->errors()->all()]);
        }

        $question = Quiz::create([
            'category_id' =>  $information->category_id,
            'user_group' =>  $information->user_group,
            'question_de' =>  $request->question_de,
            'question'    =>  $request->question,
            'answer'      =>  $request->answer,
            'order_no'    => (Quiz::count()) + 1,

        ]);
        $question->save();
        return response(['status' => trans('success')]);
    }

    /************************** */

    public function getDescHtml(Request $request){
        
        
          //  $returnHTML = view('information.desription')->with('infos', $infos)->render();
          $returnHTML = view('information.description')->render();
            return response()->json(array('status' => 'success', 'html' => $returnHTML ));
        
    }
}
