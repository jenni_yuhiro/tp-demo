<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categories;
use App\Answers;
use App\Questions;
use Validator;

class QuestionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $questions = $this->getAllQuestions();
    
 
        return view('questions.index')->withQuestions($questions);
    }

    /**
     * get all resource. // this function used for both frond end and backend
     *
     *  
     * @return \Illuminate\Http\Response
     */
    public function getAllQuestions()
    {
        return  Questions::all()->sortBy("order_no");
    }

    /**
     * get all resource. // this function used for both frond end and backend
     *
     *  
     * @return \Illuminate\Http\Response
     */
    public function getAllQuestionsWithAnswers()
    {
        //        $questions   =  Questions::all()->sortBy("order_no"); 

        //echo '<pre>';
        $result = [];
        $questions   =  Questions::all()->sortBy("order_no");

        foreach ($questions  as $key =>    $question) {

            $answers = Questions::find($question->id);
            $result["$question->category"]  = $question;
            $result["$question->category"]["$question->id"] =        $answers->answers;
            //  $result['questions']['ans'][] =  Answers::whereQuestionId($question->id)->get();
        }
        /*
echo '<pre>';
       print_r(   $result) ;
       exit();  */
        return       $questions;
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories  =  Categories::where('type' , 'question')->get();
        
        return view('questions.create')->withCategories($categories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
/*
        foreach ($request->answers['en']  as $ans_key => $answer) {
             print_r( $request->answers['de'][$ans_key])  ;
        }
exit();
*/
        $validator = Validator::make($request->all(), [  
            'question' => 'required',
            'question_de' => 'required'

        ]);



        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $question = Questions::create([
        
            'question' =>  $request->question,
            'question_de' =>  $request->question_de,
            'user_type' =>  $request->user_type,
            'order_no' => (Questions::count()) + 1,

        ]);
        $question->save();

        foreach ($request->answers['en']  as  $ans_key => $answer) {
            if ($answer  !== null) {
                $answer = Answers::create([
                    'question_id' => $question->id,
                    'answer' => $answer,
                    'answer_de' =>  $request->answers['de'][$ans_key]

                ]);
                $answer->save();
            }
        }
        return redirect('admin/questions')
            ->with('message', trans('questions.created_success'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        echo "show";
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $questions   =  Questions::find($id);
        $answers     =  Answers::whereQuestionId($questions->id)->get();
        $answers_count     =  Answers::whereQuestionId($questions->id)->count();
        $categories  =  Categories::where('type' , 'question')->get();
        return view('questions.edit')->withQuestions($questions)->withAnswers($answers)->withCategories($categories)->withAnsCount($answers_count);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $validator = Validator::make($request->all(), [ // <---
            'question' => 'required',
            'question_de'  => 'required',

        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $questions = Questions::find($id);
       
        $questions->question = $request->question;
        $questions->question_de = $request->question_de;
        $questions->user_type =   $request->user_type;
        $questions->save();

        if (Answers::whereQuestionId($questions->id)->count() > 0) {
            Answers::whereQuestionId($questions->id)->delete();
        }

        if ($request->answers  !== null) {
            foreach ($request->answers['en']   as  $ans_key => $answer) {
                $answer = Answers::create([
                    'question_id' => $questions->id,
                    'answer' => $answer,   
                       'answer_de' =>  $request->answers['de'][$ans_key]

                ]);
                $answer->save();
            }
        }

        return redirect('admin/questions')
            ->with('message', trans('questions.updated_success'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateOrder(Request $request)
    {
        $questions = Questions::all();

        foreach ($questions as $question) {
            foreach ($request->order as $order) {
                if ($order['id'] == $question->id) {
                    $question->update(['order_no' => $order['position']]);
                }
            }
        }

        return response('Update Successfully.', 200);
    }




    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       //                                                                                                                                                                                                              Answers::whereQuestionId($id)->delete();
        Questions::whereId($id)->delete();
        return response(['message' => trans('questions.delete_sucess')], 200);
    }
}
