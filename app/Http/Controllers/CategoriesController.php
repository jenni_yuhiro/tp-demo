<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Validation\Rule;
use App\Categories;
use App\category_lang;
use App\Questions;
use App\Answers;
use Validator;

class CategoriesController extends Controller
{
    
  
 /**
     * 

     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = $this->getAllCategories();

        $types  = Categories::categoryTypes();
        return view('categories.index')->withCategories($categories)->withTypes($types);
    }

    /**
     * get all resource. // this function used for both frond end and backend
     *
     *  
     * @return \Illuminate\Http\Response
     */
    public function getAllCategories()
    {
        return  Categories::all()->sortBy("order_no");
    }

    /**
     * get all resource. // this function used for both frond end and backend
     *
     *  
     * @return \Illuminate\Http\Response
     */
    public function getAllCategoriesWithAnswers()
    {

        $result = [];
        $categories   =  Categories::all()->sortBy("order_no");

        foreach ($Categories  as $key =>    $question) {

            $answers = Categories::find($question->id);



            $result["$question->category"]  = $question;
            $result["$question->category"]["$question->id"] =        $answers->answers;
            //  $result['Categories']['ans'][] =  Answers::whereQuestionId($question->id)->get();
        }
        /*
echo '<pre>';
       print_r(   $result) ;
       exit();  */
        return       $categories;
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $types  = Categories::categoryTypes();
        return view('categories.create')->withTypes($types);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [ // <---
            'type' => 'required',
            'category_name' => 'required|unique:categories,name',
            'category_name_de'  => 'required|unique:category_lang,name',

        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $category = Categories::create([
            'type' =>  $request->type,
            'name' =>  $request->category_name,
            'order_no' => (Categories::count()) + 1,

        ]);
        $category->save();


        if ($category->id !== null) {
            category_lang::create([
                'category_id' =>  $category->id,
                'lang' => 'de',
                'name' =>  $request->category_name_de,
            ]);
        }
        return redirect('admin/categories')
            ->with('message', trans('categories.created_success'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        echo "show";
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categories   =  Categories::find($id);
        $types  = Categories::categoryTypes();
        $categoryLangDE = category_lang::where('category_id', $id)->where('lang', 'de')->get();
        if (!empty($categoryLangDE[0])) $categoryLangDE = $categoryLangDE[0];
        else {
            $categoryLangDE->name = '';
        }

        return view('categories.edit')->withCategories($categories)->withTypes($types)->withCategoryLangDE($categoryLangDE);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category_lang =   category_lang::where('category_id', $id)->where('lang', 'de')->first();
        if (!empty($category_lang)) {
          echo   $category_lang_id= $category_lang->id;
        }
        $validator = Validator::make($request->all(), [ // <---
            'type' => 'required',
            //'category_name' => 'required|unique:categories,name',
            'category_name' => 'required|unique:categories,name,' . $id,             
            'category_name_de'  => 'required|unique:category_lang,name,' .   $category_lang_id ,
        ]);



        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $categories = Categories::find($id);
        $categories->name =  $request->category_name;
        $categories->type = $request->type;

        $categories->save();

        
        if (!empty($category_lang)) {
            $category_lang->name = $request->category_name_de;
            $category_lang->save();
        } else {
            category_lang::create([
                'category_id' =>   $id,
                'lang' => 'de',
                'name' =>  $request->category_name_de,
            ]);
        }
        return redirect('admin/categories')
            ->with('message', trans('categories.updated_success'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateOrder(Request $request)
    {
        $categories = Categories::all();
        foreach ($categories as $category) {
            foreach ($request->order as $order) {
                if ($order['id'] == $category->id) {
                    $category->update(['order_no' => $order['position']]);
                }
            }
        }
        return response('Update Successfully.', 200);
    }




    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //                                                                                                                                                                                                              Answers::whereQuestionId($id)->delete();
        Categories::whereId($id)->delete();
        return response(['message' => trans('categories.delete_sucess')], 200);
    }
}
