<?php

namespace App\Http\Controllers;

use App\Http\Controllers\QuestionsController;

use App\explanatoryVideo;
use App\User;
use App\Visitor;
use App\Categories;
use App\Questions;
use App\Answers;
use App\Quiz;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\Hash;

use function GuzzleHttp\json_decode;

class FrontendController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\View\View
     */


    public function indexLanguage()
    {


        return view('frontend.home-language');
    }


    public function indexUserGroup()
    {

        $lang = isset(request()->route()->parameters['lang']) ? request()->route()->parameters['lang'] : '';
        \Session::put('locale', $lang);



        return view('frontend.home');
    }
    /************************************** */
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\View\View
     */
    public function indexCategories($tab = false, $user_group = false, Request $request)
    {



        $postParms = [
            'firstname' => $request->firstname,
            'surname'  => $request->surname,
            'company' => $request->company,
            'user_work_field' => $request->user_work_field,
            'user_type' => $request->user_group_txt
        ];

     

        $result = [];
        $result_page = [];
        //echo \App::getLocale();

        if ($user_group != "") {


            if (\App::getLocale() == 'de') {
                $info_title_fname = 'info_de';
                $info_desc_fname = 'description_de';
            } else {
                $info_title_fname = 'info';
                $info_desc_fname = 'description';
            }

            $query = "  
   SELECT DISTINCT video as title,  explanatory_videos.category_id , c.order_no as order_no ,c.name , 
        explanatory_videos.id as  Id , NULL as image , NULL as info_description, NULL as page,   explanatory_videos. user_group ,
        'videos' as table_name  FROM explanatory_videos   INNER JOIN 
 categories c on c.id = category_id    WHERE
 explanatory_videos.user_group =  '$user_group'  
 UNION ALL
 SELECT DISTINCT  $info_title_fname , information. category_id  , c.order_no as order_no, c.name , information.id as  Id  ,
   information.image as image  
 ,  information. $info_desc_fname as info_description ,   page, information. user_group , 'information' as table_name 
  FROM information   INNER JOIN 
 categories c on c.id = category_id  INNER JOIN quiz  on information.category_id = quiz.category_id 

 WHERE information.user_group = '$user_group' AND quiz.user_group = '$user_group'   AND information.page = 1
  UNION ALL
 SELECT question_de, 
   category_id , c.order_no as order_no ,c.name , quiz.id as  Id   ,  NULL as image ,  NULL as info_description,  NULL as page, quiz. user_group ,
    'quiz' as table_name FROM quiz INNER JOIN categories c on c.id = category_id  
     WHERE     quiz.user_group = '$user_group'  order BY order_no
   ";
            //   } else {

            /*    $query = "  
            SELECT DISTINCT video as title, explanatory_videos.category_id , c.order_no as order_no ,c.name , explanatory_videos.id as  Id , NULL as image ,
            NULL as info_description,   explanatory_videos.user_group ,  'videos' as table_name  FROM explanatory_videos   INNER JOIN 
     categories c on c.id = category_id  INNER JOIN quiz  on explanatory_videos.category_id = quiz.category_id
     WHERE explanatory_videos.user_group =  '$user_group' AND quiz.user_group = '$user_group' 
     UNION ALL
     SELECT DISTINCT info  , information. category_id , c.order_no as order_no, c.name , information.id as  Id   ,  information.image as image , 
     information.description as info_description ,  information. user_group , 'information' as table_name  FROM information   INNER JOIN 
     categories c on c.id = category_id INNER JOIN quiz  on information.category_id = quiz.category_id 
     
 WHERE information.user_group = '$user_group' AND quiz.user_group = '$user_group'
      UNION ALL
     SELECT question, 
     category_id , c.order_no as order_no ,c.name , quiz.id as  Id   , NULL as image ,  NULL as info_description,  quiz. user_group ,
        'quiz' as table_name FROM quiz INNER JOIN categories c on c.id = category_id 
        
        WHERE     quiz.user_group = '$user_group'
          order BY order_no
       ";
            */ // }
            // echo   $query; 
            $questionsAll = \DB::select($query);


            foreach ($questionsAll as $key => $value) {


                // print_r($value);

                $result[$value->category_id][]  = $value;
                if ($value->table_name == 'information') {
                    $result_page[$value->category_id][$value->page][] =  $value;
                }
            }
        }
        $no_catergories =  count($result);





        return view('frontend.home-categories', ['questions' =>    $result, 'no_catergories' => $no_catergories, 'result_page' => $result_page ,'post_params'=> $postParms ]);
    }
    /************************************** */
    public   function getMoreInformation(Request $request)
    {
        if (\App::getLocale() == 'de') {
            $info_title_fname = 'info_de';
            $info_desc_fname = 'description_de';
        } else {
            $info_title_fname = 'info';
            $info_desc_fname = 'description';
        }
        if ($request->page !== null) {
            $query = "
        SELECT DISTINCT      $info_title_fname as title , information. category_id  , information.id as Id ,   information.image as image ,
         information.  $info_desc_fname as info_description , page, information. user_group ,  'information' as table_name FROM information  WHERE category_id = '$request->category_id' AND information.user_group = '$request->user_group'   AND information.page = $request->page  ";

            $infos = \DB::select($query);

            if (!empty($infos)) {
                $returnHTML = view('frontend.info-details')->with('infos', $infos)->render();
                return response()->json(array('status' => 'success', 'html' => $returnHTML, 'getquize' => false));
            } else {
                return response()->json(array('status' => 'success', 'getquize' => true));
            }
        }
    }
    /************************************** */
    public   function userTypeDetails(Request $request)
    {
        /*
        if (\App::getLocale() == 'de') {
            $questions =   Questions::select(
                \DB::raw(' ifnull(question_de, question) as question','id')
            )->where('user_type', $request->user_type)->get();
        } else {

            $questions =   Questions::where('user_type', $request->user_type)->get();
        } */

        $questions =   Questions::where('user_type', $request->user_type)->get()->sortBy("order_no");
        $user_question_count = $questions->count();
        $returnHTML = view('frontend.user-details')->with('questions', $questions)->render();
        return response()->json(array('status' => 'success', 'html' => $returnHTML, 'question_count' =>   $user_question_count));
    }

    /********* Front end Functions********* */



    /************************************** */


    public static function getCategoryById($id)
    {
        // ->with(['products'])
        if (\App::getLocale() == 'en') {
            $categories =   Categories::select('name as category_name', 'id')->find($id);
        } else {
            $categories = Categories::select(
                \DB::raw(' ifnull(category_lang.name, categories.name) as category_name'),

                'categories.id'
            )->leftjoin('category_lang', 'category_lang.category_id', '=', 'categories.id')->where('categories.id', $id)->get()[0];
        }

        return   $categories;
        // echo '<pre>';
        //  print_r( Categories::find($id)->with(['categoryLanguages']  )->get() ) ; exit();
        //  print_r( Categories::join('category_lang', 'category_lang.cat_id', '=', 'categories.id')->find($id)) ;  exit();
        //   return  Categories::find($id);
    }

    /************************* */
    public function getQuizDetails(Request $request)
    {

        $user_group =  $request->user_group;

        $rand_limit = RAND(10, 15);
        /*   if (\App::getLocale() == 'de') {
            $qusetion_fname = 'question_de';
            //  $quiz =    Quiz::select('question_de as question', 'id')->where('category_id', $request->category_id)->orderByRaw('RAND()')->limit($rand_limit)->get();
        } else {
            $qusetion_fname = 'question';
            //  $quiz =    Quiz::select("question_de as question", 'id')->where('category_id', $request->category_id)->orderByRaw('RAND()')->limit($rand_limit)->get();
        } */
        $qusetion_fname = 'question';
        $quiz =    Quiz::select("$qusetion_fname as question", 'id')
            ->where('category_id', $request->category_id)
            ->where('user_group',   $user_group)->where('parent_id', '0')
            ->orderByRaw('RAND()')->limit($rand_limit)
            ->get();
        $quiz_detail = [];
        $qa_arr = [];

        $quizCount = $quiz->count();
        $quizCount_1 = 0;
        if ($quizCount > 0) {

            foreach ($quiz as $key => $val) {
                $quiz_detail[$val->id] =  $val;
                $multi_quizes =  Quiz::select("question", 'id')->whereParentId($val->id)->get();
                $quiz_detail[$val->id]['parent'] =    $multi_quizes;
                if ($multi_quizes->count() > 0) {
                    $quizCount_1  = $quizCount_1 + $multi_quizes->count();
                    foreach ($multi_quizes as $keym => $valm) {
                        $qa_arr[$valm->id] = 0;
                    }
                } else {
                    $quizCount_1  = $quizCount_1 + 1;
                }
            }



            $returnHTML = view('frontend.quiz-details')->with(['quiz' => $quiz_detail, 'quizCount' => $quizCount_1, 'qa_arr' => $qa_arr, 'category_id' =>  $request->category_id])->render();
            return response()->json(array('status' => 'success', 'html' => $returnHTML));
        } else {
            return response()->json(array('status' => 'no_quiz'));
        }
    }

    /************************* */
    public function verifyQuiz(Request $request)
    {

        $quiz_count =  $request->quizCount; // Quiz::where('category_id', $request->category_id)->count();
        $false_result = false;
        if (!isset($request->answers)) {
            return response()->json(array('error' => 'failed'), 400);
        }

        if ($quiz_count != count($request->answers)) {
            return response()->json(array('error' => 'failed'), 400);
        } else {
            foreach ($request->answers  as $key => $val) {
                if ($key != "") {
                    $quizCnt =  Quiz::where('id', $key)->where('answer', $val)->count();
                    if ($quizCnt == 0) {
                        $false_result = true;
                    }
                }
            }
        }

        if ($false_result === true) {
            return response()->json(array('error' => 'failed'), 400);
        } else {
            return response()->json(array('success' => 'success'));
        }
        //var_dump(  $false_result);
        //  $quiz =    Quiz::where('category_id', $request->category_id)->orderBy("order_no")->get();
    }


    public function review(Request $request)
    {

        $params = array();
        parse_str($request->data, $params);

        $today = date('d.m.Y');

        $params['validDate'] =   date('d.m.Y', strtotime($today . ' +30 days'));

       // $params['question_count'] = $params['no_of_cat'];

      $exp_params= json_decode($params['post_params'] ,true)  ;
      $exp_params['today'] =   $today;
      $exp_params['validDate'] =   date('d.m.Y', strtotime($today . ' +30 days'));

/*
        $name = Visitor::create([
            'name'       =>    $exp_params['firstname'],
            'last_name'  =>    $exp_params['surname'],
            'company'    =>    $exp_params['company'],
            'user_group' =>    $exp_params['user_type'],
            'valid_date' =>    $params['validDate'],
            'work_field' => ($exp_params['user_work_field'] != '') ? $exp_params['user_work_field'] : null,

        ]);
        $name->save(); */
        /***********QR code array  */
        $questions  = '';
        if ($request->allquesKey != '') {
            $questions_array = json_decode($request->allquesKey, true);
            $questions =  array_keys(call_user_func_array('array_merge',  $questions_array));
        }
        $work_fields = '';
        if ($exp_params['user_work_field'] != '') {
            // Convert JSON string to Array
            $work_fields = array_values(array_filter(json_decode($exp_params['user_work_field'], true)));
        }

        $dataqr = [];
        $dataqr['name'] =  $exp_params['firstname'];
        $dataqr['last_name'] =  $exp_params['surname'];
        $dataqr['company'] = $exp_params['company'];
        $dataqr['user_group'] = $exp_params['user_type'];
        $dataqr['user_group_text'] = Questions::userTypes()[$exp_params['user_type']];
        //  $dataqr['questions'] = $questions;
        //  $dataqr['work_fields'] = $work_fields;
        $dataqr['valid_date'] = $exp_params['validDate'];
 

        $returnHTML = view('frontend.review')->withData($exp_params)->withDataqr($dataqr)->render();
        return response()->json(array('status' => 'success', 'html' => $returnHTML));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function userStore(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'name' => 'required',
                'email' => 'required'

            ]
        );



        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $name = User::create([
            'name' =>  $request->name,
            'email' =>  $request->email,
            'password'  => Hash::make('123456'),

        ]);
        $name->save();

        return redirect()->back()->with('message', trans('questions.created_success'));
    }
}
