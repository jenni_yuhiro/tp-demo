<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categories;
use App\Answers;
use App\Quiz;
use Validator;
use App\Information;
use App\explanatoryVideo;

class QuizController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $quiz = $this->getAllQuiz();

        return view('quiz.index')->withQuiz($quiz);
    }

    /**
     * get all resource. // this function used for both frond end and backend
     *
     *  
     * @return \Illuminate\Http\Response
     */
    public function getAllQuiz()
    {
        return  Quiz::whereParentId('0')->get()->sortBy("order_no");
    }

    /**
     * get all resource. // this function used for both frond end and backend
     *
     *  
     * @return \Illuminate\Http\Response
     */
    public function getAllQuizWithAnswers()
    {
        //        $quiz   =  Quiz::all()->sortBy("order_no"); 

        //echo '<pre>';




        $result = [];
        $quiz   =  Quiz::all()->sortBy("order_no");

        foreach ($quiz  as $key =>    $question) {

            $answers = Quiz::find($question->id);



            $result["$question->category"]  = $question;
            $result["$question->category"]["$question->id"] =        $answers->answers;
            //  $result['Quiz']['ans'][] =  Answers::whereQuestionId($question->id)->get();
        }
        /*
echo '<pre>';
       print_r(   $result) ;
       exit();  */
        return       $quiz;
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories  =  Categories::whereIn('type', ['question', 'video'])->get();
        return view('quiz.create')->withCategories($categories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        echo '<pre>';

        print_r($request->answers);
        print_r($request->multi_answer_sel);

        Categories::whereIn('type', ['question', 'video'])->get();
        if ($request->multi_answers ==  1) {
            $validator = Validator::make($request->all(), [
                'category' => 'required',
                'user_group' => 'required',
                'question' => 'required',


            ]);
        } else {
            $validator = Validator::make($request->all(), [
                'category' => 'required',
                'user_group' => 'required',
                'question' => 'required',
                'answer'   => 'required'

            ]);
        }


        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }


        $info_count =    Information::where(['category_id' =>  $request->category,   'user_group' =>  $request->user_group])->count();
        $video_count =    explanatoryVideo::where(['category_id' =>  $request->category,   'user_group' =>  $request->user_group])->count();


        if ($info_count  == 0 &&  $video_count  == 0) {
            return redirect()->back()->withErrors(['no_infos' => trans('quiz.no_infos')])->withInput();
        } else {

            $question = Quiz::create([
                'category_id' =>  $request->category,
                'user_group' =>  $request->user_group,
                //'question_de' =>  $request->question_de,
                'question'    =>  $request->question,
                'answer'      =>  0,
                'parent_id'   =>  0,
                'order_no'    => (Quiz::count()) + 1,

            ]);
            $question->save();
            if ($request->multi_answers ==  1) {           

                $parent_id =  $question->id;
     
                foreach ($request->answers as $key => $m_qa) {
                    
                    $question_ma = Quiz::create([
                        'category_id' =>  $request->category,
                        'user_group'  =>  $request->user_group,
                        'question'    =>  $m_qa,
                        'answer'      =>  $request->multi_answer_sel[$key],
                        'parent_id'   =>     $parent_id

                    ]);
                    $question_ma->save();
                    
                }
            }  
        }
        return redirect('admin/quiz')
            ->with('message', trans('quiz.created_success'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        echo "show";
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $quiz   =  Quiz::find($id);
        $quizMultiAns = Quiz::whereParentId($id)->get();

       
        $answers     =  Answers::whereQuestionId($quiz->id)->get();
        $answers_count     =  Answers::whereQuestionId($quiz->id)->count();
        $categories  =  Categories::whereIn('type', ['question', 'video'])->get();
        return view('quiz.edit')->withQuiz($quiz)->withquizMultiAns($quizMultiAns)->withAnswers($answers)->withCategories($categories)->withAnsCount($answers_count);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $validator = Validator::make($request->all(), [ // <---
            'category' => 'required',
            'user_group' => 'required',
            // 'question_de' => 'required',
            'question' => 'required'

        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $info_count =    Information::where(['category_id' =>  $request->category,   'user_group' =>  $request->user_group])->count();
        $video_count =    explanatoryVideo::where(['category_id' =>  $request->category,   'user_group' =>  $request->user_group])->count();


        if ($info_count  == 0 &&  $video_count  == 0) {
            return redirect()->back()->withErrors(['no_infos' => trans('quiz.no_infos')])->withInput();
        } else {
            $quiz = Quiz::find($id);
            $quiz->category_id =  $request->category;
            $quiz->user_group =   $request->user_group;
            //  $quiz->question_de = $request->question_de;
            $quiz->question = $request->question;
            $quiz->answer      =  $request->answer;
            $quiz->save();


            if ($request->multi_answers ==  1) {
               

                Quiz::whereParentId($id)->delete();            

                foreach ($request->answers as $key => $m_qa) {
             
                 if(  $m_qa != "") {
                        $question_ma = Quiz::create([
                            'category_id' =>  $request->category,
                            'user_group'  =>  $request->user_group,
                            'question'    =>  $m_qa,
                            'answer'      =>  $request->multi_answer_sel[$key],
                            'parent_id'   =>  $id
    
                        ]);
                        $question_ma->save(); 
                        }
                    }
               
                   
                  
            } else {
                Quiz::whereParentId($id)->delete();
               
            }
        }
        return redirect('admin/quiz')
            ->with('message', trans('quiz.updated_success'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateOrder(Request $request)
    {
        $quiz = Quiz::all();



        foreach ($quiz as $question) {
            foreach ($request->order as $order) {
                if ($order['id'] == $question->id) {
                    $question->update(['order_no' => $order['position']]);
                }
            }
        }

        return response('Update Successfully.', 200);
    }




    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //                                                                                                                                                                                                              Answers::whereQuestionId($id)->delete();
        Quiz::whereId($id)->delete();
        return response(['message' => trans('Quiz.delete_sucess')], 200);
    }
}
