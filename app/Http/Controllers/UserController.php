<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Requests\UserRequest;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Validator;
 

class UserController extends Controller
{
    /**
     * Display a listing of the users
     *
     * @param  \App\User  $model
     * @return \Illuminate\View\View
     */
    public function index(User $user )
    {
      
        $users = User:: select('*')->sortable()->orderBy('created_at', 'DESC')->paginate(10);        
        return view('users.index')->withUsers($users );
    }

     
}
