<?php

namespace App\Http\Controllers;

use App\explanatoryVideo;
use App\Categories;
use Illuminate\Http\Request;
use Validator;

class ExplanatoryVideoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
        $info  =  explanatoryVideo::all();

        return view('videos.index')->withVideo($info);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $video = explanatoryVideo::whereMain(1)->first();
        $categories  =  Categories::where('type', 'video')->get();
        return view('videos.create')->withCategories($categories)->withVideo($video);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\explanatoryVideo  $explanatoryVideo
     * @return \Illuminate\Http\Response
     */
    public function show(explanatoryVideo $explanatoryVideo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\explanatoryVideo  $explanatoryVideo
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $video   =  explanatoryVideo::find($id);

        $categories  =  Categories::where('type', 'video')->get();
        return view('videos.edit')->withVideo($video)->withCategories($categories);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\explanatoryVideo  $explanatoryVideo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,   $id)
    {

        /*   $validator = Validator::make($request->all(), [
            'videoName' => 'sometimes|mimes:video/mp4,mp4,video/avi,video/mpeg,video/quicktime|max:2048'
        ]);
*/

        $this->validate($request, [

            'category_id' => 'required',   'user_group' => 'required',
            'title'    =>   'required',
            'title_de' => 'required',    
            'videoName' => 'sometimes|mimes:video/mp4,mp4,video/avi,video/mpeg,video/quicktime ',

        ],
      
        [ 'videoName.required'  => __('video.validation.videoName_required') ,
         'category_id.required' => trans('video.validation.category_required') ]);



        $video   =  explanatoryVideo::find($id);

        if ($request->file()) {


            $file = $request->file('videoName');
            $file_path = 'uploads/video/' . time() . '_' . $file->getClientOriginalName();
            $file->move('uploads/video/', time() . '_' . $file->getClientOriginalName());

            if (file_exists(public_path($video->video)) &&  $video->video != "") {


                unlink(public_path($video->video));
            }

            $video->video =    $file_path;
        }
        $video->user_group =   $request->user_group;
        $video->category_id =   $request->category_id;
        $video->title =   $request->title;
        $video->title_de = $request->title_de;


        // exit;
        $video->save();
        //exit;
        return redirect('admin/video')
            ->with('message', trans('video.updated_success'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\explanatoryVideo  $explanatoryVideo
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        explanatoryVideo::whereId($id)->delete();
        return response(['message' => trans('video.delete_sucess')], 200);
    }

    public function explanatoryVideo()
    {
        // $video = [] ;
        $video = explanatoryVideo::whereMain(1)->first();
        $categories  =  Categories::where('type', 'video')->get();
        return view('questions.video')->withCategories($categories)->withVideo($video);

        // Answers::whereQuestionId($id)->delete();
        // Questions::whereId($id)->delete();         
        // return response(['message' => trans('questions.delete_sucess') ], 200);
    }
    public function uploadVideo(Request $request)
    {


        $request->validate([
            'category_id' => 'required',   'user_group' => 'required',
            'title'    =>   'required',
            'title_de' => 'required',      

            'videoName' => 'required|mimes:video/mp4,mp4,video/avi,video/mpeg,video/quicktime '
        ],
        [ 'videoName.required'  => __('video.validation.videoName_required') ,
        'category_id.required' => __('video.validation.category_required') ]);
        // 'video' => 'mimetypes:video/avi,video/mpeg,video/quicktime'


        if ($request->file()) {
            $file = $request->file('videoName');
            $file_path = 'uploads/video/' .  time() . '_' . $file->getClientOriginalName();
            $file->move('uploads/video/', time() . '_' . $file->getClientOriginalName());

            //   if (explanatoryVideo::count() == 0) {
            $video = explanatoryVideo::create([
                'title' =>  $request->title,
                'title_de' =>  $request->title_de,
                'video' =>    $file_path,
                'category_id' =>  $request->category_id,
                'user_group'  =>  $request->user_group,
                // 'main'  =>    1
            ]);
            $video->save();
            //  } 
            /*  else {
                $video     =  explanatoryVideo::whereMain(1)->first();

                // $video = explanatoryVideo::find($id);
                $video->title =  $file->getClientOriginalName();
                $video->video =  $file_path;
                $video->save();
            }
*/

            return redirect('admin/video')
                ->with('message', trans('video.created_success'));

            // if ($validator->fails()) {
            //  return redirect()->back()->with('message', trans('questions.created_success'));;
            // }

            // $videoModel->name = time().'_'.$request->file->getClientOriginalName();
            //  $videoModel->file_path = '/storage/' . $filePath;
            //  $videoModel->save();

            /*  return back()
                ->with('success', 'File has been uploaded.')
                ->with('file', $fileName);
                */
        }
    }
}
