<?php

namespace App\Http\Middleware;

use Closure;

class AdminLocalization
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
 
        if (\Request::segment(1) == 'admin' || \Request::segment(1) == 'home' ) {
            \App::setlocale('de');
        }
        return $next($request);
    }
}
