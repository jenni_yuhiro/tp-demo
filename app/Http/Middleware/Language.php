<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App ;
class Language
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

     

    public function handle($request, Closure $next)
    {

        if(\Session::has('locale'))
        {
            \App::setlocale(\Session::get('locale'));
        }
        
      // print_r(session('user_locale'));
       // \App::setLocale(session('user_locale', config('app.locale')));

      // $locale = $request->route('locale');   
       return $next($request); 
    }
}
