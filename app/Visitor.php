<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Visitor extends Model
{
    use \App\Http\Traits\UsesUuid;

  
    // column name of key
    protected $primaryKey = 'id';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',  'name' ,     'last_name' ,'company' ,'user_group','valid_date' , 'signature_flag' ,'checkin_status' , 'work_field'
    ];


    /**
     * timestamps
     *
     * @var bool
     */
    public $timestamps = true;

   

     
}
