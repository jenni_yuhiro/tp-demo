<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quiz extends Model
{
    use \App\Http\Traits\UsesUuid;

    protected $table = 'quiz';

    // column name of key
    protected $primaryKey = 'id';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'category_id', 'user_group' ,'question_de',
        'question', 'order_no', 'answer','parent_id'

    ];


    /**
     * timestamps
     *
     * @var bool
     */
    public $timestamps = false;

    public function Categories()
    {
        return $this->belongsTo('App\Categories', 'category_id', 'id');
    }

    
}
