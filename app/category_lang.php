<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class category_lang extends Model
{
    use \App\Http\Traits\UsesUuid;

    protected $table = 'category_lang';
    // column name of key
    protected $primaryKey = 'id';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;


    
    /**
     * timestamps
     *
     * @var bool
     */
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'category_id',
        'name', 'lang'

    ];
    public function categoryLanguages()
    {
        return $this->belongsTo('App\Categories', 'category_id', 'id');
    }
}
