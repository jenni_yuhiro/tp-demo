<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
    use \App\Http\Traits\UsesUuid;


    // column name of key
    protected $primaryKey = 'id';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'type',
        'name', 'order_no'

    ];


    public function categoryLanguages()
    {
        return $this->hasMany('App\category_lang', 'category_id', 'id');
    }




    public  function questions()
    {
        return $this->hasMany('App\Questions', 'category', 'id');
    }


    public static function categoryTypes()
    {
        return [
            'question' => 'Question',
            'video'    =>  'Video',
         /*   'info'     => 'Info Text',
            'static'   => 'Static Step' */
        ];
    }
}
