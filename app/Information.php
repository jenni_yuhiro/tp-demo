<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Information extends Model
{
    use \App\Http\Traits\UsesUuid;

 

    // column name of key
    protected $primaryKey = 'id';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [

         
        'id', 'category_id', 'user_group', 'page' , 'info_de',
        'info', 'order_no', 'image', 'info_desc_de' ,'info_desc' ,'description_de' ,'description'

    ];


    /**
     * timestamps
     *
     * @var bool
     */
    public $timestamps = false;

    public function Categories()
    {
        return $this->belongsTo('App\Categories', 'category_id', 'id');
    }
}
