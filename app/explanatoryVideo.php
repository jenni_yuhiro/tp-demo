<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class explanatoryVideo extends Model
{

    use \App\Http\Traits\UsesUuid;

  

    // column name of key
    protected $primaryKey = 'id';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    /**
     * timestamps
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
      'title',  'title_de', 'category_id', 'user_group' ,
        'video', 'main'

    ];

    public function Categories()
    {
        return $this->belongsTo('App\Categories', 'category_id', 'id');
    }
}
